import {
  fetchLastestBoxes,
  fetchHottestBoxes,
  fetchLastestIndividualBoxes,
  fetchHottestProduct,
  fetchProductByCategory
} from '../api/shop.api';

import { CATEGORY_FILTER } from '../constants/category.config';
import {
  /** BOX MỚI NHẤT */
  FETCH_LASTEST_BOXES_PENDING,
  FETCH_LASTEST_BOXES_SUCCESS,
  FETCH_LASTEST_BOXES_ERROR,

  /** BOX BÁN CHẠY NHẤT */
  FETCH_HOTTEST_BOXES_PENDING,
  FETCH_HOTTEST_BOXES_SUCCESS,
  FETCH_HOTTEST_BOXES_ERROR,

  /** MUA LẺ MỚI NHẤT */
  FETCH_LASTTEST_INDIVIDUAL_BOXES_PENDING,
  FETCH_LASTTEST_INDIVIDUAL_BOXES_SUCCESS,
  FETCH_LASTTEST_INDIVIDUAL_BOXES_ERROR,

  /** XEM BOX CÓ SẢN PHẨM */
  FETCH_HOTTEST_PRODUCT_PENDING,
  FETCH_HOTTEST_PRODUCT_SUCCESS,
  FETCH_HOTTEST_PRODUCT_ERROR,

  /** LẤY DANH SÁCH SẢN PHẨM CỦA CATEGORY */
  FETCH_PRODUCT_BY_CATEGORY_PENDING,
  FETCH_PRODUCT_BY_CATEGORY_SUCCESS,
  FETCH_PRODUCT_BY_CATEGORY_ERROR

} from '../constants/shop.action';

/** BOX MỚI NHẤT */
export function fetchLastestBoxesAction() {
  return (dispatch, getState) => {
    return dispatch({
      types: [
        FETCH_LASTEST_BOXES_PENDING,
        FETCH_LASTEST_BOXES_SUCCESS,
        FETCH_LASTEST_BOXES_ERROR
      ],
      payload: {
        promise: fetchLastestBoxes().then(res => res),
      },
    });
  };
}

/** BOX BÁN CHẠY NHẤT */
export function fetchHottestBoxesAction() {
  return (dispatch, getState) => {
    return dispatch({
      types: [
        FETCH_HOTTEST_BOXES_PENDING,
        FETCH_HOTTEST_BOXES_SUCCESS,
        FETCH_HOTTEST_BOXES_ERROR
      ],
      payload: {
        promise: fetchHottestBoxes().then(res => res),
      },
    });
  };
}

/** MUA LẺ MỚI NHẤT */
export function fetchLastestIndividualBoxesAction() {
  return (dispatch, getState) => {
    return dispatch({
      types: [
        FETCH_LASTTEST_INDIVIDUAL_BOXES_PENDING,
        FETCH_LASTTEST_INDIVIDUAL_BOXES_SUCCESS,
        FETCH_LASTTEST_INDIVIDUAL_BOXES_ERROR
      ],
      payload: {
        promise: fetchLastestIndividualBoxes().then(res => res),
      },
    });
  };
}

/** XEM BOX CÓ SẢN PHẨM */
export function fetchHottestProductAction() {
  return (dispatch, getState) => {
    return dispatch({
      types: [
        FETCH_HOTTEST_PRODUCT_PENDING,
        FETCH_HOTTEST_PRODUCT_SUCCESS,
        FETCH_HOTTEST_PRODUCT_ERROR
      ],
      payload: {
        promise: fetchHottestProduct().then(res => res),
      },
    });
  };
}

/** LẤY DANH SÁCH SẢN PHẨM CỦA CATEGORY */
export function fetchProductByCategoryAction(categoryFilter: any) {
  let
    paramsQuery: Array<any> = [],
    paramsQueryString: string = '',
    listBrand: Array<any> = [];

  /** Parse data from category filter (get from params url )*/
  categoryFilter.params.map(params => {
    switch (params.key) {
      case CATEGORY_FILTER.price.minPrice.key:
        paramsQuery.push(`pl=${params.value}`);
        break;

      case CATEGORY_FILTER.price.maxPrice.key:
        paramsQuery.push(`ph=${params.value}`);
        break;

      case CATEGORY_FILTER.sort.key:
        paramsQuery.push(`sort=${params.value}`);
        break;

      case CATEGORY_FILTER.page.key:
        paramsQuery.push(`page=${params.value}`);
        break;

      case CATEGORY_FILTER.brand.key:
        listBrand.push(params.value);
        break;

      default: break;
    }
  });

  /** Sort list of brand by aphabet */
  listBrand = listBrand.sort();

  /** Push list brand into query string */
  if (listBrand.length > 0) {
    paramsQuery.unshift(`bids=${listBrand.join(',')}`)
  }

  /** Build query string */
  if (paramsQuery.length > 0) {
    paramsQueryString = `&${paramsQuery.join('&')}`;
  }

  console.log(categoryFilter.idCategory);
  console.log(paramsQueryString);
  return (dispatch, getState) => {
    return dispatch({
      types: [
        FETCH_PRODUCT_BY_CATEGORY_PENDING,
        FETCH_PRODUCT_BY_CATEGORY_SUCCESS,
        FETCH_PRODUCT_BY_CATEGORY_ERROR
      ],
      payload: {
        promise: fetchHottestProduct().then(res => res),
      },
    });
  };
}
