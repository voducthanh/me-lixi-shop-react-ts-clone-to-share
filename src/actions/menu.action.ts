import {
  fetchListMenu
} from '../api/menu.api';

import {
  /** BROWSE NODE - MENU CHÍNH */
  FETCH_LIST_MENU_PENDING,
  FETCH_LIST_MENU_SUCCESS,
  FETCH_LIST_MENU_ERROR,

  /** SHOW / HIDE SUB MENU DESKTOP */
  MENU_DESKTOP_ENTER,
  MENU_DESKTOP_LEAVE,

} from '../constants/menu.action';

/** BROWSE NODE - MENU CHÍNH */
export function fetchListMenuAction() {
  return (dispatch, getState) => {
    return dispatch({
      types: [
        FETCH_LIST_MENU_PENDING,
        FETCH_LIST_MENU_SUCCESS,
        FETCH_LIST_MENU_ERROR
      ],
      payload: {
        promise: fetchListMenu().then(res => res),
      },
    });
  };
}


export function showMenuDesktopAction(_ID) {
  return {
    type: MENU_DESKTOP_ENTER,
    payload: _ID
  };
}

export function hideMenuDesktopAction() {
  return {
    type: MENU_DESKTOP_LEAVE,
  };
}
