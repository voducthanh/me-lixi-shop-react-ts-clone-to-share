import {
  fetchListExpert,
  fetchListExpertVideo
} from '../api/expert.api';

import {
  /** BOX THIẾT KẾ BỞI CÁC CHUYÊN GIA */
  FECTH_LIST_EXPERT_PENDING,
  FECTH_LIST_EXPERT_SUCCESS,
  FECTH_LIST_EXPERT_ERROR,

  /** VIDEO TƯ VẤN - TRANG CHỦ */
  FECTH_LIST_EXPERT_VIDEO_PENDING,
  FECTH_LIST_EXPERT_VIDEO_SUCCESS,
  FECTH_LIST_EXPERT_VIDEO_ERROR,

} from '../constants/expert.action';

/** BOX THIẾT KẾ BỞI CÁC CHUYÊN GIA */
export function fetchListExpertAction() {
  return (dispatch, getState) => {
    return dispatch({
      types: [
        FECTH_LIST_EXPERT_PENDING,
        FECTH_LIST_EXPERT_SUCCESS,
        FECTH_LIST_EXPERT_ERROR
      ],
      payload: {
        promise: fetchListExpert().then(res => res),
      },
    });
  };
}

/** VIDEO TƯ VẤN - TRANG CHỦ */
export function fetchListExpertVideoAction() {
  return (dispatch, getState) => {
    return dispatch({
      types: [
        FECTH_LIST_EXPERT_VIDEO_PENDING,
        FECTH_LIST_EXPERT_VIDEO_SUCCESS,
        FECTH_LIST_EXPERT_VIDEO_ERROR,
      ],
      payload: {
        promise: fetchListExpertVideo().then(res => res),
      },
    });
  };
}
