/**
 * Decode Entities html
 * @param str string
 */
export function decodeEntities(str) {
  if (null === str || 'undefined' === typeof str) {
    return '';
  }
  return String(str).replace(/&amp;/g, '&').replace(/&lt;/g, '<').replace(/&gt;/g, '>').replace(/&quot;/g, '"');
}
