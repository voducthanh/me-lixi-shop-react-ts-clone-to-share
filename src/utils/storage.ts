
/**
 * Storage data into localStorage
 * @param _key string : key data
 * @param _value any : value data
 *
 * Stringify JSON before store data
 *
 * @return void
 */
export function set(_key: string, _value: any) {
  localStorage.setItem(_key, JSON.stringify(_value));
}

/**
 * Get data from localStorage by key data
 * @param _key string : key data
 *
 * If data not exist -> return null
 * Try catch for exception -> return null
 *
 * @return data after JSON parse or null
 */
export function get(_key: string) {
  return null === localStorage.getItem(_key)
    ? ''
    : () => {
      try {
        return JSON.parse(localStorage.getItem(_key));
      } catch (e) {
        return '';
      }
    };
}

/**
 * Check data exist in localstorage
 * @param _key string : key data
 *
 * @return Boolean
 */
export function check(_key: string) {
  return null !== localStorage.getItem(_key);
}
