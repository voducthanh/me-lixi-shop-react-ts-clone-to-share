/**
 * Format currency to VND
 * @param currency value number of currency
 */
export function currenyFormat(currency) {
  /**
   * Validate value
   * - NULL
   * - UNDEFINED
   * NOT A NUMBER
   */
  if (null === currency || 'undefined' === typeof currency || true === isNaN(currency)) {
    return '0 đ';
  }

  /**
   * Validate value
   * < 0
   */
  if (currency < 0) {
    return '0 đ';
  }

  return currency.toString().replace(/(\d)(?=(\d{3})+(?!\d))/g, '$1.') + ' đ';
}
