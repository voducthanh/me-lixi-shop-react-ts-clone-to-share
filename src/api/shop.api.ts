import { get } from './server.config';
import { SHOP_API } from '../constants/path.api';


/** BOX MỚI NHẤT */
export function fetchLastestBoxes() {
  return get(SHOP_API.LASTEST_BOXES);
}

/** BOX BÁN CHẠY NHẤT */
export function fetchHottestBoxes() {
  return get(SHOP_API.HOTTEST_BOXES);
}

/** MUA LẺ MỚI NHẤT */
export function fetchLastestIndividualBoxes() {
  return get(SHOP_API.LASTTEST_INDIVIDUAL_BOXES);
}

/** XEM BOX CÓ SẢN PHẨM */
export function fetchHottestProduct() {
  return get(SHOP_API.HOTTEST_PRODUCT);
}

/** LẤY DANH SÁCH SẢN PHẨM CỦA CATEGORY */
export function fetchProductByCategory(idCategory: any, query = '') {
  console.log('fetc');
  console.log(idCategory, query);
  return get(SHOP_API.PRODUCT_BY_CATEGORY(idCategory, query));
}
