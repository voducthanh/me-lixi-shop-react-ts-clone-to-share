import { get } from './server.config';
import { EXPERT_API } from '../constants/path.api';


/** BOX THIẾT KẾ BỞI CÁC CHUYÊN GIA */
export function fetchListExpert() {
  return get(EXPERT_API.LIST_EXPERT);
}

/** VIDEO TƯ VẤN TRANG CHỦ */
export function fetchListExpertVideo() {
  return get(EXPERT_API.LIST_EXPERT_VIDEO);
}
