import { SERVER_API, VERSION_API } from '../constants/api.config';
import { IDetailApi } from '../constants/path.api';

import 'whatwg-fetch';

export const BASE_URL = '/api';

export function post(
  path: String,
  data: Object = null,
  _API_VERSION: String = VERSION_API.VERSION_1
) {
  return fetch(`${SERVER_API}${_API_VERSION}${path}`, {
    method: 'post',
    headers: {
      'Accept': 'application/json',
      'Content-Type': 'application/json'
    },
    body: data && JSON.stringify(data)
  }).then(response => response.json());
}

/**
 * get ajax function
 * @param path : api path
 * @param _API_VERSION : default version 1
 *
 * @return new promise to fetch data from API
 */
export function get(
  DETAIL_API: IDetailApi,
  _VERSION_API: string = VERSION_API.VERSION_1
) {
  return new Promise(
    (resolve, reject) => {
      fetch(`${SERVER_API}${_VERSION_API}${DETAIL_API.path}`, {
        method: 'get',
        headers: {
          'Accept': 'application/json',
          'Content-Type': 'application/json'
        },
      })
        .then(response => resolve(response.json()))
        .then(null, (err) => reject(new Error('err')));
    });
}
