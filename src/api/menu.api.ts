import { get } from './server.config';
import { MENU_API } from '../constants/path.api';


/** BROWSE NODE - MENU CHÍNH */
export function fetchListMenu() {
  return get(MENU_API.LIST_MENU);
}
