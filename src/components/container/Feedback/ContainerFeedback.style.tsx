/**
 * Created by Thành Võ on 16.03.2017
 */

import MEDIA_QUERIES from '../../../styles/mediaQueries.style';

export default {
  display: 'block',
  textAlign: 'center',

  list: {
    padding: '10px 10px 0',

    [MEDIA_QUERIES.tablet960]: {
      padding: '10px 0 0'
    }
  },

  item: {
    padding: '0 10px',
    marginBottom: '20px',
    width: '50%',

    [MEDIA_QUERIES.tablet960]: {
      padding: '0 15px',
      marginBottom: 30,
      width: '25%',
    }
  },

  buttonMore: {
    marginBottom: 30
  }
};
