/**
 * Created by Thành Võ on 16.03.2017
 */

import * as React from 'react';
import * as Radium from 'radium';
import ItemFeedback from '../../item/Feedback';

import * as VARIABLE from '../../../styles/variable.style';
import * as COMPONENT from '../../../styles/component.style';
import * as LAYOUT from '../../../styles/layout.style';

import STYLE from './ContainerFeedback.style';

const AVATAR = require('../../../assets/images/sample-data/avatar.jpg');

const FEEDBACK_THUMBNAIL_1 = require('../../../assets/images/sample-data/product-sample-1.jpg');
const FEEDBACK_THUMBNAIL_2 = require('../../../assets/images/sample-data/product-sample-2.jpg');
const FEEDBACK_THUMBNAIL_3 = require('../../../assets/images/sample-data/product-sample-3.jpg');
const FEEDBACK_THUMBNAIL_4 = require('../../../assets/images/sample-data/product-sample-4.jpg');
const FEEDBACK_THUMBNAIL_5 = require('../../../assets/images/sample-data/product-sample-5.jpg');
const FEEDBACK_THUMBNAIL_6 = require('../../../assets/images/sample-data/product-sample-6.jpg');
const FEEDBACK_THUMBNAIL_7 = require('../../../assets/images/sample-data/product-sample-7.jpg');
const FEEDBACK_THUMBNAIL_8 = require('../../../assets/images/sample-data/product-sample-8.jpg');

@Radium
class ContainerFeedback extends React.Component<any, any> {
  constructor(props: any) {
    super(props);

    this.state = {
      feedbackList: [
        {
          id: 1,
          image: FEEDBACK_THUMBNAIL_1,
          avatar: AVATAR,
          username: 'Jesisca Lala',
          title: 'Chất phấn ẩm nên không gây hiện tượng cakey, cho lớp nền mỏng, nhẹ...'
        },
        {
          id: 2,
          image: FEEDBACK_THUMBNAIL_2,
          avatar: AVATAR,
          username: 'Jesisca Lala',
          title: 'Chất phấn ẩm nên không gây hiện tượng cakey, cho lớp nền mỏng, nhẹ...'
        },
        {
          id: 3,
          image: FEEDBACK_THUMBNAIL_3,
          avatar: AVATAR,
          username: 'Jesisca Lala',
          title: 'Chất phấn ẩm nên không gây hiện tượng cakey, cho lớp nền mỏng, nhẹ...'
        },
        {
          id: 4,
          image: FEEDBACK_THUMBNAIL_4,
          avatar: AVATAR,
          username: 'Jesisca Lala',
          title: 'Chất phấn ẩm nên không gây hiện tượng cakey, cho lớp nền mỏng, nhẹ...'
        },
        {
          id: 5,
          image: FEEDBACK_THUMBNAIL_5,
          avatar: AVATAR,
          username: 'Jesisca Lala',
          title: 'Chất phấn ẩm nên không gây hiện tượng cakey, cho lớp nền mỏng, nhẹ...'
        },
        {
          id: 6,
          image: FEEDBACK_THUMBNAIL_6,
          avatar: AVATAR,
          username: 'Jesisca Lala',
          title: 'Chất phấn ẩm nên không gây hiện tượng cakey, cho lớp nền mỏng, nhẹ...'
        },
        {
          id: 7,
          image: FEEDBACK_THUMBNAIL_7,
          avatar: AVATAR,
          username: 'Jesisca Lala',
          title: 'Chất phấn ẩm nên không gây hiện tượng cakey, cho lớp nền mỏng, nhẹ...'
        },
        {
          id: 8,
          image: FEEDBACK_THUMBNAIL_8,
          avatar: AVATAR,
          username: 'Jesisca Lala',
          title: 'Chất phấn ẩm nên không gây hiện tượng cakey, cho lớp nền mỏng, nhẹ...'
        },

      ]
    };
  }

  /** RENDER */
  render() {
    return (
      <div style={STYLE}>
        <div
          style={[
            COMPONENT.block.heading,
            COMPONENT.block.heading.headingWithoutViewMore
          ]}>
          <div style={COMPONENT.block.heading.line}></div>
          <div style={COMPONENT.block.heading.title}>
            Nhận xét về LixiBox
          </div>
        </div>
        <div
          style={[
            COMPONENT.block.content,
            LAYOUT.flexContainer.wrap,
            STYLE.list
          ]}>
          {
            this.state.feedbackList.map((feedback) =>
              <div key={feedback.id} style={STYLE.item}>
                <ItemFeedback data={feedback} />
              </div>
            )
          }
        </div>
        <div
          style={[
            COMPONENT.buttonBorder,
            STYLE.buttonMore
          ]}>
          Xem thêm Chuyên gia
          <i
            className="lx lx-14-angle-right"
            style={COMPONENT.buttonIcon}></i>
        </div>
      </div>
    );
  }
};

export default ContainerFeedback;
