/**
 * Created by Thành Võ on 13.03.2017
 */

import * as React from 'react';
import * as Radium from 'radium';

import ItemMagazine from '../../item/Magazine';
import * as VARIABLE from '../../../styles/variable.style';
import * as LAYOUT from '../../../styles/layout.style';
import * as COMPONENT from '../../../styles/component.style';

import STYLE from './ContainerAsideMagazine.style';

const MAGAZINE_THUMBNAIL_1 = require('../../../assets/images/sample-data/background-expert-1.jpg');
const MAGAZINE_THUMBNAIL_2 = require('../../../assets/images/sample-data/background-video-thumbnail.jpg');
const MAGAZINE_THUMBNAIL_3 = require('../../../assets/images/sample-data/avatar.jpg');

@Radium
class ContainerAsideMagazine extends React.Component<any, any> {
  constructor(props) {
    super(props);

    this.state = {
      magazineList: [
        {
          id: 1,
          title: ' Isehan Heroine Volume & Curl Mascara Super WP by Lixibox Online',
          href: '#',
          category: 'Làm đẹp',
          thumbnail: MAGAZINE_THUMBNAIL_1
        },
        {
          id: 2,
          title: 'Isehan Kiss Me Heroine Long & Curl Super Water Proof Mascara - Black',
          href: '#',
          category: 'Chăm sóc da',
          thumbnail: MAGAZINE_THUMBNAIL_2
        },
        {
          id: 3,
          title: ' Isehan Heroine Kiss me Eyeliner -  Brown',
          href: '#',
          category: 'Trang điểm',
          thumbnail: MAGAZINE_THUMBNAIL_3
        },
        {
          id: 4,
          title: ' Isehan Heroine Kiss me Eyeliner -  Black',
          href: '#',
          category: 'Làm đẹp',
          thumbnail: MAGAZINE_THUMBNAIL_1
        },
        {
          id: 5,
          title: 'THE SAEM COVER PERFECTION TIP CONCEALER -  01 Clear Beige',
          href: '#',
          category: 'Duỡng tóc',
          thumbnail: MAGAZINE_THUMBNAIL_2
        },
        {
          id: 6,
          title: 'L\'Oréal Voluminous Miss Manga Rock Mascara',
          href: '#',
          category: 'Làm đẹp',
          thumbnail: MAGAZINE_THUMBNAIL_3
        },
      ],
      magazineSlide: [],
      magazineSlideSelected: {},
      countChangeSlide: 0,
      animating: false
    };

    if (window.innerWidth >= VARIABLE.breakPoint960) {
      let _magazineSlide = [];
      let groupMagazine = {
        id: 0,
        list: []
      };

      this.state.magazineList.map((product, $index) => {
        groupMagazine.id = _magazineSlide.length;
        groupMagazine.list.push(product);

        if (groupMagazine.list.length === 2) {
          _magazineSlide.push(Object.assign({}, groupMagazine));
          groupMagazine.list = [];
        }
      });

      this.state.magazineSlide = _magazineSlide;
      this.state.magazineSlideSelected = _magazineSlide[0];
    }
  }

  navSlide(_direction) {
    let newIndexValue = 'left' === _direction ? -1 : 1;
    newIndexValue += this.state.countChangeSlide;

    newIndexValue = newIndexValue === this.state.magazineSlide.length
      ? 0
      : (
        newIndexValue === -1
          ? this.state.magazineSlide.length - 1
          : newIndexValue
      );

    this.selectSlide(newIndexValue);
  }

  selectSlide(_index) {
    this.setState((prevState, props) => ({
      animating: true
    }));

    setTimeout(() => {
      this.setState((prevState, props) => ({
        countChangeSlide: _index,
        magazineSlideSelected: prevState.magazineSlide[_index],
        animating: false
      }));
    }, 300);
  }

  /** RENDER */
  render() {
    return (
      <div style={STYLE}>
        <div style={STYLE.title}>Lixibox Magazine</div>
        {
          window.innerWidth < VARIABLE.breakPoint960
            ? /** MOBILE VERSION < 960 */
            <div style={[LAYOUT.flexContainer.wrap, STYLE.list]}>
              {
                this.state.magazineList.map((magazine) =>
                  <div key={`magazine-${magazine.id}`} style={STYLE.list.item}>
                    <ItemMagazine data={magazine} />
                  </div>
                )
              }
            </div>
            : /** DESKTOP VERSION >= 960 */
            <div style={STYLE.magazineSlide}>

              {/** List magazine */}
              <div
                style={[
                  LAYOUT.flexContainer.wrap,
                  STYLE.list,
                  true === this.state.animating && STYLE.magazineSlide.container.animate
                ]}>
                {
                  this.state.magazineSlideSelected.list.map((magazine) =>
                    <div key={`magazine-${magazine.id}`} style={STYLE.list.item}>
                      <ItemMagazine data={magazine} />
                    </div>
                  )
                }
              </div>

              {/** Pagination */}
              <div style={[LAYOUT.flexContainer.center, COMPONENT.slidePagination]}>
                {
                  this.state.magazineSlide.map((item, $index) =>
                    <div
                      key={`video-${item.id}`}
                      onClick={() => this.selectSlide($index)}
                      style={[
                        COMPONENT.slidePagination.item,
                        $index === this.state.countChangeSlide && COMPONENT.slidePagination.itemActive
                      ]}></div>
                  )
                }
              </div>
            </div>
        }
      </div>
    );
  }
};

export default ContainerAsideMagazine;
