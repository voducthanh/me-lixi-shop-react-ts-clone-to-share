/**
 * Created by Thành Võ on 13.03.2017
 */

import * as VARIABLE from '../../../styles/variable.style';
import MEDIA_QUERIES from '../../../styles/mediaQueries.style';

export default {
  display: 'block',
  backgroundColor: VARIABLE.colorPinkHightLight,

  title: {
    width: '100%',
    fontSize: 16,
    lineHeight: '40px',
    textAlign: 'center',
    textTransform: 'uppercase',
    padding: '10px 0',
    fontFamily: VARIABLE.fontAvenirDemiBold,
    color: VARIABLE.color4D,

    [MEDIA_QUERIES.tablet960]: {
      fontSize: 18,
    }
  },

  list: {
    padding: '0 10px',

    item: {
      width: '100%',
      padding: '0 10px 20px',

      [MEDIA_QUERIES.mobile610]: {
        width: '50%',
      },

      [MEDIA_QUERIES.tablet960]: {
        width: '100%',
      }
    },
  },

  magazineSlide: {
    position: 'relative',
    paddingBottom: 30,

    container: {
      transition: VARIABLE.transitionOpacity,
      opacity: 1,

      animate: {
        opacity: 0.5
      }
    },
  }
};
