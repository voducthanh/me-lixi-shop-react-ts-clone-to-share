/**
 * Created by Thành Võ on 29.03.2017
 */

import * as VARIABLE from '../../../styles/variable.style';
import MEDIA_QUERIES from '../../../styles/mediaQueries.style';

export default {
  display: 'block',
  textAlign: 'center',

  content: {
    padding: '20px 0 0',
    marginBottom: '30px',

    item: {
      width: '33.33%',
      marginBottom: 20,

      [MEDIA_QUERIES.mobile610]: {
        width: '25%',
      },

      [MEDIA_QUERIES.tablet960]: {
        width: '16.667%',
      }
    },
  },

  buttonMore: {
    marginBottom: '30px'
  },

  customStyleLoading: {
    height: 300
  }
};
