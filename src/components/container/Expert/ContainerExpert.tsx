/**
 * Created by Thành Võ on 29.03.2017
 */

import * as React from 'react';
import * as Radium from 'radium';
const connect = require('react-redux').connect;

import Loading from '../../general/UI/Loading';
import ItemExpert from '../../item/Expert';

import {
  fetchListExpertAction
} from '../../../actions/expert.action';

import * as VARIABLE from '../../../styles/variable.style';
import * as COMPONENT from '../../../styles/component.style';
import * as LAYOUT from '../../../styles/layout.style';

import STYLE from './ContainerExpert.style';

const EXPERT_THUMBNAIL_1 = require('../../../assets/images/sample-data/background-expert-1.jpg');
const EXPERT_THUMBNAIL_2 = require('../../../assets/images/sample-data/background-video-thumbnail.jpg');
const EXPERT_THUMBNAIL_3 = require('../../../assets/images/sample-data/avatar.jpg');

/**  PROPS & STATE INTERFACE */
interface IContainerExpert extends React.Props<any> {
  listExpert: any;
  fetchListExpext: () => void;
}

function mapStateToProps(state) {
  return {
    listExpert: state.expert.get('listExpert').toJS(),
  };
}

function mapDispatchToProps(dispatch) {
  return {
    fetchListExpext: (): void => dispatch(fetchListExpertAction()),
  };
}

@Radium
class ContainerExpert extends React.Component<IContainerExpert, any> {
  constructor(props: IContainerExpert) {
    super(props);

    this.state = {};
  }

  componentWillMount() {
    this.initData();
  }


  /**
   * Init data for list expert
   * - if without data: dispatch action to get
   * - if exsit data from state: nothing action
   */

  initData() {
    const {
      listExpert,
      fetchListExpext
    } = this.props;

    /** FETCH DATA IF WITHOUT */
    'undefined' === typeof listExpert.most_revenue_experts && fetchListExpext();
  }

  renderListExpert() {
    const { listExpert: { most_revenue_experts } } = this.props;

    return (
      <div>
        <div
          style={[
            LAYOUT.flexContainer.wrap,
            LAYOUT.flexContainer.justify,
            STYLE.content
          ]}>
          {
            most_revenue_experts.map((expert, $index) =>
              $index <= 11 &&
              <div
                key={`expert-${expert.id}`}
                style={STYLE.content.item}>
                <ItemExpert expert={expert} />
              </div>
            )
          }
        </div>
        <div
          key="button-more-expert"
          style={[
            COMPONENT.buttonBorder,
            STYLE.buttonMore
          ]}>
          Xem thêm Chuyên gia
          <i
            className="lx lx-14-angle-right"
            style={COMPONENT.buttonIcon}></i>
        </div>
      </div>
    );
  }

  render() {
    const { listExpert } = this.props;

    return (
      <div style={STYLE}>
        <div style={COMPONENT.block.heading}>
          <div style={COMPONENT.block.heading.line}></div>
          <div style={COMPONENT.block.heading.title}>
            Thiết kế bởi Chuyên gia
          </div>
        </div>

        {
          'undefined' === typeof listExpert.most_revenue_experts
            /** Loading Icon */
            ? <Loading customStyle={STYLE.customStyleLoading} />
            /** Show list data */
            : this.renderListExpert()
        }
      </div >
    );
  }
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(ContainerExpert);
