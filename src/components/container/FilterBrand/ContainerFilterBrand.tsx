/**
 * Created by Thành Võ on 13.03.2017
 */

import * as React from 'react';
import * as Radium from 'radium';

import ItemFilterBrand from '../../item/FilterBrand';
import ItemFilterPrice from '../../item/FilterPrice';

import STYLE from './ContainerFilterBrand.style';

@Radium
class ContainerFilterBrand extends React.Component<any, any> {
  constructor(props: any) {
    super(props);

    this.state = {
      mouseHover: false
    };
  }

  /** RENDER */
  render() {
    return (
      <div style={STYLE}>
        {/** Filter Brand */}
        <ItemFilterBrand closeFilterBrand={this.props.closeFilterBrand} />

        {/** Filter Price Range */}
        <ItemFilterPrice closeFilterPrice={this.props.closeFilterBrand} />
      </div>
    );
  }
};

export default ContainerFilterBrand;
