/**
 * Created by Thành Võ on 13.03.2017
 */

import * as VARIABLE from '../../../styles/variable.style';
import MEDIA_QUERIES from '../../../styles/mediaQueries.style';

export default {
  display: 'block',
  backgroundColor: VARIABLE.colorPinkHightLight,

  title: {
    width: '100%',
    fontSize: 16,
    lineHeight: '40px',
    textAlign: 'center',
    textTransform: 'uppercase',
    padding: '10px 0',
    fontFamily: VARIABLE.fontAvenirDemiBold,
    color: VARIABLE.color4D,

    [MEDIA_QUERIES.tablet960]: {
      fontSize: 18,
    }
  },

  list: {
    padding: '0 10px',

    [MEDIA_QUERIES.tablet960]: {
      padding: '0 30px 10px',
      borderBottom: `1px solid ${VARIABLE.colorBlack01}`,
    },

    item: {
      width: '50%',
      padding: '0 10px 5px',

      [MEDIA_QUERIES.tablet960]: {
        width: '100%',
        padding: '0 20px 5px',
      },
    },
  },

  videoSlide: {
    position: 'relative',
    paddingBottom: 30,

    container: {
      transition: VARIABLE.transitionOpacity,
      opacity: 1,

      animate: {
        opacity: 0.5
      }
    },

    pagination: {}
  },

  customStyleLoading: {
    height: 400
  }
};
