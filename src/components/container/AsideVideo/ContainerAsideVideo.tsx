/**
 * Created by Thành Võ on 27.03.2017
 */

import * as React from 'react';
import * as Radium from 'radium';
const connect = require('react-redux').connect;

import Loading from '../../general/UI/Loading';
import ItemVideo from '../../item/Video';

import { fetchListExpertVideoAction } from '../../../actions/expert.action';

import * as VARIABLE from '../../../styles/variable.style';
import * as COMPONENT from '../../../styles/component.style';
import * as LAYOUT from '../../../styles/layout.style';

import STYLE from './ContainerAsideVideo.style';

const VIDEO_THUMBNAIL_1 = require('../../../assets/images/sample-data/background-expert-1.jpg');
const VIDEO_THUMBNAIL_2 = require('../../../assets/images/sample-data/background-video-thumbnail.jpg');
const VIDEO_THUMBNAIL_3 = require('../../../assets/images/sample-data/avatar.jpg');

/**  PROPS & STATE INTERFACE */
interface IContainerAsideVideoProps extends React.Props<any> {
  listExpertVideo: Array<any>;
  fetchListExpextVideo: () => void;
};

function mapStateToProps(state) {
  return {
    listExpertVideo: state.expert.get('listExpertVideo').toJS(),
  };
}

function mapDispatchToProps(dispatch) {
  return {
    fetchListExpextVideo: (): void => dispatch(fetchListExpertVideoAction()),
  };
}

interface IContainerAsideVideoState {
  videoList: Array<any>;
  videoSlide: Array<any>;
  videoSlideSelected: any;
  countChangeSlide: number;
  animating: boolean;
};

@Radium
class ContainerAsideVideo extends React.Component<IContainerAsideVideoProps, any> {
  constructor(props: any) {
    super(props);

    this.state = {
      listExpertVideo: this.props.listExpertVideo,
      countChangeSlide: 0,
      animating: false
    };
  }

  componentWillMount() {
    const { listExpertVideo } = this.props;

    this.initData();
    this.initDataSlide();
  }

  /**
   * Init data for list expert
   * - if without data: dispatch action to get
   * - if exsit data from state: nothing action
   */

  initData() {
    const {
      listExpertVideo,
      fetchListExpextVideo
    } = this.props;

    /** FETCH DATA IF WITHOUT */
    0 === listExpertVideo.length && fetchListExpextVideo();
  }

  /**
   * Init data for slide
   * When : Component will mount OR Component will receive new props
   */
  initDataSlide(_newlistExpertVideo: Array<any> = this.state.listExpertVideo) {
    if (window.innerWidth >= VARIABLE.breakPoint960) {
      /**
       * On DESKTOP
       * Init data for product slide & product slide selected
       */
      let _videoSlide = [];
      let groupVideo = {
        id: 0,
        list: []
      };

      /** Assign data into each slider */
      _newlistExpertVideo.map((product, $index) => {
        groupVideo.id = _videoSlide.length;
        groupVideo.list.push(product);

        if (groupVideo.list.length === 2) {
          _videoSlide.push(Object.assign({}, groupVideo));
          groupVideo.list = [];
        }
      });

      this.setState({
        listExpertVideo: _newlistExpertVideo,
        videoSlide: _videoSlide,
        videoSlideSelected: _videoSlide[0]
      });
    } else {
      /**
       * On Mobile
       * Only init data for list product, not apply slide animation
       */
      this.setState({
        listExpertVideo: _newlistExpertVideo
      });
    }
  }

  /**
   * When component will receive new props -> init data for slide
   * @param nextProps prop from parent
   */
  componentWillReceiveProps(nextProps) {
    this.initDataSlide(nextProps.listExpertVideo);
  }

  /**
   * Navigate slide by button left or right
   * @param _direction `LEFT` or `RIGHT`
   * Will set new index value by @param _direction
   */
  navSlide(_direction) {
    const { countChangeSlide, videoSlide } = this.state;

    /**
     * If navigate to right: increase index value -> set +1 by countChangeSlide
     * If vavigate to left: decrease index value -> set -1 by countChangeSlide
     */

    let newIndexValue = 'left' === _direction ? -1 : 1;
    newIndexValue += countChangeSlide;

    /**
     * Validate new value in range [0, productSlide.length - 1]
     */
    newIndexValue = newIndexValue === videoSlide.length
      ? 0 /** If over max value -> set 0 */
      : (
        newIndexValue === -1
          ? videoSlide.length - 1 /** If under min value -> set productSlide.length - 1 */
          : newIndexValue
      );

    /** Change to new index value */
    this.selectSlide(newIndexValue);
  }

  /**
   * Change slide by set state and setTimeout for animation
   * @param _index new index value
   */
  selectSlide(_index) {
    /** Start animation */
    this.setState((prevState, props) => ({
      animating: true
    }));

    /** Change background */
    setTimeout(() => {
      this.setState((prevState, props) => ({
        countChangeSlide: _index,
        videoSlideSelected: prevState.videoSlide[_index],
        animating: false
      }));
    }, 300);
  }

  /**
   * Render content component only for mobile
   */
  renderMobileVersion() {
    const { listExpertVideo } = this.state;

    return (
      <div
        style={[
          LAYOUT.flexContainer.wrap,
          STYLE.list
        ]}>
        {
          listExpertVideo.map((video) =>
            <div
              key={`video-${video.id}`}
              style={STYLE.list.item}>
              <ItemVideo video={video} />
            </div>
          )
        }
      </div>
    );
  }

  /**
   * Render content component only for desktop
   */
  renderDesktopVersion() {
    const {
      animating,
      videoSlideSelected,
      videoSlide,
      countChangeSlide
    } = this.state;

    return (
      <div style={STYLE.videoSlide}>

        {/** List video */}
        <div
          style={[
            LAYOUT.flexContainer.wrap,
            STYLE.videoSlide.container,
            true === animating && STYLE.videoSlide.container.animate
          ]}>
          {
            videoSlideSelected.list.map((video) =>
              <div key={`video-${video.id}`} style={STYLE.list.item}>
                <ItemVideo video={video} />
              </div>
            )
          }
        </div>

        {/** Pagination */}
        <div
          style={[
            LAYOUT.flexContainer.center,
            COMPONENT.slidePagination,
            STYLE.videoSlide.pagination
          ]}>
          {
            videoSlide.map((item, $index) =>
              <div
                key={`video-${item.id}`}
                onClick={() => this.selectSlide($index)}
                style={[
                  COMPONENT.slidePagination.item,
                  $index === countChangeSlide
                  && COMPONENT.slidePagination.itemActive
                ]}></div>
            )
          }
        </div>
      </div>
    );
  }

  render() {
    const { listExpertVideo } = this.state;

    return (
      <div style={STYLE}>
        <div style={STYLE.title}>Video Tư vấn</div>
        {
          0 === listExpertVideo.length
            /** Loading Icon */
            ? <Loading customStyle={STYLE.customStyleLoading} />
            : /** Show data */
            (
              window.innerWidth < VARIABLE.breakPoint960
                ? /** MOBILE VERSION < 960 */
                this.renderMobileVersion()
                : /** DESKTOP VERSION >= 960 */
                this.renderDesktopVersion()
            )
        }
      </div >
    );
  }
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(ContainerAsideVideo);
