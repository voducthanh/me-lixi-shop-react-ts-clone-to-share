/**
 * Created by Thành Võ on 13.03.2017
 */

import * as React from 'react';
import * as Radium from 'radium';

import ItemProduct from '../../item/Product';
import Loading from '../../general/UI/Loading';

import * as VARIABLE from '../../../styles/variable.style';
import * as COMPONENT from '../../../styles/component.style';
import * as LAYOUT from '../../../styles/layout.style';

import STYLE from './ContainerProductSlide.style';

/**  PROPS & STATE INTERFACE */
interface IContainerProductSlideProps extends React.Props<any> {
  title?: string;
  column?: number;
  viewMore?: string;
  type?: string;
  data?: Array<any>;
};

interface IContainerProductSlideState {
  mouseHover: boolean;
  productList: Array<any>;
  productSlide: Array<any>;
  productSlideSelected: Object;
  countChangeSlide: number;
  animating: boolean;
};

@Radium
class ContainerProductSlide
  extends React.Component<IContainerProductSlideProps, any> {
  static defaultProps: IContainerProductSlideProps;

  constructor(props: IContainerProductSlideProps) {
    super(props);

    this.state = {
      mouseHover: false,
      productList: this.props.data,
      productSlide: [],
      productSlideSelected: {},
      countChangeSlide: 0,
      animating: false
    };
  }

  /**
   * When component will mount -> init data for slide
   */
  componentWillMount() {
    this.initDataSlide();
  }

  /**
   * Init data for slide
   * When : Component will mount OR Component will receive new props
   */
  initDataSlide(_newProductList: Array<any> = this.state.productList) {
    _newProductList = _newProductList || this.state.productList;

    if (window.innerWidth >= VARIABLE.breakPoint960) {
      /**
       * On DESKTOP
       * Init data for product slide & product slide selected
       */
      let _productSlide = [];
      let groupProduct = {
        id: 0,
        list: []
      };

      /** Assign data into each slider */
      _newProductList.map((product, $index) => {
        groupProduct.id = _productSlide.length;
        groupProduct.list.push(product);

        if (groupProduct.list.length === this.props.column) {
          _productSlide.push(Object.assign({}, groupProduct));
          groupProduct.list = [];
        }
      });

      this.setState({
        productList: _newProductList,
        productSlide: _productSlide,
        productSlideSelected: _productSlide[0] || {}
      });
    } else {
      /**
       * On Mobile
       * Only init data for list product, not apply slide animation
       */
      this.setState({
        productList: _newProductList
      });
    }
  }

  /**
   * When component will receive new props -> init data for slide
   * @param nextProps prop from parent
   */
  componentWillReceiveProps(nextProps) {
    this.initDataSlide(nextProps.data);
  }

  /**
   * Navigate slide by button left or right
   * @param _direction `LEFT` or `RIGHT`
   * Will set new index value by @param _direction
   */
  navSlide(_direction) {
    const { productSlide, countChangeSlide } = this.state;

    /**
     * If navigate to right: increase index value -> set +1 by countChangeSlide
     * If vavigate to left: decrease index value -> set -1 by countChangeSlide
     */
    let newIndexValue = 'left' === _direction ? -1 : 1;
    newIndexValue += countChangeSlide;

    /**
     * Validate new value in range [0, productSlide.length - 1]
     */
    newIndexValue = newIndexValue === productSlide.length
      ? 0 /** If over max value -> set 0 */
      : (
        newIndexValue === -1
          /** If under min value -> set productSlide.length - 1 */
          ? productSlide.length - 1
          : newIndexValue
      );

    /** Change to new index value */
    this.selectSlide(newIndexValue);
  }

  /**
   * Change slide by set state and setTimeout for animation
   * @param _index new index value
   */
  selectSlide(_index) {
    /** Start animation */
    this.setState((prevState, props) => ({
      animating: true
    }));

    /** Change background */
    setTimeout(() => {
      this.setState((prevState, props) => ({
        countChangeSlide: _index,
        productSlideSelected: prevState.productSlide[_index],
        animating: false
      }));
    }, 300);
  }

  /**
   * Render Heading component: For Mobile and Desktop
   */
  renderHeadingComponent() {
    const { title, viewMore, column, type } = this.props;
    const { productList } = this.state;

    return (
      <div style={COMPONENT.block.heading}>
        {/** Line */}
        <div style={COMPONENT.block.heading.line}></div>

        {/** Heading title */}
        <div style={COMPONENT.block.heading.title}>{title}</div>

        {/** View more */}
        <div style={COMPONENT.block.heading.viewMore}>
          {viewMore}
          <i
            className="lx lx-14-angle-right"
            style={COMPONENT.block.heading.viewMore.icon}></i>
        </div>
      </div>
    );
  }

  /**
   * Render content component only for mobile
   */
  renderMobileVersion() {
    const { column, type } = this.props;
    const { productList } = this.state;

    return (
      <div
        style={[
          LAYOUT.flexContainer.wrap,
          COMPONENT.block.content
        ]}>
        {
          /** List all product */
          productList.map((product) =>
            <div
              key={product.id}
              data={product}
              style={[
                STYLE.column3,
                3 === column && STYLE.column3,
                4 === column && STYLE.column4,
                5 === column && STYLE.column5,
              ]}>
              <ItemProduct data={product} type={type} />
            </div>
          )
        }
      </div>
    );
  }

  /**
   * Render content component only for desktop
   */
  renderDesktopVersion() {
    const { title, viewMore, column, type } = this.props;

    const {
      productList,
      productSlide,
      productSlideSelected,
      countChangeSlide,
      animating,
      mouseHover
    } = this.state;

    return (
      <div
        style={STYLE.productSlide}
        onMouseEnter={() => this.setState({ mouseHover: true })}
        onMouseLeave={() => this.setState({ mouseHover: false })}>

        {/** List product */}
        <div
          style={[
            LAYOUT.flexContainer.wrap,
            COMPONENT.block.content,
            STYLE.productSlide.container,
            true === animating && STYLE.productSlide.container.animate,
          ]}>
          {
            /** List product in each slide */
            productSlideSelected.list.map((product) =>
              <div
                key={product.id}
                data={product}
                style={[
                  STYLE.column3,
                  3 === column && STYLE.column3,
                  4 === column && STYLE.column4,
                  5 === column && STYLE.column5,
                ]}>
                <ItemProduct data={product} type={type} />
              </div>
            )
          }
        </div>

        {/** Pagination */}
        <div
          style={[
            LAYOUT.flexContainer.center,
            COMPONENT.slidePagination,
            STYLE.productSlide.pagination,
            mouseHover && STYLE.productSlide.pagination.active,
          ]}>
          {
            /** List pagiantion button */
            productSlide.map((item, $index) =>
              <div
                key={`banner-main-home-${item.id}`}
                onClick={() => this.selectSlide($index)}
                style={[
                  COMPONENT.slidePagination.item,
                  $index === countChangeSlide
                  && COMPONENT.slidePagination.itemActive
                ]}></div>
            )
          }
        </div>

        {/** Button Navigation */}
        <div>
          {/* Left navigation */}
          <div
            onClick={() => this.navSlide('left')}
            style={[
              LAYOUT.flexContainer.center,
              LAYOUT.flexContainer.verticalCenter,
              COMPONENT.slideNavigation,
              COMPONENT.slideNavigation.black,
              COMPONENT.slideNavigation.left,
              mouseHover && COMPONENT.slideNavigation.left.active
            ]}>
            <div
              className="lx lx-15-angle-left"
              style={COMPONENT.slideNavigation.icon}></div>
          </div>
          {/* Right navigation */}
          <div
            onClick={() => this.navSlide('right')}
            style={[
              LAYOUT.flexContainer.center,
              LAYOUT.flexContainer.verticalCenter,
              COMPONENT.slideNavigation,
              COMPONENT.slideNavigation.black,
              COMPONENT.slideNavigation.right,
              mouseHover && COMPONENT.slideNavigation.right.active
            ]}>
            <div
              className="lx lx-14-angle-right"
              style={COMPONENT.slideNavigation.icon}></div>
          </div>
        </div>
      </div>
    );
  }

  render() {
    const { title, viewMore, column, type } = this.props;
    const { productList } = this.state;

    return (
      <div style={STYLE}>
        {
          /** Render Heading component */
          this.renderHeadingComponent()
        }

        {
          0 === productList.length
            /** Loading Icon */
            ? <Loading customStyle={STYLE.customStyleLoading} />
            : /** Show data */
            (
              window.innerWidth < VARIABLE.breakPoint960
                ? /** MOBILE VERSION < 960 */
                this.renderMobileVersion()
                : /** DESKTOP VERSION >= 960 */
                this.renderDesktopVersion()
            )
        }
      </div>
    );
  }
};

ContainerProductSlide.defaultProps = {
  title: 'Danh sách Sản phẩm',
  viewMore: 'Xem thêm',
  column: 3,
  type: 'full',
  data: []
};

export default ContainerProductSlide;
