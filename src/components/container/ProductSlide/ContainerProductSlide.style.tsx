/**
 * Created by Thành Võ on 13.03.2017
 */

import * as VARIABLE from '../../../styles/variable.style';
import MEDIA_QUERIES from '../../../styles/mediaQueries.style';

export default {
  display: 'block',

  column3: {
    width: '50%',

    [MEDIA_QUERIES.tablet960]: {
      width: '33.333%',
    }
  },

  column4: {
    width: '50%',

    [MEDIA_QUERIES.tablet960]: {
      width: '25%',
    }
  },

  column5: {
    width: '50%',

    [MEDIA_QUERIES.tablet960]: {
      width: '20%',
    }
  },

  productSlide: {
    position: 'relative',
    overflow: 'hidden',

    container: {
      transition: VARIABLE.transitionOpacity,
      opacity: 1,

      animate: {
        opacity: 0.5
      }
    },

    pagination: {
      opacity: 0,
      bottom: 0,
      transition: VARIABLE.transitionNormal,

      active: {
        opacity: 1,
        bottom: 20,
      }
    },
    navigation: {},
  },

  customStyleLoading: {
    height: 300
  }
};
