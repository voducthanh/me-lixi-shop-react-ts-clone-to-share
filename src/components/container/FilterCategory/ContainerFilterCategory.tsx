/**
 * Created by Thành Võ on 13.03.2017
 */

import * as React from 'react';
import * as Radium from 'radium';

import ItemFilterCategory from '../../item/FilterCategory';

import * as VARIABLE from '../../../styles/variable.style';
import * as COMPONENT from '../../../styles/component.style';
import * as LAYOUT from '../../../styles/layout.style';

import STYLE from './ContainerFilterCategory.style';

@Radium
class ContainerFilterCategory extends React.Component<any, any> {
  constructor(props: any) {
    super(props);

    this.state = {
      mouseHover: false
    };
  }

  /** RENDER */
  render() {
    return (
      <div style={STYLE}>
        <ItemFilterCategory closeFilterCategory={this.props.closeFilterCategory} />
      </div>
    );
  }
};

export default ContainerFilterCategory;
