/**
 * Created by Thành Võ on 13.03.2017
 */

import * as VARIABLE from '../../../styles/variable.style';
import MEDIA_QUERIES from '../../../styles/mediaQueries.style';

export default {
  display: 'block',
  position: 'relative',
  zIndex: VARIABLE.zIndex5,

  column3: {
    width: '50%',

    [MEDIA_QUERIES.tablet960]: {
      width: '33.333%',
    }
  },

  column4: {
    width: '50%',

    [MEDIA_QUERIES.tablet960]: {
      width: '25%',
    }
  },

  column5: {
    width: '50%',

    [MEDIA_QUERIES.tablet960]: {
      width: '20%',
    }
  },

  blockContent: {
    padding: '0 0 10px',
  },

  statInfo: {
    width: '100%',
    marginBottom: 40,
    position: 'relative',
    zIndex: VARIABLE.zIndex9,
    padding: '0 10px',

    [MEDIA_QUERIES.tablet960]: {
      padding: 0,
    },

    count: {
      height: 32,
      lineHeight: '32px',
      fontSize: 12,
      color: VARIABLE.color4D,
      whiteSpace: 'nowrap',

      number: {
        color: VARIABLE.color2E,
        fontFamily: VARIABLE.fontAvenirDemiBold,
        marginRight: 5,
      }
    },

    sort: {
      position: 'relative',

      text: {
        height: 32,
        lineHeight: '32px',
        fontSize: 12,
        color: VARIABLE.color4D,
        paddingRight: 10,
        display: 'none',

        [MEDIA_QUERIES.mobile610]: {
          display: 'block'
        }
      },

      item: {
        cursor: 'pointer',
        color: VARIABLE.color4D,

        icon: {
          height: 30,
          width: 30,
          minWidth: 30,
          fontSize: 12,
          lineHeight: '30px',
          textAlign: 'center',
          color: 'inherit',
        },

        title: {
          height: 30,
          lineHeight: '30px',
          fontSize: 12,
          whiteSpace: 'nowrap',
          color: 'inherit',
          selected: {
            display: 'none',

            [MEDIA_QUERIES.mobile610]: {
              display: 'block'
            }
          }
        },
      },

      itemSelected: {
        border: `1px solid ${VARIABLE.colorD2}`,
        padding: 0,
        borderRadius: 3,
        transition: VARIABLE.transitionNormal,

        noBorder: {
          border: `1px solid ${VARIABLE.colorWhite}`,
        },

        [MEDIA_QUERIES.mobile610]: {
          padding: '0 15px 0 0'
        },
      },

      itemList: {
        padding: '0 20px 0 0',

        ':hover': {
          backgroundColor: VARIABLE.colorF7,
          color: VARIABLE.colorPink,
        }
      },

      list: {
        visibility: 'hidden',
        position: 'absolute',
        top: 42,
        right: 0,
        boxShadow: VARIABLE.shadowBlur,
        backgroundColor: VARIABLE.colorWhite,
        borderRadius: 3,
        padding: '12px 0',
        transition: VARIABLE.transitionTop,

        show: {
          visibility: 'visible',
          top: 32,
        }
      }
    },

    iconWithoutText: {
      border: `1px solid ${VARIABLE.colorD2}`,
      height: 32,
      width: 32,
      minWidth: 32,
      fontSize: 14,
      lineHeight: '32px',
      borderRadius: 3,

      ':hover': {
        background: VARIABLE.colorF7,
      },

      category: {
        marginRight: 10,
      },

      filter: {
        marginLeft: 10,
      }
    }
  }
};
