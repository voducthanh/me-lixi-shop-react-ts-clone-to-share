/**
 * Created by Thành Võ on 13.03.2017
 */

import * as React from 'react';
import * as Radium from 'radium';

import { fetchProductByCategoryAction } from '../../../actions/shop.action';

import ItemProduct from '../../item/Product';

import * as VARIABLE from '../../../styles/variable.style';
import * as COMPONENT from '../../../styles/component.style';
import * as LAYOUT from '../../../styles/layout.style';

import STYLE from './ContainerProductList.style';

const PRODUCT_THUMBNAIL_1 = require('../../../assets/images/sample-data/product-thumbnail-1.jpg');
const PRODUCT_THUMBNAIL_2 = require('../../../assets/images/sample-data/product-thumbnail-2.png');
const PRODUCT_THUMBNAIL_3 = require('../../../assets/images/sample-data/product-thumbnail-3.png');
const PRODUCT_THUMBNAIL_4 = require('../../../assets/images/sample-data/product-thumbnail-4.png');
const PRODUCT_THUMBNAIL_5 = require('../../../assets/images/sample-data/product-thumbnail-5.png');
const PRODUCT_THUMBNAIL_6 = require('../../../assets/images/sample-data/product-thumbnail-6.jpg');

interface IContainerProductListProps extends React.Props<any> {
  categoryFilter: any;
  title: string;
  column: number;
  viewMore?: string;
  type?: string;
  showCategory: () => void;
  showFilter: () => void;
};

@Radium
class ContainerProductList extends React.Component<IContainerProductListProps, any> {
  static defaultProps: IContainerProductListProps;

  constructor(props) {
    super(props);

    console.log('this.props.categoryFilter', this.props.categoryFilter);
    fetchProductByCategoryAction(this.props.categoryFilter);

    this.state = {
      productList: [
        {
          id: 1,
          title: ' Isehan Heroine Volume & Curl Mascara Super WP by Lixibox Online',
          href: '#',
          price: {
            current: '280,000 đ',
            old: '310,000 đ',
            sale: '-11%'
          },
          thumbnail: PRODUCT_THUMBNAIL_1
        },
        {
          id: 2,
          title: 'Isehan Kiss Me Heroine Long & Curl Super Water Proof Mascara - Black',
          href: '#',
          price: {
            current: '280,000 đ',
            old: '310,000 đ',
            sale: '-10%'
          },
          thumbnail: PRODUCT_THUMBNAIL_2
        },
        {
          id: 3,
          title: ' Isehan Heroine Kiss me Eyeliner -  Brown',
          href: '#',
          price: {
            current: '280,000 đ',
            old: '310,000 đ',
            sale: '-20%'
          },
          thumbnail: PRODUCT_THUMBNAIL_3
        },
        {
          id: 4,
          title: ' Isehan Heroine Kiss me Eyeliner -  Black',
          href: '#',
          price: {
            current: '280,000 đ',
            old: '310,000 đ',
            sale: '-30%'
          },
          thumbnail: PRODUCT_THUMBNAIL_4
        },
        {
          id: 5,
          title: 'THE SAEM COVER PERFECTION TIP CONCEALER -  01 Clear Beige',
          href: '#',
          price: {
            current: '280,000 đ',
            old: '310,000 đ',
            sale: '-12%'
          },
          thumbnail: PRODUCT_THUMBNAIL_5
        },
        {
          id: 6,
          title: 'L\'Oréal Voluminous Miss Manga Rock Mascara',
          href: '#',
          price: {
            current: '280,000 đ',
            old: '310,000 đ',
            sale: '-19%'
          },
          thumbnail: PRODUCT_THUMBNAIL_6
        },
        {
          id: 7,
          title: 'W7 In The Buff Natural Nudes Eye Colour Palette',
          href: '#',
          price: {
            current: '280,000 đ',
            old: '310,000 đ',
            sale: '-27%'
          },
          thumbnail: PRODUCT_THUMBNAIL_1
        },
        {
          id: 8,
          title: 'Dior Addict Lip Glow - 004 Coral',
          href: '#',
          price: {
            current: '280,000 đ',
            old: '310,000 đ',
            sale: '-11%'
          },
          thumbnail: PRODUCT_THUMBNAIL_2
        },
        {
          id: 9,
          title: 'Dior Addict Lip Glow - 001 Pink',
          href: '#',
          price: {
            current: '280,000 đ',
            old: '310,000 đ',
            sale: '-17%'
          },
          thumbnail: PRODUCT_THUMBNAIL_3
        },
        {
          id: 10,
          title: 'Espoir The Knit Nowear M Lipstick -  Modest',
          href: '#',
          price: {
            current: '280,000 đ',
            old: '310,000 đ',
            sale: '-1%'
          },
          thumbnail: PRODUCT_THUMBNAIL_4
        },
        {
          id: 11,
          title: 'THE SAEM COVER PERFECTION TIP CONCEALER -  01 Clear Beige',
          href: '#',
          price: {
            current: '280,000 đ',
            old: '310,000 đ',
            sale: '-12%'
          },
          thumbnail: PRODUCT_THUMBNAIL_5
        },
        {
          id: 12,
          title: 'Dior Addict Lip Glow - 001 Pink',
          href: '#',
          price: {
            current: '280,000 đ',
            old: '310,000 đ',
            sale: '-21%'
          },
          thumbnail: PRODUCT_THUMBNAIL_6
        },
        {
          id: 13,
          title: ' Isehan Heroine Volume & Curl Mascara Super WP by Lixibox Online',
          href: '#',
          price: {
            current: '280,000 đ',
            old: '310,000 đ',
            sale: '-11%'
          },
          thumbnail: PRODUCT_THUMBNAIL_1
        },
        {
          id: 14,
          title: 'Isehan Kiss Me Heroine Long & Curl Super Water Proof Mascara - Black',
          href: '#',
          price: {
            current: '280,000 đ',
            old: '310,000 đ',
            sale: '-10%'
          },
          thumbnail: PRODUCT_THUMBNAIL_2
        },
        {
          id: 15,
          title: ' Isehan Heroine Kiss me Eyeliner -  Brown',
          href: '#',
          price: {
            current: '280,000 đ',
            old: '310,000 đ',
            sale: '-20%'
          },
          thumbnail: PRODUCT_THUMBNAIL_3
        },
        {
          id: 16,
          title: ' Isehan Heroine Kiss me Eyeliner -  Black',
          href: '#',
          price: {
            current: '280,000 đ',
            old: '310,000 đ',
            sale: '-30%'
          },
          thumbnail: PRODUCT_THUMBNAIL_4
        },
        {
          id: 17,
          title: 'THE SAEM COVER PERFECTION TIP CONCEALER -  01 Clear Beige',
          href: '#',
          price: {
            current: '280,000 đ',
            old: '310,000 đ',
            sale: '-12%'
          },
          thumbnail: PRODUCT_THUMBNAIL_5
        },
        {
          id: 18,
          title: 'L\'Oréal Voluminous Miss Manga Rock Mascara',
          href: '#',
          price: {
            current: '280,000 đ',
            old: '310,000 đ',
            sale: '-19%'
          },
          thumbnail: PRODUCT_THUMBNAIL_6
        },
        {
          id: 19,
          title: 'W7 In The Buff Natural Nudes Eye Colour Palette',
          href: '#',
          price: {
            current: '280,000 đ',
            old: '310,000 đ',
            sale: '-27%'
          },
          thumbnail: PRODUCT_THUMBNAIL_1
        },
        {
          id: 20,
          title: 'Dior Addict Lip Glow - 004 Coral',
          href: '#',
          price: {
            current: '280,000 đ',
            old: '310,000 đ',
            sale: '-11%'
          },
          thumbnail: PRODUCT_THUMBNAIL_2
        },
      ],
      sortList: [
        {
          id: 1,
          title: 'Mới nhất',
          class: 'lx lx-32-clock',
          selected: true,
        },
        {
          id: 2,
          title: 'Giá tăng dần',
          class: 'lx lx-33-arrow-up',
          selected: false,
        },
        {
          id: 3,
          title: 'Giá giảm dần',
          class: 'lx lx-34-arrow-down',
          selected: false,
        }
      ],
      hoverSort: false
    };
  }

  selectSort(_sort) {
    this.setState((prevState, props) => ({
      sortList: prevState.sortList.map((sort) => {
        sort.selected = sort.id === _sort.id;
        return sort;
      }),
      hoverSort: false
    }));
  }

  /** RENDER */
  render() {
    return (
      <div style={STYLE}>
        {/** 1. Heading block: Title without view more */}
        <div
          style={[
            COMPONENT.block.heading,
            COMPONENT.block.heading.headingWithoutViewMore
          ]}>
          <div style={COMPONENT.block.heading.line}></div>
          <div style={COMPONENT.block.heading.title}>
            {this.props.title}
          </div>
        </div>

        {/** 2. Content: Start info & product list */}
        < div
          style={[
            LAYOUT.flexContainer.wrap,
            COMPONENT.block.content,
            STYLE.blockContent
          ]}>

          {/** 2.1. Stat imfo: count product & sort product */}
          <div style={[LAYOUT.flexContainer.justify, STYLE.statInfo]}>

            {/** LEFT */}
            <div style={LAYOUT.flexContainer.left}>
              {/** 2.1.0. Category */}
              {
                window.innerWidth < VARIABLE.breakPoint960 &&
                <div
                  key={'icon-category'}
                  className="lx lx-35-category"
                  onClick={this.props.showCategory}
                  style={[
                    STYLE.statInfo.iconWithoutText,
                    STYLE.statInfo.iconWithoutText.category,
                  ]}></div>
              }

              {/** 2.1.1. Count product */}
              <div style={STYLE.statInfo.count}>
                <span style={STYLE.statInfo.count.number}>
                  120
                </span>
                Sản phẩm
              </div>
            </div>

            {/** RIGHT */}
            <div style={LAYOUT.flexContainer.right}>
              {/** 2.1.2. Sort product*/}
              <div
                style={[
                  LAYOUT.flexContainer.right,
                  STYLE.statInfo.sort
                ]}
                onTouchStart={() => this.setState({ hoverSort: true })}
                onTouchEnd={() => this.setState({ hoverSort: false })}
                onMouseEnter={() => this.setState({ hoverSort: true })}
                onMouseLeave={() => this.setState({ hoverSort: false })}>
                <div
                  style={STYLE.statInfo.sort.text}>
                  Sắp xếp theo:
                </div>


                {/** 2.1.2.1 Sort selected */}
                {
                  this.state.sortList.map(sort =>
                    true === sort.selected &&
                    <div
                      key={`sort-selected-${sort.id}`}
                      style={[
                        LAYOUT.flexContainer.right,
                        STYLE.statInfo.sort.item,
                        STYLE.statInfo.sort.itemSelected,
                        this.state.hoverSort && STYLE.statInfo.sort.itemSelected.noBorder
                      ]}>
                      <i
                        className={sort.class}
                        style={STYLE.statInfo.sort.item.icon}></i>
                      <div
                        style={[
                          STYLE.statInfo.sort.item.title,
                          STYLE.statInfo.sort.item.title.selected
                        ]}>
                        {sort.title}
                      </div>
                    </div>
                  )
                }
                {/** 2.1.2.1 Sort in list */}
                <div
                  style={[
                    STYLE.statInfo.sort.list,
                    this.state.hoverSort && STYLE.statInfo.sort.list.show
                  ]}>
                  {
                    this.state.sortList.map(sort =>
                      false === sort.selected &&
                      <div
                        key={`sort-list-${sort.id}`}
                        style={[
                          LAYOUT.flexContainer.left,
                          STYLE.statInfo.sort.item,
                          STYLE.statInfo.sort.itemList,
                        ]}
                        onClick={() => this.selectSort(sort)}>
                        <i className={sort.class} style={STYLE.statInfo.sort.item.icon}></i>
                        <div style={STYLE.statInfo.sort.item.title}>{sort.title}</div>
                      </div>
                    )
                  }
                </div>
              </div>

              {/** 2.1.3. Filter */}
              {
                window.innerWidth < VARIABLE.breakPoint960 &&
                <div
                  key={'icon-filter'}
                  className="lx lx-36-filter"
                  onClick={this.props.showFilter}
                  style={[
                    STYLE.statInfo.iconWithoutText,
                    STYLE.statInfo.iconWithoutText.filter,
                  ]}></div>
              }
            </div>

          </div>

          {/** 2.2. Product list*/}
          {
            this.state.productList.map((product) =>
              <div
                key={product.id}
                data={product}
                style={[
                  STYLE.column3,
                  3 === this.props.column && STYLE.column3,
                  4 === this.props.column && STYLE.column4,
                  5 === this.props.column && STYLE.column5,
                ]}>
                <ItemProduct data={product} type={this.props.type} />
              </div>
            )
          }
        </div>
      </div>
    );
  }
};

ContainerProductList.defaultProps = {
  categoryFilter: '',
  title: 'Danh sách Sản phẩm',
  viewMore: 'Xem thêm',
  column: 3,
  type: 'product',
  showCategory: () => { },
  showFilter: () => { }
};

export default ContainerProductList;
