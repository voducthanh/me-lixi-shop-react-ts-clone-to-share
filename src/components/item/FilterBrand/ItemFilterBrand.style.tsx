/**
 * Created by Thành Võ on 27.03.2017
 */

import * as VARIABLE from '../../../styles/variable.style';
import MEDIA_QUERIES from '../../../styles/mediaQueries.style';

export default {
  position: 'relative',

  brandList: {
    paddingLeft: 20,
    paddingRight: 20,

    [MEDIA_QUERIES.tablet1024]: {
      paddingLeft: 0,
      paddingRight: 0,
    }
  },

  brandItem: {
    marginBottom: 10,
    width: '50%',
    cursor: 'pointer',
    paddingRight: 10,

    icon: {
      width: 16,
      minWidth: 16,
      height: 16,
      borderRadius: 3,
      border: `1px solid ${VARIABLE.color97}`,
      marginRight: 10,
      transition: VARIABLE.transitionBackground,
      position: 'relative',
      backgroundColor: VARIABLE.colorWhite,

      selected: {
        backgroundColor: VARIABLE.colorPink,
        border: `1px solid ${VARIABLE.colorPink}`,
      },

      firstCheck: {
        position: 'absolute',
        width: 6,
        height: 2,
        backgroundColor: VARIABLE.colorWhite,
        borderRadius: 2,
        transform: 'rotate(45deg)',
        top: 8,
        left: 1,
      },

      lastCheck: {
        position: 'absolute',
        width: 10,
        height: 2,
        borderRadius: 2,
        backgroundColor: VARIABLE.colorWhite,
        transform: 'rotate(-45deg)',
        top: 6,
        left: 4,
      },
    },

    title: {
      lineHeight: '18px',
      fontSize: 14,
      color: VARIABLE.color4D,
      transition: VARIABLE.transitionColor,

      ':hover': {
        color: VARIABLE.colorPink
      }
    }
  }
};
