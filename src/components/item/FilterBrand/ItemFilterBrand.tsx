/**
 * Created by Thành Võ on 13.03.2017
 */

import * as React from 'react';
import * as Radium from 'radium';

import * as VARIABLE from '../../../styles/variable.style';
import * as LAYOUT from '../../../styles/layout.style';
import * as COMPONENT from '../../../styles/component.style';

import STYLE from './ItemFilterBrand.style';

/**  PROPS & STATE INTERFACE */
interface IItemFilterBrandProps extends React.Props<any> {
  closeFilterBrand: () => void;
};

interface IItemFilterBrandState {
  brandList: Array<any>;
};

@Radium
class ItemFilterBrand extends React.Component<IItemFilterBrandProps, any> {
  constructor(props) {
    super(props);

    this.state = {
      brandList: [
        {
          id: 1,
          title: 'Missha',
          selected: true
        },
        {
          id: 2,
          title: 'Whamissa',
          selected: true
        },
        {
          id: 3,
          title: 'Adalout Natural',
          selected: true
        },
        {
          id: 4,
          title: 'Ciracle',
          selected: false
        },
        {
          id: 5,
          title: 'Corsx Adalout Natural',
          selected: false
        },
        {
          id: 6,
          title: 'EOS',
          selected: false
        },
        {
          id: 7,
          title: 'Clinque',
          selected: false
        },
        {
          id: 8,
          title: 'Simple Skin',
          selected: false
        },
        {
          id: 9,
          title: 'Coach',
          selected: false
        },
        {
          id: 10,
          title: 'LV',
          selected: false
        },
        {
          id: 11,
          title: 'Secret Key',
          selected: false
        },
        {
          id: 12,
          title: 'Lovely',
          selected: false
        },

      ]
    };
  }

  selectBarnd(_brand) {
    this.setState((prevState, props) => ({
      brandList: prevState.brandList.map((brand) => {
        brand.selected =
          brand.id === _brand.id
            ? !brand.selected
            : brand.selected;
        return brand;
      }),
    }));
  }

  closeFilterBrand() {
    this.setState((prevState, props) => ({
      brandList: prevState.brandList.map((brand) => {
        brand.selected = false;
        return brand;
      })
    }));
    this.props.closeFilterBrand();
  }

  checkShowClose() {
    return window.innerWidth < VARIABLE.breakPoint960
      || this.state.brandList.filter(brand => brand.selected).length > 0;
  }

  render() {
    const { brandList } = this.state;

    return (
      <div
        style={[
          COMPONENT.asideBlock,
          STYLE
        ]}>

        {/** 1. Heading */}
        <div style={COMPONENT.asideBlock.heading}>
          <div style={COMPONENT.asideBlock.heading.text}>
            Thuơng hiệu
          </div>
          {
            true === this.checkShowClose() &&
            <div
              key={`close-filter-brand`}
              style={COMPONENT.asideBlock.heading.close}
              className="lx lx-31-close"
              onClick={() => this.closeFilterBrand()}></div>
          }
        </div>

        {/** 2. Content */}
        <div
          style={[
            LAYOUT.flexContainer.wrap,
            COMPONENT.asideBlock.content,
            STYLE.brandList
          ]}>
          {
            brandList.map(brand =>
              <div
                onClick={() => this.selectBarnd(brand)}
                key={`filter-brand-${brand.id}`}
                style={[
                  LAYOUT.flexContainer.left,
                  STYLE.brandItem
                ]}>
                <div
                  style={[
                    STYLE.brandItem.icon,
                    brand.selected && STYLE.brandItem.icon.selected
                  ]}>
                  <div style={STYLE.brandItem.icon.firstCheck}></div>
                  <div style={STYLE.brandItem.icon.lastCheck}></div>
                </div>
                <div
                  key={`filter-brand-title-${brand.id}`}
                  style={STYLE.brandItem.title}>
                  {brand.title}
                </div>
              </div>
            )
          }
        </div>
      </div>
    );
  }
};

export default ItemFilterBrand;
