/**
 * Created by Thành Võ on 13.03.2017
 */

import * as React from 'react';
import * as Radium from 'radium';

import * as VARIABLE from '../../../styles/variable.style';
import * as LAYOUT from '../../../styles/layout.style';
import * as COMPONENT from '../../../styles/component.style';

import STYLE from './ItemFilterCategory.style';

/**  PROPS & STATE INTERFACE */
interface IItemFilterCategoryProps extends React.Props<any> {
  closeFilterCategory: () => void;
};

interface IItemFilterCategoryState {
  menuList: Array<any>;
};

@Radium
class ItemFilterCategory extends React.Component<IItemFilterCategoryProps, any> {
  constructor(props) {
    super(props);

    this.state = {
      menuList: [
        {
          id: 1,
          title: 'Beauty',
          description: 'Mua lẻ',
          href: '#',
          open: true,
          sub: [
            {
              id: 2,
              title: 'Makeup',
              description: 'Trang điểm',
              href: '#',
              open: false,
              sub: [
                {}
              ]
            },
            {
              id: 3,
              title: 'Skin Care',
              description: 'Chăm sóc da',
              href: '#',
              open: true,
              sub: [
                {
                  id: 8,
                  title: 'Mask',
                  description: 'Mặt nạ',
                  href: '#',
                  open: false,
                  sub: [
                    {}
                  ]
                },
                {
                  id: 9,
                  title: 'Cleanse',
                  description: 'Rửa mặt',
                  href: '#',
                  open: false,
                  sub: [
                    {}
                  ]
                },
                {
                  id: 10,
                  title: 'Moisturize',
                  description: 'Duỡng ẩm',
                  href: '#',
                  open: true,
                  sub: [
                    {
                      id: 15,
                      title: 'BB/CC/DD Cream',
                      description: '',
                      href: '#',
                      open: false,
                      sub: []
                    },
                    {
                      id: 16,
                      title: 'Night Cream',
                      description: 'kem duỡng',
                      href: '#',
                      open: false,
                      sub: []
                    },
                    {
                      id: 17,
                      title: 'New Elmusion / Lotion / Moisturize Cream',
                      description: 'Sữa duỡng',
                      href: '#',
                      open: false,
                      sub: []
                    },
                    {
                      id: 18,
                      title: 'Gel',
                      description: 'Gel duỡng',
                      href: '#',
                      open: false,
                      sub: []
                    },
                    {
                      id: 19,
                      title: 'Face oil',
                      description: 'Dầu duỡng',
                      href: '#',
                      open: false,
                      sub: []
                    },
                    {
                      id: 20,
                      title: 'Neck Cream',
                      description: 'Duỡng da vùng cổ',
                      href: '#',
                      open: false,
                      sub: []
                    },
                    {
                      id: 21,
                      title: 'Face Mist',
                      description: 'Xịt khoáng',
                      href: '#',
                      open: false,
                      sub: []
                    },
                  ]
                },
                {
                  id: 11,
                  title: 'Treat',
                  description: 'Trị liệu',
                  href: '#',
                  open: false,
                  sub: [
                    {}
                  ]
                },
                {
                  id: 12,
                  title: 'Sun protection',
                  description: 'Kem chống nắng',
                  href: '#',
                  open: false,
                  sub: []
                },
                {
                  id: 13,
                  title: 'Lip Treatmment',
                  description: 'Duỡng môi',
                  href: '#',
                  open: false,
                  sub: []
                },
                {
                  id: 14,
                  title: 'New Skin Care',
                  description: 'Duỡng da mới nhất',
                  href: '#',
                  open: false,
                  sub: []
                },
              ]
            },
            {
              id: 4,
              title: 'Bath & Body',
              description: 'Cơ thể',
              href: '#',
              open: false,
              sub: [
                {}
              ]
            },
            {
              id: 5,
              title: 'Tool & Accessories',
              description: 'Cọ & Phụ kiện',
              href: '#',
              open: false,
              sub: [
                {}
              ]
            },
            {
              id: 6,
              title: 'New Arrivals',
              description: 'Hàng mới nhất',
              href: '#',
              open: false,
              sub: []
            },
            {
              id: 7,
              title: 'Best Sallers',
              description: 'Hàng hot nhất',
              href: '#',
              open: false,
              sub: []
            },
          ]
        }
      ]
    };
  }

  renderMenuItem(_menu) {
    return (
      <div
        key={`filte-menu-${_menu.id}`}
        style={STYLE.menuItem}>
        <div style={[LAYOUT.flexContainer.left, STYLE.menuItem.title]}>
          <div
            className={
              _menu.sub.length > 0
                ? (true === _menu.open ? 'lx lx-12-angle-down' : 'lx lx-14-angle-right')
                : ''
            }
            style={STYLE.menuItem.title.icon}></div>
          <div style={STYLE.menuItem.title.text}>
            <div
              key={`menu-text-main-${_menu.id}`}
              style={STYLE.menuItem.title.text.main}>
              {_menu.title}
            </div>
            {
              '' !== _menu.description &&
              <div style={STYLE.menuItem.title.text.sub}>{_menu.description}</div>
            }
          </div>
        </div>
        {
          _menu.sub.length > 0 && true === _menu.open &&
          <div style={STYLE.subItem}>
            {
              _menu.sub.map(sub => this.renderMenuItem(sub))
            }
          </div>
        }
      </div>
    );
  }

  render() {
    const { menuList } = this.state;
    const { closeFilterCategory } = this.props;

    return (
      <div
        style={[COMPONENT.asideBlock, STYLE]}
        onMouseEnter={() => this.setState({ hoverProduct: true })}
        onMouseLeave={() => this.setState({ hoverProduct: false })}>

        {/** 1. Heading */}
        <div style={COMPONENT.asideBlock.heading}>
          <div style={COMPONENT.asideBlock.heading.text}>
            Tìm theo Danh mục
          </div>
          <div
            key={`close-filter-category`}
            style={[
              COMPONENT.asideBlock.heading.close,
              STYLE.iconClose
            ]}
            className="lx lx-31-close"
            onClick={closeFilterCategory}></div>
        </div>

        {/** 2. Content */}
        <div
          style={[
            COMPONENT.asideBlock.content,
            STYLE.menuList
          ]}>
          {
            menuList.map(menu => this.renderMenuItem(menu))
          }
        </div>
      </div>
    );
  }
};

export default ItemFilterCategory;
