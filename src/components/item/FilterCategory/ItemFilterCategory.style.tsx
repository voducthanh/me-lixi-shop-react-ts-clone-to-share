/**
 * Created by Thành Võ on 27.03.2017
 */

import * as VARIABLE from '../../../styles/variable.style';
import MEDIA_QUERIES from '../../../styles/mediaQueries.style';

export default {
  display: 'block',
  position: 'relative',

  iconClose: {
    display: 'block',

    [MEDIA_QUERIES.tablet960]: {
      display: 'none',
    },
  },

  menuList: {
    paddingLeft: 15,
    paddingRight: 15,

    [MEDIA_QUERIES.tablet1024]: {
      paddingLeft: 0,
      paddingRight: 0,
    }
  },

  menuItem: {
    marginBottom: 3,
    cursor: 'pointer',

    title: {
      icon: {
        fontSize: 11,
        width: 24,
        minWidth: 24,
        height: 24,
        lineHeight: '24px',
        textAlign: 'center',
        color: VARIABLE.color4D,
      },

      text: {
        main: {
          color: VARIABLE.color4D,
          fontSize: 15,
          lineHeight: '24px',

          ':hover': {
            color: VARIABLE.colorPink,
          }
        },

        sub: {
          fontSize: 12,
          lineHeight: '16px',
          color: VARIABLE.color97
        }
      }
    },
  },

  subItem: {
    paddingLeft: 24
  }
};
