/**
 * Created by Thành Võ on 13.03.2017
 */

import * as React from 'react';
import * as Radium from 'radium';

import { currenyFormat } from '../../../utils/currency';

import * as VARIABLE from '../../../styles/variable.style';

import STYLE from './ItemProduct.style';

/**  PROPS & STATE INTERFACE */
interface IItemProductProps extends React.Props<any> {
  data: any;
  type: string;
};

interface IItemProductState {
  hoverProduct: boolean;
};

@Radium
class ItemProduct extends React.Component<IItemProductProps, any> {
  static defaultProps: IItemProductProps;

  constructor(props) {
    super(props);

    this.state = {
      hoverProduct: false
    };
  }

  /**
   * Calculate percent sale value
   * @param newPrice is current price ( < old price)
   * @param oldPrice is last price (maybe not exist, assigned by user)
   *
   * Divide betwen newPrice and oldPrice to calculate percent sale value
   */
  calculateSale(newPrice, oldPrice) {
    let valueSale = Math.floor((oldPrice - newPrice) / oldPrice * 100);
    valueSale = valueSale <= 0 ? 0 : valueSale;
    return `-${valueSale}%`;
  }

  render() {
    const { data, type } = this.props;
    const { hoverProduct } = this.state;

    return (
      <div
        style={STYLE}
        onMouseEnter={() => this.setState({ hoverProduct: true })}
        onMouseLeave={() => this.setState({ hoverProduct: false })}>

        {/** 1. TOP CONTENT */}
        <div style={STYLE.top}>
          {/** 1.1. Icon Wish list */}
          {
            'full' === type &&
            <i
              className="lx lx-18-heart"
              style={[
                STYLE.top.icon,
                STYLE.top.favorite,
                (
                  window.innerWidth < VARIABLE.breakPoint1024 || true === hoverProduct
                ) && STYLE.top.icon.show,
              ]}></i>
          }

          {/** 1.2. Shopping cart */}
          {
            'full' === type &&
            <i
              className="lx lx-03-shopping-cart"
              style={[
                STYLE.top.icon,
                STYLE.top.shoppingCart,
                (
                  window.innerWidth < VARIABLE.breakPoint1024 || true === hoverProduct
                ) && STYLE.top.icon.show,
              ]}></i>
          }

          {/** 1.3. Sale with percent */}
          {
            'full' === type &&
            <div style={STYLE.top.sale}>
              {this.calculateSale(data.price, data.msrp_price)}
            </div>
          }

          {/** 1.4. Product image */}
          <img
            src={data.primary_picture_large_url}
            style={STYLE.top.image} />

          {/** 1.5. Logo category */}
          <i
            className="lx lx-01-logo"
            style={STYLE.top.logoCategory}></i>

          {/** 1.6. Line under logo category */}
          <div
            style={[
              STYLE.top.line,
              true === hoverProduct && STYLE.top.line.active
            ]}></div>
        </div>

        {/** 2. BOTTOM CONTENT */}
        <div style={STYLE.bottom}>
          {/** 2.1. overlay to click */}
          <div style={STYLE.bottom.overlay}></div>

          {/** 2.2. product title */}
          <div style={STYLE.bottom.name}>
            {data.name}
          </div>

          {/** 2.3. current price */}
          {
            'full' === type &&
            <div style={STYLE.bottom.price}>
              {currenyFormat(data.price)}
            </div>
          }

          {/** 2.4. old price */}
          {
            'full' === type &&
            <div style={STYLE.bottom.priceOld}>
              Giá trị thực: {currenyFormat(data.msrp_price)}
            </div>
          }
        </div>
      </div>
    );
  }
};

ItemProduct.defaultProps = {
  data: {},
  type: 'full'
};

export default ItemProduct;
