/**
 * Created by Thành Võ on 27.03.2017
 */

import * as VARIABLE from '../../../styles/variable.style';
import MEDIA_QUERIES from '../../../styles/mediaQueries.style';

export default {
  display: 'block',
  position: 'relative',
  marginBottom: '20px',
  padding: 5,

  top: {
    width: '100%',
    paddingTop: '67.77%',
    position: 'relative',
    borderBottom: `1px solid ${VARIABLE.colorE5}`,

    icon: {
      width: 32,
      height: 32,
      position: 'absolute',
      borderRadius: '50%',
      backgroundColor: VARIABLE.colorE5,
      lineHeight: '32px',
      textAlign: 'center',
      color: VARIABLE.color4D,
      fontSize: 15,
      left: '50%',
      zIndex: VARIABLE.zIndex3,
      transition: VARIABLE.transitionNormal,
      opacity: 0,
      top: 0,

      show: {
        opacity: 1,
        top: 20
      }
    },

    favorite: {
      marginLeft: -42
    },

    shoppingCart: {
      marginLeft: 10
    },

    sale: {
      width: 32,
      height: 32,
      lineHeight: '32px',
      color: VARIABLE.colorWhite,
      fontSize: '10px',
      textAlign: 'center',
      position: 'absolute',
      backgroundColor: VARIABLE.colorRed,
      borderRadius: '50%',
      zIndex: VARIABLE.zIndex3,
      fontFamily: VARIABLE.fontAvenirMedium,
      right: 4,
      top: '50%',
      marginTop: -36,

      [MEDIA_QUERIES.tablet1024]: {
        width: 36,
        height: 36,
        lineHeight: '36px',
        fontSize: 12,
      }
    },

    image: {
      maxWidth: '100%',
      maxHeight: '100%',
      position: 'absolute',
      top: '50%',
      left: '50%',
      transform: 'translate(-50%, -50%)',
      zIndex: VARIABLE.zIndex1,
    },

    line: {
      width: 0,
      height: '1px',
      background: VARIABLE.colorPink,
      position: 'absolute',
      left: '50%',
      transform: 'translate(-50%, 0)',
      bottom: -1,
      zIndex: VARIABLE.zIndex2,
      transition: VARIABLE.transitionNormal,
      opacity: 0,

      active: {
        width: '100%',
        opacity: 1,
      }
    },

    logoCategory: {
      position: 'absolute',
      width: 26,
      height: 26,
      fontSize: 26,
      color: VARIABLE.colorPink,
      left: '50%',
      marginLeft: -13,
      bottom: -13,
      zIndex: VARIABLE.zIndex9,
      background: VARIABLE.colorWhite,
      cursor: 'pointer',
      transform: 'scale(1)',
      transition: VARIABLE.transitionNormal,
      borderRadius: '50%',

      ':hover': {
        transform: 'scale(1.5)',
      }
    }
  },
  bottom: {
    paddingTop: 20,
    paddingBottom: 20,

    overlay: {
      width: '100%',
      height: '100%',
      backgroundColor: 'transparent',
      position: 'absolute',
      top: 0,
      left: 0,
      zIndex: VARIABLE.zIndex7,
      cursor: 'pointer',
    },

    name: {
      textTransform: 'uppercase',
      fontSize: 12,
      color: VARIABLE.color4D,
      lineHeight: '18px',
      height: 36,
      overflow: 'hidden',
      padding: '0 20px',
      width: '100%',
      textAlign: 'center',
      marginBottom: 5,

      [MEDIA_QUERIES.tablet1024]: {
        fontSize: 13,
        lineHeight: '20px',
        height: 40,
        marginBottom: 10,
      }
    },

    price: {
      fontSize: 16,
      lineHeight: '22px',
      color: VARIABLE.colorPink,
      fontFamily: VARIABLE.fontAvenirDemiBold,
      width: '100%',
      textAlign: 'center',

      [MEDIA_QUERIES.tablet1024]: {
        fontSize: 20,
        lineHeight: '28px',
        marginBottom: 5,
      }
    },

    priceOld: {
      color: VARIABLE.color97,
      fontSize: 11,
      lineHeight: '20px',
      textAlign: 'center',
      width: '100%',

      [MEDIA_QUERIES.tablet1024]: {
        fontSize: 12,
      }
    }
  },
};
