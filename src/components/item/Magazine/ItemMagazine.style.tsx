/**
 * Created by Thành Võ on 29.03.2017
 */

import * as VARIABLE from '../../../styles/variable.style';
import MEDIA_QUERIES from '../../../styles/mediaQueries.style';

export default {
  cursor: 'pointer',

  thumbnail: {
    width: 70,
    minWidth: 70,
    height: 100,
    backgroundPosition: 'center',
    backgroundSize: 'cover',
  },

  info: {
    flex: 5,
    background: VARIABLE.colorWhite03,
    paddingTop: 10,
    transition: VARIABLE.transitionBackground,

    ':hover': {
      background: VARIABLE.colorWhite
    },

    title: {
      fontFamily: VARIABLE.fontAvenirDemiBold,
      fontSize: 14,
      lineHeight: '20px',
      maxHeight: 60,
      overflow: 'hidden',
      padding: '0 20px 0 10px',
      textAlign: 'left',
      color: VARIABLE.color2E,
    },

    category: {
      fontFamily: VARIABLE.fontAvenirRegular,
      color: VARIABLE.color97,
      fontSize: 12,
      lineHeight: '20px',
      paddingLeft: 10,

      [MEDIA_QUERIES.tablet960]: {
        fontSize: 13,
        lineHeight: '20px'
      },
    },
  }
};
