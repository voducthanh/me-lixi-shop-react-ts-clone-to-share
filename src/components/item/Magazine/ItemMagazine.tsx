/**
 * Created by Thành Võ on 13.03.2017
 */

import * as React from 'react';
import * as Radium from 'radium';

import * as VARIABLE from '../../../styles/variable.style';
import * as LAYOUT from '../../../styles/layout.style';

import STYLE from './ItemMagazine.style';

/**  PROPS & STATE INTERFACE */
interface IItemMagazineProps extends React.Props<any> {
  data: any;
};

interface IItemMagazineState {
  hoverMagazine: boolean;
};

@Radium
class ItemMagazine extends React.Component<IItemMagazineProps, any> {
  constructor(props) {
    super(props);

    this.state = {
      hoverMagazine: false
    };
  }

  /** RENDER */
  render() {
    const { data } = this.props;

    return (
      <div
        style={[
          LAYOUT.flexContainer.justify,
          STYLE
        ]}
        onMouseEnter={() => this.setState({ hoverMagazine: true })}
        onMouseLeave={() => this.setState({ hoverMagazine: false })}>
        <div
          style={[
            { backgroundImage: `url('${data.thumbnail}')` },
            STYLE.thumbnail,
          ]}></div>
        <div style={STYLE.info}>
          <div style={STYLE.info.title}>
            {data.title}
          </div>
          <div style={STYLE.info.category}>
            {data.category}
          </div>
        </div>
      </div>
    );
  }
};

export default ItemMagazine;
