/**
 * Created by Thành Võ on 16.03.2017
 */

import * as VARIABLE from '../../../styles/variable.style';

const EXPERT_AVATAR = require('../../../assets/images/sample-data/background-expert-1.jpg');

export default {
  display: 'block',
  padding: '10px',
  cursor: 'pointer',

  avatar: {
    width: '75px',
    height: '75px',
    borderRadius: '50%',
    backgroundSize: 'cover',
    backgroundPosition: 'center',
    margin: '0 auto 15px',
    transition: VARIABLE.transitionNormal,

    hovered: {
      transform: 'scale(1.2)'
    }
  },

  name: {
    width: '100%',
    fontSize: '12px',
    lineHeight: '20px',
    fontFamily: VARIABLE.fontAvenirMedium,
    color: VARIABLE.color4D,
    textAlign: 'center',
    overflow: 'hidden',
    textOverflow: 'ellipsis',
    whiteSpace: 'nowrap',

    hovered: {
      color: VARIABLE.colorPink
    }
  }
};
