/**
 * Created by Thành Võ on 16.03.2017
 */

import * as React from 'react';
import * as Radium from 'radium';

const EXPERT_AVATAR = require('../../../assets/images/sample-data/background-expert-1.jpg');

import STYLE from './ItemExpert.style';

/**  PROPS & STATE INTERFACE */
interface IItemExpertProps extends React.Props<any> {
  expert: any;
};

interface IItemExpertState { };

@Radium
class ItemExpert extends React.Component<IItemExpertProps, any> {
  constructor(props) {
    super(props);

    this.state = {
      hoverExpert: false,
    };
  }

  render() {
    const { expert } = this.props;
    const { hoverExpert } = this.state;
    return (
      <div
        style={STYLE}
        onMouseEnter={() => this.setState({ hoverExpert: true })}
        onMouseLeave={() => this.setState({ hoverExpert: false })} >
        <div
          style={[
            STYLE.avatar,
            {
              backgroundImage: `url('${expert.avatar_medium_url}')`
            },
            hoverExpert && STYLE.avatar.hovered
          ]}></div>
        <div
          style={[
            STYLE.name,
            hoverExpert && STYLE.name.hovered
          ]}>
          {`${expert.first_name} ${expert.last_name}`}
        </div>
      </div >
    );
  }
};

export default ItemExpert;
