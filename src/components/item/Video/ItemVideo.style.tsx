import { Transform } from 'stream';
/**
 * Created by Thành Võ on 27.03.2017
 */

import * as VARIABLE from '../../../styles/variable.style';
import MEDIA_QUERIES from '../../../styles/mediaQueries.style';

export default {
  display: 'block',
  marginBottom: 10,
  cursor: 'pointer',
  position: 'ralative',

  thumbnail: {
    backgroundPosition: 'center',
    backgroundSize: 'cover',
    paddingTop: '60%',
    position: 'relative',
    marginBottom: 10,
    overflow: 'hidden',

    overlay: {
      position: 'absolute',
      width: '100%',
      height: '100%',
      top: 0,
      left: 0,
      backgroundPosition: 'center',
      backgroundSize: 'cover',
      opacity: 1,
      transition: VARIABLE.transitionNormal,
      transform: 'scale(1.15)',

      hovered: {
        transform: 'scale(1)',
      }
    },

    icon: {
      position: 'absolute',
      width: 40,
      height: 40,
      color: VARIABLE.colorWhite,
      fontSize: 40,
      top: '50%',
      left: '50%',
      transform: 'translate(-50%, -50%)',
      opacity: 0.69,
      transition: VARIABLE.transitionNormal,

      hovered: {
        opacity: 1,
        transform: 'translate(-50%, -50%) scale(2)'
      }
    }
  },

  title: {
    fontFamily: VARIABLE.fontAvenirMedium,
    fontSize: 13,
    lineHeight: '20px',
    height: 40,
    overflow: 'hidden',
    padding: '0 20px',
    textAlign: 'center',
    color: VARIABLE.color4D,
    marginBottom: 10,

    [MEDIA_QUERIES.tablet960]: {
      fontSize: 15,
      lineHeight: '22px',
      height: 44,
    },

    hovered: {
      color: VARIABLE.colorPink
    }
  },
};

