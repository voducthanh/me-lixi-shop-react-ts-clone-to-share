/**
 * Created by Thành Võ on 27.03.2017
 */

import * as React from 'react';
import * as Radium from 'radium';

import * as RADIUM from '../../../styles/variable.style';

import STYLE from './ItemVideo.style';

/**  PROPS & STATE INTERFACE */
interface IItemVideoProps extends React.Props<any> {
  video: any;
};

interface IItemVideoState {
  hoverVideo: boolean;
};

@Radium
class ItemVideo extends React.Component<IItemVideoProps, any> {
  constructor(props) {
    super(props);

    this.state = {
      hoverVideo: false
    };
  }

  render() {
    const { hoverVideo } = this.state;
    const { video } = this.props;

    return (
      <div
        style={STYLE}
        onMouseEnter={() => this.setState({ hoverVideo: true })}
        onMouseLeave={() => this.setState({ hoverVideo: false })}>

        {/** Thumbnail */}
        <div
          style={[
            { backgroundImage: `url('${video.snippet.thumbnails.medium.url}')` },
            STYLE.thumbnail,
          ]}>

          {/** Overlay */}
          <div
            style={[
              { backgroundImage: `url('${video.snippet.thumbnails.medium.url}')` },
              STYLE.thumbnail.overlay,
              hoverVideo && STYLE.thumbnail.overlay.hovered,
            ]}></div>

          {/** Icon video */}
          <i
            className="lx lx-19-play-video"
            style={[
              STYLE.thumbnail.icon,
              hoverVideo && STYLE.thumbnail.icon.hovered
            ]}></i>
        </div>

        {/** Title */}
        <div style={[STYLE.title, hoverVideo && STYLE.title.hovered]}>
          {video.snippet.title}
        </div>
      </div>
    );
  }
};

export default ItemVideo;
