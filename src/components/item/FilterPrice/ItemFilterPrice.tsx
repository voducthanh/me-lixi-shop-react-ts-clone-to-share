/**
 * Created by Thành Võ on 13.03.2017
 */

import * as React from 'react';
import * as Radium from 'radium';

import * as VARIABLE from '../../../styles/variable.style';
import * as LAYOUT from '../../../styles/layout.style';
import * as COMPONENT from '../../../styles/component.style';

import STYLE from './ItemFilterPrice.style';

/**  PROPS & STATE INTERFACE */
interface IItemFilterPriceProps extends React.Props<any> {
  closeFilterPrice: () => void;
};

interface IItemFilterPriceState { };

@Radium
class ItemFilterPrice extends React.Component<IItemFilterPriceProps, any> {
  constructor(props) {
    super(props);

    this.state = {
    };
  }

  closeFilterPrice() {
    this.props.closeFilterPrice();
  }

  checkShowClose() {
    return window.innerWidth < VARIABLE.breakPoint960;
  }

  render() {
    return (
      <div style={[COMPONENT.asideBlock, STYLE]}>

        {/* 1. Heading */}
        <div style={COMPONENT.asideBlock.heading}>
          <div style={COMPONENT.asideBlock.heading.text}>
            Giá (VNĐ)
          </div>
          {
            true === this.checkShowClose() &&
            <div
              key={`close-filter-brand`}
              style={COMPONENT.asideBlock.heading.close}
              className="lx lx-31-close"
              onClick={() => this.closeFilterPrice()}></div>
          }
        </div>

        {/* 2. Content */}
        <div
          style={[
            LAYOUT.flexContainer.wrap,
            COMPONENT.asideBlock.content,
            STYLE.priceRange,
          ]}>
          <div style={STYLE.rangeSlider}>
            <div style={STYLE.rangeSlider.rangeBar}></div>
            <div style={[STYLE.rangeSlider.button, STYLE.rangeSlider.left]}></div>
            <div style={[STYLE.rangeSlider.button, STYLE.rangeSlider.right]}></div>
          </div>
          <div style={STYLE.rangeValue}>
            <div style={[LAYOUT.flexContainer.justify, STYLE.rangeValue.minMax]}>
              <div style={[STYLE.priceRange.text, STYLE.rangeValue.minMax]}>100K</div>
              <div style={[STYLE.priceRange.text, STYLE.rangeValue.minMax]}>2,000K</div>
            </div>
            <div style={[LAYOUT.flexContainer.center]}>
              <div style={[STYLE.priceRange.text]}>500K</div>
              <div style={[STYLE.priceRange.text]}> -</div>
              <div style={[STYLE.priceRange.text]}>1,000K</div>
            </div>
          </div>
        </div>
      </div >
    );
  }
};

export default ItemFilterPrice;
