/**
 * Created by Thành Võ on 27.03.2017
 */

import * as VARIABLE from '../../../styles/variable.style';
import MEDIA_QUERIES from '../../../styles/mediaQueries.style';

export default {
  position: 'relative',

  priceRange: {
    padding: '20px 20px',

    text: {
      fontSize: 14,
      height: 20,
      color: VARIABLE.color4D,
    }
  },

  rangeSlider: {
    width: '100%',
    height: 26,
    position: 'relative',
    marginBottom: 10,

    rangeBar: {
      width: '100%',
      height: 12,
      backgroundColor: VARIABLE.colorE5,
      border: `1px solid ${VARIABLE.colorB0}`,
      borderRadius: 6,
      position: 'absolute',
      left: 0,
      top: 7,
    },

    button: {
      width: 26,
      height: 26,
      borderRadius: '50%',
      cursor: 'pointer',
      backgroundColor: VARIABLE.colorFA,
      border: `1px solid ${VARIABLE.colorB0}`,
      top: 0,
      position: 'absolute',
    },

    left: {
      left: 20
    },

    right: {
      right: 20
    },
  },

  rangeValue: {
    width: '100%',

    minMax: {
      width: '100%',
      marginBottom: 10,
    },
  }
};
