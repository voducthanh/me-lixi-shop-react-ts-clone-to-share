/**
 * Created by Thành Võ on 17.03.2017
 */

import * as React from 'react';
import * as Radium from 'radium';

import * as LAYOUT from '../../../styles/layout.style';
import * as VARIABLE from '../../../styles/variable.style';

import STYLE from './ItemFeedback.style';

/**  PROPS & STATE INTERFACE */
interface IItemFeedbackProps extends React.Props<any> {
  data: any;
};

interface IItemFeedbackState {
  hoverFeedback: boolean;
};

@Radium
class ItemFeedback extends React.Component<IItemFeedbackProps, any> {
  constructor(props) {
    super(props);

    this.state = {
      hoverFeedback: false,
    };
  }

  /** RENDER */
  render() {
    const { hoverFeedback } = this.state;
    const { data } = this.props;

    return (
      <div
        style={STYLE}
        onTouchStart={() => this.setState({ hoverFeedback: true })}
        onTouchEnd={() => this.setState({ hoverFeedback: false })}
        onMouseEnter={() => this.setState({ hoverFeedback: true })}
        onMouseLeave={() => this.setState({ hoverFeedback: false })}>
        <div
          style={[
            { backgroundImage: `url('${data.image}')` },
            STYLE.thumbnail,
          ]}>
          <div
            style={[
              STYLE.info,
              hoverFeedback && STYLE.info.hover
            ]}>
            <div
              style={[
                LAYOUT.flexContainer.left,
                STYLE.info.user
              ]}>
              <div
                style={[
                  { backgroundImage: `url('${data.avatar}')` },
                  STYLE.info.user.avatar,
                ]}></div>
              <div style={STYLE.info.user.name}>
                {data.username}
              </div>
            </div>
            <div
              style={[
                STYLE.info.content,
                hoverFeedback && STYLE.info.content.show
              ]}>
              {data.title}
            </div>
          </div>
        </div>
      </div>
    );
  }
};

export default ItemFeedback;
