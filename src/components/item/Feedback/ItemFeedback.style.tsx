/**
 * Created by Thành Võ on 29.03.2017
 */

import * as VARIABLE from '../../../styles/variable.style';
import MEDIA_QUERIES from '../../../styles/mediaQueries.style';

export default {
  display: 'block',
  position: 'relative',

  thumbnail: {
    backgroundPosition: 'center',
    backgroundSize: 'cover',
    width: '100%',
    paddingTop: '100%'
  },

  info: {
    padding: 10,
    position: 'absolute',
    bottom: 0,
    left: 0,
    width: '100%',
    maxWidth: '100%',
    backgroundColor: VARIABLE.colorBlack02,
    transition: VARIABLE.transitionBackground,

    hover: {
      backgroundColor: VARIABLE.colorBlack,
    },

    user: {
      width: '100%',
      maxWidth: '100%',

      avatar: {
        width: 26,
        minWidth: 26,
        height: 26,
        borderRadius: '50%',
        backgroundSize: 'cover',
        backgroundPosition: 'center',
        marginRight: 10,
      },

      name: {
        lineHeight: '26px',
        height: 26,
        fontSize: 16,
        fontFamily: VARIABLE.fontAvenirDemiBold,
        color: VARIABLE.colorE5,
        whiteSpace: 'nowrap',
        overflow: 'hidden',
        textOverflow: 'ellipsis'
      },
    },

    content: {
      fontSize: 14,
      lineHeight: '20px',
      overflow: 'hidden',
      color: VARIABLE.colorWhite07,
      textAlign: 'justify',
      display: 'none',
      transition: VARIABLE.transitionNormal,
      height: 0,
      opacity: 0,
      marginTop: 0,

      show: {
        height: 60,
        opacity: 1,
        marginTop: 10,
      },

      [MEDIA_QUERIES.mobile610]: {
        display: 'block'
      }
    },
  }
};
