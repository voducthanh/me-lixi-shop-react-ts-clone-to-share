/**
 * Created by Thành Võ on 23.03.2017
 */

import * as VARIABLE from '../../../styles/variable.style';

export default {
  display: 'block',
  width: '100%',
  paddingTop: '5.263%',
  marginBottom: 10,
  position: 'relative',

  item: {
    backgroundPosition: 'center',
    backgroundSize: 'cover',
    width: '100%',
    height: '100%',
    position: 'absolute',
    top: 0,
    left: 0,
    transition: VARIABLE.transitionNormal,
  },

  itemAnimate: {
    opacity: .5,
    transform: 'skewX(-10deg) rotateX(90deg)'
  }
};
