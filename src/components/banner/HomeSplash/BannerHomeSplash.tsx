/**
 * Created by Thành Võ on 13.03.2017
 */

import * as React from 'react';
import * as Radium from 'radium';

const SPLASH_BACKGROUND_1 = require('../../../assets/images/sample-data/banner-home-splash-1.jpg');
const SPLASH_BACKGROUND_2 = require('../../../assets/images/sample-data/banner-home-splash-2.jpg');

import STYLE from './BannerHomeSplash.style';

@Radium
class BannerHomeSplash extends React.Component<any, any> {
  constructor(props: any) {
    super(props);

    this.state = {
      bannerSelected: {
        id: 1,
        image: SPLASH_BACKGROUND_1,
        href: '#',
        title: 'Banner 1'
      },
      bannerList: [
        {
          id: 1,
          image: SPLASH_BACKGROUND_1,
          href: '#',
          title: 'Banner 1'
        },
        {
          id: 2,
          image: SPLASH_BACKGROUND_2,
          href: '#',
          title: 'Banner 2'
        }
      ],
      countChangeSlide: 0,
      timerChangeSlide: null,
      animating: false
    };
  }

  componentDidMount() {
    this.state.timerChangeSlide = setInterval(() => {
      this.setState((prevState: any, props: any) => ({
        animating: true
      }));

      setTimeout(() => {
        this.setState((prevState, props) => ({
          countChangeSlide: prevState.countChangeSlide === prevState.bannerList.length - 1
            ? 0
            : prevState.countChangeSlide + 1,
          bannerSelected: prevState.bannerList[
          prevState.countChangeSlide === prevState.bannerList.length - 1
            ? 0
            : prevState.countChangeSlide + 1
          ],
          animating: false
        }));
      }, 300);
    }, 3000);
  }

  componentWillUnmount() {
    clearInterval(this.state.timerChangeSlide);
  }

  /** RENDER */
  render() {
    return (
      <div style={STYLE}>
        <div
          style={[
            STYLE.item,
            {
              backgroundImage: `url('${this.state.bannerSelected.image}')`
            },
            this.state.animating === true && STYLE.itemAnimate
          ]}></div>
      </div>
    );
  }
};

export default BannerHomeSplash;
