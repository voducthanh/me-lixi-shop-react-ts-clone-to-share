/**
 * Created by Thành Võ on 24.03.2017
 */

import * as VARIABLE from '../../../styles/variable.style';

export default {
  display: 'block',
  width: '100%',
  paddingTop: '30.717%',
  marginBottom: '10px',
  position: 'relative',
  overflow: 'hidden',

  mobileRatio: {
    paddingTop: '50%',
  },

  item: {
    backgroundPosition: 'center',
    backgroundSize: 'cover',
    width: '100%',
    height: '100%',
    position: 'absolute',
    top: '0',
    left: '0',
    transition: VARIABLE.transitionOpacity,
    cursor: 'pointer',
  },

  itemAnimate: {
    opacity: 0.5,
  },
};
