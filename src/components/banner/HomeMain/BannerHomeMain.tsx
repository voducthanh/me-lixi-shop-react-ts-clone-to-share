/**
 * Created by Thành Võ on 13.03.2017
 */

import * as React from 'react';
import * as Radium from 'radium';

import * as LAYOUT from '../../../styles/layout.style';
import * as COMPONENT from '../../../styles/component.style';
import * as VARIABLE from '../../../styles/variable.style';

const BANNER_BACKGROUND_1 = require('../../../assets/images/sample-data/banner-home-main-1.jpg');
const BANNER_BACKGROUND_2 = require('../../../assets/images/sample-data/banner-home-main-2.png');
const BANNER_BACKGROUND_3 = require('../../../assets/images/sample-data/banner-home-main-3.png');
const BANNER_BACKGROUND_4 = require('../../../assets/images/sample-data/banner-home-main-4.jpg');
const BANNER_BACKGROUND_5 = require('../../../assets/images/sample-data/banner-home-main-5.jpg');

import STYLE from './BannerHomeMain.style';

@Radium
class BannerHomeMain extends React.Component<any, any> {
  constructor(props: any) {
    super(props);

    this.state = {
      mouseHover: false,
      bannerSelected: {
        id: 1,
        image: BANNER_BACKGROUND_1,
        href: '#',
        title: 'Banner 1'
      },
      bannerList: [
        {
          id: 1,
          image: BANNER_BACKGROUND_1,
          href: '#',
          title: 'Banner 1'
        },
        {
          id: 2,
          image: BANNER_BACKGROUND_2,
          href: '#',
          title: 'Banner 2'
        },
        {
          id: 3,
          image: BANNER_BACKGROUND_3,
          href: '#',
          title: 'Banner 3'
        },
        {
          id: 4,
          image: BANNER_BACKGROUND_4,
          href: '#',
          title: 'Banner 4'
        },
        {
          id: 5,
          image: BANNER_BACKGROUND_5,
          href: '#',
          title: 'Banner 5'
        }
      ],
      countChangeSlide: 0,
      timerChangeSlide: null,
      animating: false
    };
  }

  componentDidMount() {
    this.state.timerChangeSlide = setInterval(() => {
      this.setState((prevState, props) => ({
        animating: true
      }));

      setTimeout(() => {
        this.navSlide('right', true);
      }, 300);
    }, 5000);
  }

  componentWillUnmount() {
    clearInterval(this.state.timerChangeSlide);
  }

  navSlide(_direction, _keepAutoSlide = false) {
    let newIndexValue = 'left' === _direction ? -1 : 1;
    newIndexValue += this.state.countChangeSlide;

    newIndexValue = newIndexValue === this.state.bannerList.length
      ? 0
      : (
        newIndexValue === -1
          ? this.state.bannerList.length - 1
          : newIndexValue
      );

    this.selectSlide(newIndexValue, _keepAutoSlide);
  }

  selectSlide(_index, _keepAutoSlide = false) {
    if (false === _keepAutoSlide) {
      clearInterval(this.state.timerChangeSlide);
    }

    this.setState((prevState, props) => ({
      animating: true
    }));
    setTimeout(() => {
      this.setState((prevState, props) => ({
        countChangeSlide: _index,
        bannerSelected: prevState.bannerList[_index],
        animating: false
      }));
    }, 300);
  }

  /** RENDER */
  public render() {
    return (
      <div
        style={[
          STYLE,
          window.innerWidth < VARIABLE.breakPoint960 && STYLE.mobileRatio
        ]}
        onMouseEnter={() => this.setState({ mouseHover: true })
        }
        onMouseLeave={() => this.setState({ mouseHover: false })}>

        {/** Banner image */}
        < div
          style={[
            STYLE.item,
            {
              backgroundImage: `url('${this.state.bannerSelected.image}')`
            },
            this.state.animating === true && STYLE.itemAnimate
          ]}> </div>

        {/** Banner Pagination */}
        < div style={[LAYOUT.flexContainer.center, COMPONENT.slidePagination]}>
          {
            this.state.bannerList.map((banner, $index) =>
              <div
                key={`banner-main-home-${banner.id}`}
                onClick={() => this.selectSlide($index)}
                style={
                  [
                    COMPONENT.slidePagination.item,
                    $index === this.state.countChangeSlide && COMPONENT.slidePagination.itemActive
                  ]}> </div>
            )
          }
        </div >

        {/** Button Navigation */}
        < div >
          <div
            onClick={() => this.navSlide('left')}
            style={
              [
                LAYOUT.flexContainer.center,
                LAYOUT.flexContainer.verticalCenter,
                COMPONENT.slideNavigation,
                COMPONENT.slideNavigation.black,
                COMPONENT.slideNavigation.left,
                this.state.mouseHover && COMPONENT.slideNavigation.left.active
              ]}>
            <div className="lx lx-15-angle-left" style={COMPONENT.slideNavigation.icon} > </div>
          </div>
          < div
            onClick={() => this.navSlide('right')}
            style={[
              LAYOUT.flexContainer.center,
              LAYOUT.flexContainer.verticalCenter,
              COMPONENT.slideNavigation,
              COMPONENT.slideNavigation.black,
              COMPONENT.slideNavigation.right,
              this.state.mouseHover && COMPONENT.slideNavigation.right.active
            ]}>
            <div className="lx lx-14-angle-right" style={COMPONENT.slideNavigation.icon} > </div>
          </div>
        </div>
      </div>
    );
  }
};

export default BannerHomeMain;
