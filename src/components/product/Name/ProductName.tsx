/**
 * Created by Thành Võ on 03.04.2017
 */

import * as React from 'react';
import * as Radium from 'radium';

import STYLE from './ProductName.style';

/**  PROPS & STATE INTERFACE */
interface IProductNameProps extends React.Props<any> {
  name: string
};

ProductName.defaultProps = {
  name: 'Tên Sản phẩm',
};

interface IProductNameState {
  showNaviTop: boolean
};

@Radium
class ProductName extends React.Component<IProductNameProps, any> {
  static defaultProps: IProductNameProps;

  constructor(props: IProductNameProps) {
    super(props);

    this.state = {}
  }

  render() {
    const { name } = this.props;

    return (
      <div style={STYLE}>
        {name}
      </div>
    )
  }
};

export default ProductName;
