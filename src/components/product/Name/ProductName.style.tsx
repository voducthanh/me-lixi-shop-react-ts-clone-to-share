/**
 * Created by Thành Võ on 03.04.2017
 */

import * as VARIABLE from '../../../styles/variable.style';
import MEDIA_QUERIES from '../../../styles/mediaQueries.style';

export default {
  display: 'block',
  width: '100%',
  fontFamily: VARIABLE.fontAvenirDemiBold,
  padding: '30px 15px 15px',
  textTransform: 'uppercase',
  color: VARIABLE.color4D,
  fontSize: 15,
  lineHeight: '22px',
  textAlign: 'center',

  [MEDIA_QUERIES.tablet960]: {
    fontSize: 18,
    lineHeight: '24px',
    padding: '15px 0',
    textAlign: 'left',
  }
};
