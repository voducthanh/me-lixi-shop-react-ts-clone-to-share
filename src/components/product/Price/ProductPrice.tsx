/**
 * Created by Thành Võ on 03.04.2017
 */

import * as  React from 'react';
import * as Radium from 'radium';

import STYLE from './ProductPrice.style';

/**  PROPS & STATE INTERFACE */
interface IProductPriceProps extends React.Props<any> {
  price: any
};

interface IProductPriceState { };

@Radium
class ProductPrice extends React.Component<IProductPriceProps, any> {
  constructor(props) {
    super(props);

    this.state = {}
  }

  render() {
    const { price } = this.props;

    return (
      <div style={STYLE}>
        <div style={STYLE.current}>
          {price.current.display}
        </div>
        <div style={STYLE.old}>
          {price.old.display}
        </div>
      </div>
    )
  }
};

export default ProductPrice;
