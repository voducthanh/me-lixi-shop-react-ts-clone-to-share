/**
 * Created by Thành Võ on 03.04.2017
 */

import * as VARIABLE from '../../../styles/variable.style';
import MEDIA_QUERIES from '../../../styles/mediaQueries.style';

export default {
  display: 'block',
  width: '100%',

  current: {
    fontSize: 22,
    lineHeight: '26px',
    color: VARIABLE.color4D,
    fontFamily: VARIABLE.fontAvenirDemiBold,
  },

  old: {
    fontSize: 13,
    color: VARIABLE.color97,
    textDecoration: 'line-through'
  },
};
