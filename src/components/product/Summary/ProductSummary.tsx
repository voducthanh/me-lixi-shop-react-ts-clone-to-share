/**
 * Created by Thành Võ on 03.04.2017
 */

import * as React from 'react';
import * as Radium from 'radium';

import RatingStar from '../../general/UI/RatingStar';

import * as LAYOUT from '../../../styles/layout.style';

import STYLE from './ProductSummary.style';

/**  PROPS & STATE INTERFACE */
interface IProductSummaryProps extends React.Props<any> {
  summary: any
};

interface IProductSummaryState { };

@Radium
class ProductSummary extends React.Component<IProductSummaryProps, any> {
  constructor(props: IProductSummaryProps) {
    super(props);

    this.state = {}
  }

  render() {
    const { summary } = this.props;

    return (
      <div style={[STYLE]}>
        {/** 1. Rating & Love */}
        <div
          style={[
            LAYOUT.flexContainer.left,
            LAYOUT.flexContainer.verticalCenter,
            STYLE.ratingLove
          ]}>
          {/** 1.1. Rating */}
          <RatingStar value={summary.rating.value} />
          {
            0 === summary.rating.value
              ? <div style={STYLE.ratingLove.text}>Đánh giá ngay</div>
              : <div style={STYLE.ratingLove.text}>{summary.rating.count} đánh giá</div>

          }

          {/** 1.2. Spacer */}
          <div style={STYLE.ratingLove.text}>/</div>

          {/** 1.3. Love */}
          <div
            style={[
              LAYOUT.flexContainer.left,
              LAYOUT.flexContainer.verticalCenter,
              STYLE.ratingLove.text
            ]}>
            <i
              className="lx lx-18-heart"
              style={STYLE.ratingLove.iconHeart}></i>
            {summary.love} loves
          </div>
        </div>

        {/** 2. promotion text*/}
        {/** 2.1. lixicoint */}
        <div
          style={[
            LAYOUT.flexContainer.left,
            LAYOUT.flexContainer.verticalCenter,
            STYLE.promotionText,
            STYLE.promotionText.pink
          ]}>
          <i
            className="lx lx-06-dollar"
            style={STYLE.promotionText.icon}></i>
          <div style={STYLE.promotionText.text}>
            Nhận ngay
            <span
              style={STYLE.promotionText.bold}>
              310 Lixicoin
            </span>
            khi mua box này
          </div>
        </div>

        {/** 2.2. free delivery */}
        <div
          style={[
            LAYOUT.flexContainer.left,
            LAYOUT.flexContainer.verticalCenter,
            STYLE.promotionText,
            STYLE.promotionText.gray
          ]}>
          <i
            className="lx lx-40-delivery"
            style={STYLE.promotionText.icon}></i>
          <div style={STYLE.promotionText.text}>
            <span style={STYLE.promotionText.bold}>
              Miễn phí vận chuyển
            </span>
            đơn hàng từ 800,000 đ
          </div>
        </div>
      </div>
    )
  }
};

export default ProductSummary;
