/**
 * Created by Thành Võ on 03.04.2017
 */

import * as VARIABLE from '../../../styles/variable.style';
import MEDIA_QUERIES from '../../../styles/mediaQueries.style';

export default {
  display: 'block',
  width: '100%',
  padding: '0 15px',
  marginBottom: 15,

  ratingLove: {
    marginBottom: 5,

    text: {
      fontSize: 14,
      color: VARIABLE.color4D,
      lineHeight: '34px',
      height: 30,
      padding: '0 6px',
      whiteSpace: 'nowrap',
    },

    iconHeart: {
      fontSize: 16,
      width: 16,
      height: 16,
      lineHeight: '16px',
      textAlign: 'center',
      color: VARIABLE.colorPink,
      marginRight: 10
    }
  },

  promotionText: {
    padding: '8px 10px 8px 0',
    borderRadius: 2,
    marginBottom: 10,

    text: {
      color: VARIABLE.color75,
      fontSize: 14,
      lineHeight: '18px',
    },

    icon: {
      width: 45,
      minWidth: 45,
      height: 18,
      lineHeight: '18px',
      fontSize: 18,
      color: VARIABLE.color4D
    },

    bold: {
      fontFamily: VARIABLE.fontAvenirDemiBold,
      whiteSpace: 'nowrap',
      color: VARIABLE.color4D,
    },

    pink: {
      backgroundColor: VARIABLE.colorPink01,
    },

    gray: {
      backgroundColor: VARIABLE.colorBlack005,
    }
  }
};
