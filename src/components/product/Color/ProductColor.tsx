/**
 * Created by Thành Võ on 03.04.2017
 */

import * as React from 'react';
import * as Radium from 'radium';

import * as LAYOUT from '../../../styles/layout.style';

import STYLE from './ProductColor.style';

/**  PROPS & STATE INTERFACE */
interface IProductColorProps extends React.Props<any> {
  colorList: Array<any>,
  changeColor: any
};

interface IProductColorState {
  showNaviTop: boolean
};

@Radium
class ProductColor extends React.Component<IProductColorProps, any> {
  constructor(props) {
    super(props);

    this.state = {}
  }

  render() {
    const { colorList, changeColor } = this.props;

    return (
      <div style={STYLE}>
        {/** 1. Text color selected */}
        <div style={STYLE.name}>
          Color:
          <span style={STYLE.name.value}>
            {
              colorList.filter(color => color.selected)[0].name
            }
          </span>
        </div>

        {/** 2. Color List */}
        <div
          style={[
            LAYOUT.flexContainer.center,
            LAYOUT.flexContainer.wrap,
            STYLE.list
          ]}>
          {
            colorList.map(color =>
              <div
                key={`product-color-${color.id}`}
                onClick={() => changeColor(color)}
                style={[
                  { backgroundColor: `#${color.color}` },
                  STYLE.list.item,
                  color.light && STYLE.list.item.light
                ]}>

                {/** 2.1. icon check: first */}
                <div
                  style={[
                    STYLE.list.item.icon,
                    color.light && STYLE.list.item.icon.light,
                    STYLE.list.item.first,
                    color.selected && STYLE.list.item.selected,
                  ]}></div>

                {/** 2.2. icon check: last */}
                <div
                  style={[
                    STYLE.list.item.icon,
                    color.light && STYLE.list.item.icon.light,
                    STYLE.list.item.last,
                    color.selected && STYLE.list.item.selected
                  ]}></div>
              </div>
            )
          }
        </div>
      </div>
    )
  }
};

export default ProductColor;
