/**
 * Created by Thành Võ on 03.04.2017
 */

import * as VARIABLE from '../../../styles/variable.style';

export default {
  display: 'block',
  width: '100%',
  padding: '0 15px',
  marginBottom: 15,

  name: {
    color: VARIABLE.color75,
    fontSize: 14,
    lineHeight: '20px',
    marginBottom: 10,
    textAlign: 'center',

    value: {
      color: VARIABLE.color2E,
      marginLeft: 3,
      fontFamily: VARIABLE.fontAvenirDemiBold,
    },
  },

  list: {
    item: {
      width: 35,
      minWidth: 35,
      height: 35,
      borderRadius: '50%',
      marginRight: 8,
      marginLeft: 8,
      marginBottom: 16,
      cursor: 'pointer',
      transition: VARIABLE.transitionOpacity,
      position: 'relative',
      opacity: 0.95,

      light: {
        boxShadow: `0 0 0 3px ${VARIABLE.color97} inset`
      },

      icon: {
        backgroundColor: VARIABLE.colorWhite,
        position: 'absolute',
        borderRadius: 2,
        opacity: 0,
        transition: VARIABLE.transitionOpacity,

        light: {
          backgroundColor: VARIABLE.color97,
        }
      },

      first: {
        width: 8,
        height: 3,
        transform: 'rotate(45deg)',
        top: 18,
        left: 9,
      },

      last: {
        width: 15,
        height: 3,
        transform: 'rotate(-45deg)',
        top: 16,
        left: 12,
      },

      selected: {
        opacity: 1
      }
    },
  }
};
