/**
 * Created by Thành Võ on 03.04.2017
 */

import * as VARIABLE from '../../../styles/variable.style';
import MEDIA_QUERIES from '../../../styles/mediaQueries.style';

export default {
  display: 'block',
  width: '100%',
  position: 'relative',
  zIndex: VARIABLE.zIndex5,

  heading: {
    height: 70,
    position: 'relative',

    tab: {
      flex: 1,
      height: 60,
      borderTop: `1px solid ${VARIABLE.colorE5}`,
      borderBottom: `1px solid ${VARIABLE.colorE5}`,
      position: 'relative',
      cursor: 'pointer',
      backgroundColor: VARIABLE.colorF7,
      zIndex: VARIABLE.zIndex5,

      icon: {
        fontSize: 20,
        width: 20,
        height: 20,
        color: VARIABLE.color75,

        selected: {
          color: VARIABLE.colorWhite
        }
      },
    },

    tabSelected: {
      height: 60,
      position: 'absolute',
      top: 0,
      backgroundColor: VARIABLE.colorPink,
      zIndex: VARIABLE.zIndex9,
      transition: VARIABLE.transitionLeft,
      boxShadow: VARIABLE.shadow1,

      arrow: {
        position: 'absolute',
        width: 24,
        height: 10,
        left: '50%',
        bottom: -10,
        marginLeft: -12,
        borderTop: `10px solid ${VARIABLE.colorPink}`,
        borderLeft: `12px solid transparent`,
        borderRight: `12px solid transparent`,
        boxSizing: 'border-box',
      }
    }
  },

  content: {
    tab: {}
  }
};
