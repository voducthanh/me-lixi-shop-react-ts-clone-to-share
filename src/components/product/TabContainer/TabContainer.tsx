/**
 * Created by Thành Võ on 03.04.2017
 */

import * as  React from 'react';
import * as Radium from 'radium';

import TabInfo from './general/TabInfo'
import TabProduct from './general/TabProduct'
import TabRating from './general/TabRating'
import TabComment from './general/TabComment'
import TabDelivery from './general/TabDelivery'

import * as LAYOUT from '../../../styles/layout.style';

import STYLE from './TabContainer.style';

/**  PROPS & STATE INTERFACE */
interface ITabContainerProps extends React.Props<any> {
  product: any
};

interface ITabContainerState {
  tabList: Array<any>,
  styleTabSelected: any
};

@Radium
class TabContainer extends React.Component<ITabContainerProps, any> {
  constructor(props: ITabContainerProps) {
    super(props);

    this.state = {
      tabList: [
        {
          id: 0,
          nameCode: 'info',
          class: 'lx lx-43-info',
          title: '',
          selected: false
        },
        {
          id: 1,
          nameCode: 'product',
          class: 'lx lx-44-tags',
          title: '',
          selected: false
        },
        {
          id: 2,
          nameCode: 'rating',
          class: 'lx lx-38-start-line',
          title: '',
          selected: false
        },
        {
          id: 3,
          nameCode: 'comment',
          class: 'lx lx-45-comments',
          title: '',
          selected: false
        },
        {
          id: 4,
          nameCode: 'delivery',
          class: 'lx lx-40-delivery',
          title: '',
          selected: true
        },
      ],
      styleTabSelected: {
        left: 0,
        width: 0,
      }
    }
  }

  componentDidMount() {
    this.styleForTabSelected();
  }

  /** Set style for tab selected background */
  styleForTabSelected() {
    this.setState((prevState, props) => ({
      styleTabSelected: {
        width: (100 / prevState.tabList.length) + '%',
        left: (prevState.tabList.filter(tab => tab.selected)[0].id * (100 / prevState.tabList.length)) + '%'
      }
    }));
  }

  /** handle when onClick change Tab */
  changeTab(_tab) {
    this.setState((prevState, props) => ({
      tabList: prevState.tabList.map(tab => {
        tab.selected = tab.id === _tab.id;
        return tab;
      })
    }));

    this.styleForTabSelected();
  }

  /** Render tab content */
  renderTabContent(_tab) {
    const { product } = this.props;

    switch (_tab.nameCode) {
      case 'info': return <TabInfo />;
      case 'product': return <TabProduct product={product.productDetail} />;
      case 'rating': return <TabRating rating={product.summary.rating} />;
      case 'comment': return <TabComment comment={product.comment} />;
      case 'delivery': return <TabDelivery />;
    }
  }

  /** RENDER */
  render() {
    const { tabList, styleTabSelected } = this.state;

    return (
      <div style={[STYLE]}>

        {/** 1. HEADING */}
        <div
          style={[
            LAYOUT.flexContainer.justify,
            STYLE.heading
          ]}>

          {/** 1.1. Tab item */}
          {
            tabList.map(tab =>
              <div
                key={`product-tab-heading-${tab.id}`}
                onClick={() => this.changeTab(tab)}
                style={[
                  LAYOUT.flexContainer.center,
                  LAYOUT.flexContainer.verticalCenter,
                  STYLE.heading.tab,
                ]}>
                <i
                  style={[
                    STYLE.heading.tab.icon,
                  ]} className={tab.class}></i>
              </div>
            )
          }

          {/** 1.2. Tab selected background : for animation */}
          <div
            style={[
              {
                left: styleTabSelected.left,
                width: styleTabSelected.width,
              },
              LAYOUT.flexContainer.center,
              LAYOUT.flexContainer.verticalCenter,
              STYLE.heading.tabSelected
            ]}>
            <i
              style={[
                STYLE.heading.tab.icon,
                STYLE.heading.tab.icon.selected,
              ]}
              className={
                tabList.filter(tab => tab.selected)[0].class
              }></i>
            <div style={STYLE.heading.tabSelected.arrow}>
            </div>
          </div>
        </div>

        {/** 2. CONTENT */}
        <div>
          {
            tabList.map(tab =>
              tab.selected &&
              <div key={`product-tab-content-${tab.id}`}>
                {this.renderTabContent(tab)}
              </div>
            )
          }
        </div>
      </div >
    )
  }
};

export default TabContainer;
