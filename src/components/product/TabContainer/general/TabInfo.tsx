/**
 * Created by Thành Võ on 03.04.2017
 */

import * as React from 'react';
import * as Radium from 'radium';

import * as COMPONENT from '../../../../styles/component.style';
import * as TYPOGRAPHY from '../../../../styles/typography.style';

import STYLE from './TabInfo.style';

/**  PROPS & STATE INTERFACE */
interface ITabInfoProps extends React.Props<any> { };

interface ITabInfoState { };

@Radium
class TabInfo extends React.Component<ITabInfoProps, any> {
  constructor(props) {
    super(props);

    this.state = {}
  }

  render() {
    return (
      <div style={STYLE}>
        <div
          style={[
            TYPOGRAPHY.title.normal,
            STYLE.heading
          ]}>
          Giới thiệu Box
          </div>
        <div>
          <p style={[TYPOGRAPHY.bodyText.normal, STYLE.content]}>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Under sit amet, consectetur adipisicing elit. Under sit amet, consectetur adipisicing elit. Under sit amet, consectetur adipisicing elit. Unde, reiciendis.</p>
          <p style={[TYPOGRAPHY.bodyText.normal, STYLE.content]}>Lorem ipsum dolor sit amet.Lorem ipsum dolor sit amet.Lorem ipsum dolor dolor sit amet.Lorem ipsum dolor sit amet.Lorem ipsum dolor dolor sit amet.Lorem ipsum dolor sit amet.Lorem ipsum dolor dolor sit amet.Lorem ipsum dolor sit amet.Lorem ipsum dolor dolor sit amet.Lorem ipsum dolor sit amet.Lorem ipsum dolor sit amet.Lorem ipsum dolor sit amet.Lorem ipsum dolor sit amet.</p>
          <p style={[TYPOGRAPHY.bodyText.normal, STYLE.content]}>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Possimus alias unde deleniti dolor sit amet, consectetur adipisicing elit. Possimus alias unde deleniti dolor sit amet, consectetur adipisicing elit. Possimus alias unde deleniti dolor sit amet, consectetur adipisicing elit. Possimus alias unde deleniti dolor sit amet, consectetur adipisicing elit. Possimus alias unde deleniti dolor sit amet, consectetur adipisicing elit. Possimus alias unde deleniti dolor sit amet, consectetur adipisicing elit. Possimus alias unde deleniti dolor sit amet, consectetur adipisicing elit. Possimus alias unde deleniti dolor sit amet, consectetur adipisicing elit. Possimus alias unde deleniti aperiam, velit minus maiores odio fuga ab assumenda.</p>
          <p style={[TYPOGRAPHY.bodyText.normal, STYLE.content]}>Lorem ipsum dolor sit ametorem ipsum dolor sit ametorem ipsum dolor sit ametorem ipsum dolor sit ametorem ipsum dolor sit amet.</p>
        </div>
      </div>
    )
  }
};

export default TabInfo;
