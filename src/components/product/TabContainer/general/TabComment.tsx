/**
 * Created by Thành Võ on 03.04.2017
 */

import * as React from 'react';
import * as Radium from 'radium';

import * as LAYOUT from '../../../../styles/layout.style';
import * as TYPOGRAPHY from '../../../../styles/typography.style';

import STYLE from './TabComment.style';

/**  PROPS & STATE INTERFACE */
interface ITabCommentProps extends React.Props<any> {
  comment: Array<any>
};

interface ITabCommentState {
  comment: Array<any>
};

@Radium
class TabComment extends React.Component<ITabCommentProps, any> {
  constructor(props: ITabCommentProps) {
    super(props);

    this.state = {
      comment: this.props.comment
    }
  }

  toggleReply(_comment) {
    this.setState((prevState, props) => ({
      comment: prevState.comment.map(item => {
        item.replyShow = item.id === _comment.id
          ? !item.replyShow
          : item.replyShow;
        return item;
      })
    }));
  }

  render() {
    const { comment } = this.state;

    return (
      <div style={STYLE}>
        {/** 1. Form Send comment */}
        <div
          style={[
            LAYOUT.flexContainer.left,
            STYLE.formReply
          ]}>

          {/** 1.1. Avatar */}
          <div
            style={[
              { backgroundImage: `url('${require('../../../../assets/images/sample-data/avatar.jpg')}')` },
              STYLE.formReply.avatar
            ]}></div>

          {/** 1.2. Content */}
          <div style={STYLE.formReply.content}>
            {/*<textarea style={STYLE.formReply.content.input}></textarea>*/}
          </div>
        </div>

        {/** 2. List comment */}
        <div>
          {
            comment.map(comment =>
              <div
                style={[LAYOUT.flexContainer.left, STYLE.list.item]}
                key={`comment-${comment.id}`}>

                {/** 2.1. Avatar  user */}
                <div
                  style={[
                    { backgroundImage: `url('${comment.avatar}')` },
                    STYLE.list.item.avatar
                  ]}></div>

                {/** 2.2. detail */}
                <div style={STYLE.list.item.detail}>

                  {/** 2.2.1. Top info : username + date*/}
                  <div
                    style={[
                      LAYOUT.flexContainer.left,
                      LAYOUT.flexContainer.verticalCenter,
                      STYLE.list.item.detail.topInfo
                    ]}>
                    {/** 2.2.1.1. username */}
                    <div
                      style={TYPOGRAPHY.bodyText.titlePink}>
                      {comment.username}
                    </div>

                    {/** 2.2.1.2. Dor spacer */}
                    <div style={STYLE.list.item.detail.topInfo.dot}></div>

                    {/** 2.2.1.3. date */}
                    <div style={TYPOGRAPHY.bodyText.sub}>
                      {comment.date}
                    </div>
                  </div>

                  {/** 2.2.2. Content */}
                  <div
                    style={[
                      TYPOGRAPHY.bodyText.normal,
                      STYLE.list.item.detail.content
                    ]}>
                    {comment.content}
                  </div>

                  {/** 2.2.3. Sub */}
                  {
                    comment.reply.length > 0 &&
                    <div style={STYLE.list.item.detail.reply}>

                      {/** 2.2.3.1. Sub heading */}
                      <div
                        onClick={() => this.toggleReply(comment)}
                        style={[
                          LAYOUT.flexContainer.left,
                          STYLE.list.item.detail.reply.heading,
                          !comment.replyShow && STYLE.list.item.detail.reply.heading.withBorderBottom,
                        ]}>
                        <div
                          style={STYLE.list.item.detail.reply.heading.icon}
                          className={'lx lx-50-comment'}></div>
                        <div style={TYPOGRAPHY.bodyText.sub}>
                          {comment.reply.length} trả lời
                        </div>
                        <div
                          style={[
                            STYLE.list.item.detail.reply.heading.icon,
                            STYLE.list.item.detail.reply.heading.icon.open
                          ]}
                          className={
                            comment.replyShow
                              ? 'lx lx-47-angle-double-up'
                              : 'lx lx-46-angle-double-down'}>
                        </div>
                      </div>

                      {/** 2.2.3.2. Sub Listing */}
                      {
                        comment.replyShow &&
                        <div style={STYLE.list.item.detail.reply.list}>
                          {
                            comment.reply.map(reply =>
                              /** Reply item */
                              <div
                                style={[LAYOUT.flexContainer.left, STYLE.list.item]}
                                key={`comment-reply-${reply.id}`}>

                                {/** REPLY.1. Avatar  user */}
                                <div
                                  style={[
                                    { backgroundImage: `url('${reply.avatar}')` },
                                    STYLE.list.item.avatar,
                                    STYLE.list.item.avatar.reply,
                                  ]}></div>

                                {/** REPLY.2. detail */}
                                <div style={STYLE.list.item.detail}>

                                  {/** REPLY.2.1. Top info : username + date*/}
                                  <div
                                    style={[
                                      LAYOUT.flexContainer.left,
                                      LAYOUT.flexContainer.verticalCenter,
                                      STYLE.list.item.detail.topInfo
                                    ]}>

                                    {/** REPLY.2.1.1. username */}
                                    <div style={TYPOGRAPHY.bodyText.bold}>
                                      {reply.username}
                                    </div>

                                    {/** REPLY.2.1.2. Dor spacer */}
                                    <div style={STYLE.list.item.detail.topInfo.dot}></div>

                                    {/** REPLY.2.1.3. date */}
                                    <div style={TYPOGRAPHY.bodyText.sub}>
                                      {reply.date}
                                    </div>
                                  </div>

                                  {/** REPLY.2.2. Content */}
                                  <div style={[TYPOGRAPHY.bodyText.normal, STYLE.list.item.detail.content]}>
                                    {reply.content}
                                  </div>
                                </div>
                              </div>
                            )
                          }
                        </div>
                      }
                    </div>
                  }
                </div>
              </div>
            )
          }
        </div>
      </div>
    )
  }
};

export default TabComment;
