/**
 * Created by Thành Võ on 03.04.2017
 */

import * as VARIABLE from '../../../../styles/variable.style';
import MEDIA_QUERIES from '../../../../styles/mediaQueries.style';

export default {
  width: '100%',
  display: 'block',
  paddingTop: 15,

  item: {
    marginBottom: 15,
    padding: '0 15px',

    [MEDIA_QUERIES.mobile610]: {
      flexWrap: 'nowrap',
    },

    thumbnail: {
      width: '100%',
      marginBottom: 15,

      [MEDIA_QUERIES.mobile610]: {
        minWidth: 170,
        paddingRight: 20,
      },

      image: {
        width: '100%',
        height: 'auto',
      }
    },

    info: {
      padding: '0 0 15px',
      borderBottom: `1px solid ${VARIABLE.colorE5}`,

      name: {
        marginBottom: 10
      },

      subInfo: {
        marginBottom: 10,

        dotIcon: {
          width: 6,
          minWidth: 6,
          height: 6,
          borderRadius: 3,
          backgroundColor: VARIABLE.color4D,
          margin: '0 10px',
        }
      },

      detailContent: {
        marginBottom: 10,
      },

      extra: {
        title: {
          cursor: 'pointer',
        },

        icon: {
          width: 20,
          height: 20,
          lineHeight: '20px',
          fontSize: 11,
          margin: '0 5px',
          textAlign: 'center',
          color: VARIABLE.colorPink,
        },

        content: {
          marginTop: 10,
        }
      }
    }
  }
};
