/**
 * Created by Thành Võ on 03.04.2017
 */

import * as React from 'react';
import * as Radium from 'radium';

import DeliverCalculationContainer from '../../DeliverCalculationContainer/'

import * as COMPONENT from '../../../../styles/component.style';
import * as TYPOGRAPHY from '../../../../styles/typography.style';

let IMAGE_BANK_LIST = require('../../../../assets/images/product/bank-list.jpg');

import STYLE from './TabDelivery.style';

/**  PROPS & STATE INTERFACE */
interface ITabDeliveryProps extends React.Props<any> { };

interface ITabDeliveryState { };

@Radium
class TabDelivery extends React.Component<ITabDeliveryProps, any> {
  constructor(props) {
    super(props);

    this.state = {}
  }

  render() {
    return (
      <div style={STYLE}>

        {/** 1. Top info */}
        <div style={STYLE.top}>
          <div style={TYPOGRAPHY.title.normal}>
            Giao hàng trên toàn quốc
          </div>
          <div style={TYPOGRAPHY.bodyText.normal}>
            Để biết thời gian giao hàng và dự kiến, vui lòng nhập thông tin về địa điểm nhận hàng:
          </div>
        </div>

        {/** 2. Select box for country + calculate price */}
        <DeliverCalculationContainer />

        {/** 3. Bottom info */}
        <div style={TYPOGRAPHY.title.normal}>
          THANH TOÁN ONLINE TIỆN LỢI, AN TOÀN
        </div>
        <div style={TYPOGRAPHY.bodyText.normal}>
          Nhận thanh toán trực tiếp qua thẻ ATM, thẻ tín dụng VISA, MasterCard qua cổng OnePay hoặc chuyển khoản qua Vietcombank.
        </div>

        <img style={STYLE.bankList} src={IMAGE_BANK_LIST} />
      </div>
    )
  }
};

export default TabDelivery;
