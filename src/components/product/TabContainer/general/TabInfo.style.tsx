/**
 * Created by Thành Võ on 03.04.2017
 */

import * as VARIABLE from '../../../../styles/variable.style';
import MEDIA_QUERIES from '../../../../styles/mediaQueries.style';

export default {
  width: '100%',
  display: 'block',
  padding: '20px 15px',

  heading: {
    marginBottom: 10
  },

  content: {
    marginBottom: 12,
  },
};
