/**
 * Created by Thành Võ on 03.04.2017
 */

import * as VARIABLE from '../../../../styles/variable.style';
import MEDIA_QUERIES from '../../../../styles/mediaQueries.style';

export default {
  width: '100%',
  display: 'block',
  padding: '25px 15px 15px',

  heading: {
    marginBottom: 40,

    text: {
      textAlign: 'center',
      padding: '0 50px',
      marginBottom: 20,
      width: '100%',
    },

    button: {
      borderRadius: 17
    }
  },

  content: {
    textEmpty: {
      width: '100%',
      textAlign: 'center'
    },

    list: {
      item: {
        paddingBottom: 15,
        borderBottom: `1px solid ${VARIABLE.colorE5}`,
        marginBottom: 15,

        info: {
          marginBottom: 15,

          avatar: {
            width: 50,
            minWidth: 50,
            height: 50,
            borderRadius: 25,
            marginRight: 10,
            backgroundPosition: 'center',
            backgroundSize: 'cover',
          },

          username: {
            paddingRight: 15
          },

          detail: {
            flex: 10,

            groupUsername: {
              marginBottom: 5,
            }
          }
        },

      }
    }
  }
};
