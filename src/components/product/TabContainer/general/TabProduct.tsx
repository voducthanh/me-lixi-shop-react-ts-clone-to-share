/**
 * Created by Thành Võ on 03.04.2017
 */

import * as React from 'react';
import * as Radium from 'radium';

import * as LAYOUT from '../../../../styles/layout.style';
import * as COMPONENT from '../../../../styles/component.style';
import * as TYPOGRAPHY from '../../../../styles/typography.style';

import STYLE from './TabProduct.style';

/**  PROPS & STATE INTERFACE */
interface ITabProductProps extends React.Props<any> {
  product: Array<any>
};

interface ITabProductState {
  product: Array<any>
};

@Radium
class TabProduct extends React.Component<ITabProductProps, any> {
  constructor(props) {
    super(props);

    this.state = {
      product: this.props.product
    }
  }

  /** Toggle to show / hide extra content */
  toggleExtra(_product) {
    this.setState((prevState, props) => ({
      product: prevState.product.map(item => {
        item.extra.show = item.id === _product.id
          ? !item.extra.show
          : item.extra.show;
        return item;
      })
    }));
  }

  render() {
    const { product } = this.state;
    return (
      <div style={STYLE}>
        {
          product.map(product =>
            <div
              style={[
                LAYOUT.flexContainer.wrap,
                STYLE.item
              ]}
              key={`product-detail-${product.id}`}>

              {/** 1. product Thumnail */}
              <div style={STYLE.item.thumbnail}>
                <img
                  style={STYLE.item.thumbnail.image}
                  src={product.image} />
              </div>

              {/** 2. product info  */}
              <div style={STYLE.item.info}>

                {/** 2.1. Product name */}
                <div
                  style={[
                    TYPOGRAPHY.title.normal,
                    STYLE.item.info.name
                  ]}>
                  {product.name}
                </div>

                {/** 2.2. Product subinfo : brand + country */}
                <div
                  style={[
                    LAYOUT.flexContainer.left,
                    LAYOUT.flexContainer.verticalCenter,
                    STYLE.item.info.subInfo
                  ]}>
                  <div style={TYPOGRAPHY.bodyText.border}>
                    {product.brand}
                  </div>
                  <div style={TYPOGRAPHY.bodyText.border}>
                    {product.country}
                  </div>
                </div>

                {/** 2.3. Product detail content: text */}
                <div
                  style={[
                    STYLE.item.info.detailContent,
                    TYPOGRAPHY.bodyText.normal
                  ]}>
                  {product.content}
                </div>

                {/** 2.4. Product extra info */}
                <div style={STYLE.item.info}>

                  {/** 2.4.1. Extra info header */}
                  <div
                    onClick={() => this.toggleExtra(product)}
                    style={[
                      LAYOUT.flexContainer.left,
                      STYLE.item.info.extra.title
                    ]}>
                    <div style={TYPOGRAPHY.bodyText.bold}>
                      {product.extra.title}
                    </div>
                    <i
                      style={STYLE.item.info.extra.icon}
                      className={
                        product.extra.show
                          ? 'lx lx-47-angle-double-up'
                          : 'lx lx-46-angle-double-down'
                      }></i>
                  </div>

                  {/** 2.4.2. Extra info content */}
                  {
                    product.extra.show &&
                    <div
                      style={[
                        TYPOGRAPHY.bodyText.normal,
                        STYLE.item.info.extra.content
                      ]}>
                      {product.extra.content}
                    </div>
                  }
                </div>
              </div>
            </div>
          )
        }
      </div>
    )
  }
};

export default TabProduct;
