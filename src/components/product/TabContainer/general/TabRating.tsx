/**
 * Created by Thành Võ on 03.04.2017
 */

import * as React from 'react';
import * as Radium from 'radium';

import RatingStar from '../../../general/UI/RatingStar';

import * as COMPONENT from '../../../../styles/component.style';
import * as LAYOUT from '../../../../styles/layout.style';
import * as TYPOGRAPHY from '../../../../styles/typography.style';

import STYLE from './TabRating.style';

/**  PROPS & STATE INTERFACE */
interface ITabRatingProps extends React.Props<any> {
  rating: any
};

interface ITabRatingState {
  showNaviTop: boolean
};

@Radium
class TabRating extends React.Component<ITabRatingProps, any> {
  constructor(props) {
    super(props);

    this.state = {}
  }

  render() {
    const { rating } = this.props;

    return (
      <div style={[STYLE]}>

        {/** 1. Heading */}
        <div
          style={[
            LAYOUT.flexContainer.center,
            LAYOUT.flexContainer.wrap,
            STYLE.heading
          ]}>

          {/** 1.1. text */}
          <div
            style={[
              TYPOGRAPHY.bodyText.normal,
              STYLE.heading.text
            ]}>
            Chúng tôi rất muốn lắng nghe ý kiến của bạn về Box này.
          </div>

          {/** 1.2. Spacer */}
          <div></div>

          {/** 1.3. Button */}
          <div
            style={[
              COMPONENT.buttonBorder,
              COMPONENT.buttonBorder.pink,
              STYLE.heading.button
            ]}>
            Đánh giá ngay
          </div>
        </div>

        {/** 2. Content : list rating */}
        {
          rating.list.length === 0
            ? /** 2.1. Empty List */
            <div
              style={[
                TYPOGRAPHY.bodyText.normal,
                STYLE.content.textEmpty
              ]}>
              Hãy trở thành người đầu tiên đánh giá box này
            </div>
            : /** 2.2. List Rating detail */
            <div style={STYLE.content.list}>
              {
                rating.list.map(rating =>
                  /** Item rating */
                  <div
                    style={STYLE.content.list.item}
                    key={`rating-item-${rating.id}`}>

                    {/** 2.2.1. Item : Info */}
                    <div
                      style={[
                        LAYOUT.flexContainer.left,
                        STYLE.content.list.item.info
                      ]}>

                      {/** 2.2.1.1. Item : Info : Avatar user */}
                      <div
                        style={[
                          { backgroundImage: `url('${rating.avatar}')` },
                          STYLE.content.list.item.info.avatar
                        ]}></div>
                      <div style={STYLE.content.list.item.info.detail}>

                        {/** 2.2.1.2. Item : Info : username + Date */}
                        <div
                          style={[
                            LAYOUT.flexContainer.justify,
                            STYLE.content.list.item.info.detail.groupUsername
                          ]}>
                          <div
                            style={[
                              TYPOGRAPHY.bodyText.bold,
                              STYLE.content.list.item.info.username,
                            ]}>
                            {rating.username}
                          </div>
                          <div style={TYPOGRAPHY.bodyText.sub}>
                            {rating.date}
                          </div>
                        </div>

                        {/** 2.2.1.3. Item : Info : Star rating component */}
                        <RatingStar value={rating.value} />
                      </div>
                    </div>

                    {/** 2.2.2. Item : Content Detail */}
                    <div
                      style={TYPOGRAPHY.bodyText.normal}>
                      {rating.content}
                    </div>
                  </div>
                )
              }
            </div>
        }
      </div >
    )
  }
};

export default TabRating;
