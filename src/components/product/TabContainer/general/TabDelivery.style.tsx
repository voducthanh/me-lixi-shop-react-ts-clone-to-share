/**
 * Created by Thành Võ on 03.04.2017
 */

import * as VARIABLE from '../../../../styles/variable.style';
import MEDIA_QUERIES from '../../../../styles/mediaQueries.style';

export default {
  width: '100%',
  display: 'block',
  padding: '25px 15px 15px',

  top: {
    marginBottom: 10
  },

  selectCountry: {},

  infoDelivery: {
    icon: {},

    info: {
      value: {
        color: VARIABLE.colorPink
      }
    }
  },

  bankList: {
    maxWidth: '100%',
    height: 'auto'
  }
};
