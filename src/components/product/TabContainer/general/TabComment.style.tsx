/**
 * Created by Thành Võ on 03.04.2017
 */

import * as VARIABLE from '../../../../styles/variable.style';
import MEDIA_QUERIES from '../../../../styles/mediaQueries.style';

export default {
  width: '100%',
  display: 'block',
  padding: '25px 15px 15px',

  formReply: {
    avatar: {},
    content: {
      input: {},
      buttonReply: {},
      buttonSend: {},
    },
  },

  list: {
    item: {
      marginBottom: 20,

      avatar: {
        width: 50,
        minWidth: 50,
        height: 50,
        borderRadius: 25,
        marginRight: 10,
        backgroundPosition: 'center',
        backgroundSize: 'cover',

        reply: {
          width: 40,
          minWidth: 40,
          height: 40,
          borderRadius: 20,
        }
      },

      detail: {
        topInfo: {
          marginBottom: 10,

          dot: {
            width: 4,
            minWidth: 4,
            height: 4,
            borderRadius: 2,
            backgroundColor: VARIABLE.color97,
            margin: '0 7px',
          }
        },

        content: {
          paddingBottom: 10,
          borderBottom: `1px solid ${VARIABLE.colorE5}`
        },

        reply: {
          paddingTop: 15,

          heading: {
            cursor: 'pointer',
            paddingBottom: 15,

            withBorderBottom: {
              borderBottom: `1px solid ${VARIABLE.colorE5}`
            },

            icon: {
              width: 20,
              height: 20,
              fontSize: 14,
              lineHeight: '20px',
              textAlign: 'center',
              color: VARIABLE.color97,
              marginRight: 10,

              open: {
                fontSize: 11,
                marginLeft: 10,
                color: VARIABLE.colorPink
              }
            }
          },
          list: {}
        }
      }
    }
  }
};
