/**
 * Created by Thành Võ on 03.04.2017
 */

import * as VARIABLE from '../../../styles/variable.style';
import MEDIA_QUERIES from '../../../styles/mediaQueries.style';

export default {
  display: 'block',
  width: '100%',
  marginBottom: 30,

  main: {
    width: '100%',
    marginBottom: 20,

    image: {
      width: '100%',
      height: 'auto',
      paddingTop: '67.7425%',
      backgroundSize: 'cover',
      backgroundPosition: 'center',
    }
  },

  list: {
    container: {
      paddingLeft: 15,
      paddingRight: 15,
      transition: VARIABLE.transitionNormal,
      margin: '0 auto',
    },

    item: {
      width: 80,
      minWidth: 80,
      height: 54,
      backgroundSize: 'cover',
      backgroundPosition: 'center',
      margin: 5,
      cursor: 'pointer',
      opacity: .4,

      ':hover': {
        boxShadow: VARIABLE.shadowBorderGray,
      },

      selected: {
        boxShadow: VARIABLE.shadowBorderBlack,
        pointerEvents: 'none',
        opacity: 1,

        ':hover': {
          boxShadow: VARIABLE.shadowBorderBlack,
        },
      }
    },
  },
};
