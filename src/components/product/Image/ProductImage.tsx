/**
 * Created by Thành Võ on 03.04.2017
 */

import * as React from 'react';
import * as Radium from 'radium';

import * as LAYOUT from '../../../styles/layout.style';

import STYLE from './ProductImage.style';

/**  PROPS & STATE INTERFACE */
interface IProductImageProps extends React.Props<any> {
  changeImage: any,
  imageList: Array<any>
};

interface IProductImageState {
  translateListImage: number
};

@Radium
class ProductImage extends React.Component<IProductImageProps, any> {
  constructor(props: IProductImageProps) {
    super(props);

    this.state = {
      translateListImage: 0
    }
  }

  selectImage(_image, _index) {
    /** Emit event upto parent */
    this.props.changeImage(_image);

    /** Upadate layout: translate list image */
    let offsetWidth = this.props.imageList.length * 90 + 30 - window.innerWidth;
    let maxIndex = Math.floor(offsetWidth / 90) + 2;
    maxIndex = maxIndex < 1 ? 1 : maxIndex;

    let stepTranslate = _index === 0
      ? 1
      : (_index >= maxIndex ? maxIndex : _index);

    this.setState({
      translateListImage: (stepTranslate - 1) * 90 * (-1)
    });
  }

  render() {
    const { imageList } = this.props;
    const { translateListImage } = this.state;

    return (
      <div style={STYLE}>
        {/** 1. Main Image */}
        <div style={STYLE.main}>
          <div
            style={[
              { backgroundImage: `url('${imageList.filter(image => image.selected)[0].image}')` },
              STYLE.main.image
            ]}></div>
        </div>

        {/** List image : only show when have from 2 image */}
        {
          imageList.length > 1 &&
          <div>
            <div
              style={[
                {
                  transform: `translate(${translateListImage}px, 0)`,
                  width: imageList.length * 90 + 30
                },
                LAYOUT.flexContainer.left,
                STYLE.list.container
              ]}>
              {
                imageList.map((image, $index) =>
                  <div
                    key={`product-image-${image.id}`}
                    onClick={() => this.selectImage(image, $index)}
                    style={[
                      { backgroundImage: `url('${image.image}')` },
                      STYLE.list.item,
                      image.selected && STYLE.list.item.selected,
                    ]}></div>
                )
              }
            </div>
          </div>
        }
      </div>
    )
  }
};

export default ProductImage;
