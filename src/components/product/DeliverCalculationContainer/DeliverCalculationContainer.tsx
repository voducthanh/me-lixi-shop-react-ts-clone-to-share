/**
 * Created by Thành Võ on 03.04.2017
 */

import * as React from 'react';
import * as Radium from 'radium';

import SelectBox from '../../general/UI/SelectBox';

import * as TYPOGRAPHY from '../../../styles/typography.style';
import * as LAYOUT from '../../../styles/layout.style';

import STYLE from './DeliverCalculationContainer.style';

/**  PROPS & STATE INTERFACE */
interface IDeliverCalculationContainerProps extends React.Props<any> {
  routes: any
};

interface IDeliverCalculationContainerState {
  cityList: Array<any>,
  districtList: Array<any>,
  valueCalculated: object
};

@Radium
class DeliverCalculationContainer extends React.Component<IDeliverCalculationContainerProps, any> {
  constructor(props) {
    super(props);

    this.state = {
      cityList: [
        {
          id: 1,
          title: 'TP. Hồ Chí Minh',
          selected: false,
          district: [
            {
              id: 1,
              title: '1. Tên Quận Demo 1',
              price: '15000 đ',
              time: '1 ngày'
            },
            {
              id: 2,
              title: '1. Tên Quận Demo 2',
              price: '45000 đ',
              time: '3 ngày'
            },
            {
              id: 3,
              title: '1. Tên Quận Demo 3',
              price: '95000 đ',
              time: '6 ngày'
            },
            {
              id: 4,
              title: '1. Tên Quận Demo 4',
              price: '25000 đ',
              time: '2 ngày'
            },
            {
              id: 5,
              title: '1. Tên Quận Demo 5',
              price: '15000 đ',
              time: '6 ngày'
            },
            {
              id: 6,
              title: '1. Tên Quận Demo 6',
              price: '25000 đ',
              time: '1 ngày'
            },
            {
              id: 7,
              title: '1. Tên Quận Demo 7',
              price: '25000 đ',
              time: '3 ngày'
            },
            {
              id: 8,
              title: '1. Tên Quận Demo 8',
              price: '45000 đ',
              time: '3 ngày'
            },
            {
              id: 9,
              title: '1. Tên Quận Demo 9',
              price: '45000 đ',
              time: '3 ngày'
            },
            {
              id: 10,
              title: '1. Tên Quận Demo 10',
              price: '45000 đ',
              time: '3 ngày'
            },
            {
              id: 11,
              title: '1. Tên Quận Demo 11',
              price: '15000 đ',
              time: '3 ngày'
            },
            {
              id: 12,
              title: '1. Tên Quận Demo 12',
              price: '25000 đ',
              time: '3 ngày'
            },
            {
              id: 13,
              title: '1. Tên Quận Demo 13',
              price: '45000 đ',
              time: '3 ngày'
            },
            {
              id: 14,
              title: '1. Tên Quận Demo 14',
              price: '25000 đ',
              time: '3 ngày'
            },
          ]
        },
        {
          id: 2,
          title: 'Hà Nội',
          selected: false,
          district: [
            {
              id: 1,
              title: '2. Tên Quận Demo 1',
              price: '15000 đ',
              time: '1 ngày'
            },
            {
              id: 2,
              title: '2. Tên Quận Demo 2',
              price: '45000 đ',
              time: '3 ngày'
            },
            {
              id: 3,
              title: '2. Tên Quận Demo 3',
              price: '95000 đ',
              time: '6 ngày'
            },
            {
              id: 4,
              title: '2. Tên Quận Demo 4',
              price: '25000 đ',
              time: '2 ngày'
            },
            {
              id: 5,
              title: '2. Tên Quận Demo 5',
              price: '15000 đ',
              time: '6 ngày'
            },
            {
              id: 6,
              title: '2. Tên Quận Demo 6',
              price: '25000 đ',
              time: '1 ngày'
            },
            {
              id: 7,
              title: '2. Tên Quận Demo 7',
              price: '25000 đ',
              time: '3 ngày'
            },
            {
              id: 8,
              title: '2. Tên Quận Demo 8',
              price: '45000 đ',
              time: '3 ngày'
            },
            {
              id: 9,
              title: '2. Tên Quận Demo 9',
              price: '45000 đ',
              time: '3 ngày'
            },
            {
              id: 10,
              title: '2. Tên Quận Demo 10',
              price: '45000 đ',
              time: '3 ngày'
            },
            {
              id: 11,
              title: '2. Tên Quận Demo 11',
              price: '15000 đ',
              time: '3 ngày'
            },
            {
              id: 12,
              title: '2. Tên Quận Demo 12',
              price: '25000 đ',
              time: '3 ngày'
            },
            {
              id: 13,
              title: '2. Tên Quận Demo 13',
              price: '45000 đ',
              time: '3 ngày'
            },
            {
              id: 14,
              title: '2. Tên Quận Demo 14',
              price: '25000 đ',
              time: '3 ngày'
            },
          ]
        },
        {
          id: 3,
          title: 'Bình Dương',
          selected: false,
          district: [
            {
              id: 1,
              title: 'Tên Quận Demo 1',
              price: '15000 đ',
              time: '1 ngày'
            },
            {
              id: 2,
              title: 'Tên Quận Demo 2',
              price: '45000 đ',
              time: '3 ngày'
            },
            {
              id: 3,
              title: 'Tên Quận Demo 3',
              price: '95000 đ',
              time: '6 ngày'
            },
            {
              id: 4,
              title: 'Tên Quận Demo 4',
              price: '25000 đ',
              time: '2 ngày'
            },
            {
              id: 5,
              title: 'Tên Quận Demo 5',
              price: '15000 đ',
              time: '6 ngày'
            },
            {
              id: 6,
              title: 'Tên Quận Demo 6',
              price: '25000 đ',
              time: '1 ngày'
            },
            {
              id: 7,
              title: 'Tên Quận Demo 7',
              price: '25000 đ',
              time: '3 ngày'
            },
            {
              id: 8,
              title: 'Tên Quận Demo 8',
              price: '45000 đ',
              time: '3 ngày'
            },
            {
              id: 9,
              title: 'Tên Quận Demo 9',
              price: '45000 đ',
              time: '3 ngày'
            },
            {
              id: 10,
              title: 'Tên Quận Demo 10',
              price: '45000 đ',
              time: '3 ngày'
            },
            {
              id: 11,
              title: 'Tên Quận Demo 11',
              price: '15000 đ',
              time: '3 ngày'
            },
            {
              id: 12,
              title: 'Tên Quận Demo 12',
              price: '25000 đ',
              time: '3 ngày'
            },
            {
              id: 13,
              title: 'Tên Quận Demo 13',
              price: '45000 đ',
              time: '3 ngày'
            },
            {
              id: 14,
              title: 'Tên Quận Demo 14',
              price: '25000 đ',
              time: '3 ngày'
            },
          ]
        },
        {
          id: 4,
          title: 'Long An',
          selected: false,
          district: [
            {
              id: 1,
              title: 'Tên Quận Demo 1',
              price: '15000 đ',
              time: '1 ngày'
            },
            {
              id: 2,
              title: 'Tên Quận Demo 2',
              price: '45000 đ',
              time: '3 ngày'
            },
            {
              id: 3,
              title: 'Tên Quận Demo 3',
              price: '95000 đ',
              time: '6 ngày'
            },
            {
              id: 4,
              title: 'Tên Quận Demo 4',
              price: '25000 đ',
              time: '2 ngày'
            },
            {
              id: 5,
              title: 'Tên Quận Demo 5',
              price: '15000 đ',
              time: '6 ngày'
            },
            {
              id: 6,
              title: 'Tên Quận Demo 6',
              price: '25000 đ',
              time: '1 ngày'
            },
            {
              id: 7,
              title: 'Tên Quận Demo 7',
              price: '25000 đ',
              time: '3 ngày'
            },
            {
              id: 8,
              title: 'Tên Quận Demo 8',
              price: '45000 đ',
              time: '3 ngày'
            },
            {
              id: 9,
              title: 'Tên Quận Demo 9',
              price: '45000 đ',
              time: '3 ngày'
            },
            {
              id: 10,
              title: 'Tên Quận Demo 10',
              price: '45000 đ',
              time: '3 ngày'
            },
            {
              id: 11,
              title: 'Tên Quận Demo 11',
              price: '15000 đ',
              time: '3 ngày'
            },
            {
              id: 12,
              title: 'Tên Quận Demo 12',
              price: '25000 đ',
              time: '3 ngày'
            },
            {
              id: 13,
              title: 'Tên Quận Demo 13',
              price: '45000 đ',
              time: '3 ngày'
            },
            {
              id: 14,
              title: 'Tên Quận Demo 14',
              price: '25000 đ',
              time: '3 ngày'
            },
          ]
        },
        {
          id: 5,
          title: 'Đồng Nai',
          selected: false,
          district: [
            {
              id: 1,
              title: 'Tên Quận Demo 1',
              price: '15000 đ',
              time: '1 ngày'
            },
            {
              id: 2,
              title: 'Tên Quận Demo 2',
              price: '45000 đ',
              time: '3 ngày'
            },
            {
              id: 3,
              title: 'Tên Quận Demo 3',
              price: '95000 đ',
              time: '6 ngày'
            },
            {
              id: 4,
              title: 'Tên Quận Demo 4',
              price: '25000 đ',
              time: '2 ngày'
            },
            {
              id: 5,
              title: 'Tên Quận Demo 5',
              price: '15000 đ',
              time: '6 ngày'
            },
            {
              id: 6,
              title: 'Tên Quận Demo 6',
              price: '25000 đ',
              time: '1 ngày'
            },
            {
              id: 7,
              title: 'Tên Quận Demo 7',
              price: '25000 đ',
              time: '3 ngày'
            },
            {
              id: 8,
              title: 'Tên Quận Demo 8',
              price: '45000 đ',
              time: '3 ngày'
            },
            {
              id: 9,
              title: 'Tên Quận Demo 9',
              price: '45000 đ',
              time: '3 ngày'
            },
            {
              id: 10,
              title: 'Tên Quận Demo 10',
              price: '45000 đ',
              time: '3 ngày'
            },
            {
              id: 11,
              title: 'Tên Quận Demo 11',
              price: '15000 đ',
              time: '3 ngày'
            },
            {
              id: 12,
              title: 'Tên Quận Demo 12',
              price: '25000 đ',
              time: '3 ngày'
            },
            {
              id: 13,
              title: 'Tên Quận Demo 13',
              price: '45000 đ',
              time: '3 ngày'
            },
            {
              id: 14,
              title: 'Tên Quận Demo 14',
              price: '25000 đ',
              time: '3 ngày'
            },
          ]
        },
        {
          id: 6,
          title: 'Cần Thơ',
          selected: false,
          district: [
            {
              id: 1,
              title: 'Tên Quận Demo 1',
              price: '15000 đ',
              time: '1 ngày'
            },
            {
              id: 2,
              title: 'Tên Quận Demo 2',
              price: '45000 đ',
              time: '3 ngày'
            },
            {
              id: 3,
              title: 'Tên Quận Demo 3',
              price: '95000 đ',
              time: '6 ngày'
            },
            {
              id: 4,
              title: 'Tên Quận Demo 4',
              price: '25000 đ',
              time: '2 ngày'
            },
            {
              id: 5,
              title: 'Tên Quận Demo 5',
              price: '15000 đ',
              time: '6 ngày'
            },
            {
              id: 6,
              title: 'Tên Quận Demo 6',
              price: '25000 đ',
              time: '1 ngày'
            },
            {
              id: 7,
              title: 'Tên Quận Demo 7',
              price: '25000 đ',
              time: '3 ngày'
            },
            {
              id: 8,
              title: 'Tên Quận Demo 8',
              price: '45000 đ',
              time: '3 ngày'
            },
            {
              id: 9,
              title: 'Tên Quận Demo 9',
              price: '45000 đ',
              time: '3 ngày'
            },
            {
              id: 10,
              title: 'Tên Quận Demo 10',
              price: '45000 đ',
              time: '3 ngày'
            },
            {
              id: 11,
              title: 'Tên Quận Demo 11',
              price: '15000 đ',
              time: '3 ngày'
            },
            {
              id: 12,
              title: 'Tên Quận Demo 12',
              price: '25000 đ',
              time: '3 ngày'
            },
            {
              id: 13,
              title: 'Tên Quận Demo 13',
              price: '45000 đ',
              time: '3 ngày'
            },
            {
              id: 14,
              title: 'Tên Quận Demo 14',
              price: '25000 đ',
              time: '3 ngày'
            },
          ]
        },
        {
          id: 7,
          title: 'Đã Nẵng',
          selected: false,
          district: [
            {
              id: 1,
              title: 'Tên Quận Demo 1',
              price: '15000 đ',
              time: '1 ngày'
            },
            {
              id: 2,
              title: 'Tên Quận Demo 2',
              price: '45000 đ',
              time: '3 ngày'
            },
            {
              id: 3,
              title: 'Tên Quận Demo 3',
              price: '95000 đ',
              time: '6 ngày'
            },
            {
              id: 4,
              title: 'Tên Quận Demo 4',
              price: '25000 đ',
              time: '2 ngày'
            },
            {
              id: 5,
              title: 'Tên Quận Demo 5',
              price: '15000 đ',
              time: '6 ngày'
            },
            {
              id: 6,
              title: 'Tên Quận Demo 6',
              price: '25000 đ',
              time: '1 ngày'
            },
            {
              id: 7,
              title: 'Tên Quận Demo 7',
              price: '25000 đ',
              time: '3 ngày'
            },
            {
              id: 8,
              title: 'Tên Quận Demo 8',
              price: '45000 đ',
              time: '3 ngày'
            },
            {
              id: 9,
              title: 'Tên Quận Demo 9',
              price: '45000 đ',
              time: '3 ngày'
            },
            {
              id: 10,
              title: 'Tên Quận Demo 10',
              price: '45000 đ',
              time: '3 ngày'
            },
            {
              id: 11,
              title: 'Tên Quận Demo 11',
              price: '15000 đ',
              time: '3 ngày'
            },
            {
              id: 12,
              title: 'Tên Quận Demo 12',
              price: '25000 đ',
              time: '3 ngày'
            },
            {
              id: 13,
              title: 'Tên Quận Demo 13',
              price: '45000 đ',
              time: '3 ngày'
            },
            {
              id: 14,
              title: 'Tên Quận Demo 14',
              price: '25000 đ',
              time: '3 ngày'
            },
          ]
        },
        {
          id: 8,
          title: 'Sóc Trăng',
          selected: false,
          district: [
            {
              id: 1,
              title: 'Tên Quận Demo 1',
              price: '15000 đ',
              time: '1 ngày'
            },
            {
              id: 2,
              title: 'Tên Quận Demo 2',
              price: '45000 đ',
              time: '3 ngày'
            },
            {
              id: 3,
              title: 'Tên Quận Demo 3',
              price: '95000 đ',
              time: '6 ngày'
            },
            {
              id: 4,
              title: 'Tên Quận Demo 4',
              price: '25000 đ',
              time: '2 ngày'
            },
            {
              id: 5,
              title: 'Tên Quận Demo 5',
              price: '15000 đ',
              time: '6 ngày'
            },
            {
              id: 6,
              title: 'Tên Quận Demo 6',
              price: '25000 đ',
              time: '1 ngày'
            },
            {
              id: 7,
              title: 'Tên Quận Demo 7',
              price: '25000 đ',
              time: '3 ngày'
            },
            {
              id: 8,
              title: 'Tên Quận Demo 8',
              price: '45000 đ',
              time: '3 ngày'
            },
            {
              id: 9,
              title: 'Tên Quận Demo 9',
              price: '45000 đ',
              time: '3 ngày'
            },
            {
              id: 10,
              title: 'Tên Quận Demo 10',
              price: '45000 đ',
              time: '3 ngày'
            },
            {
              id: 11,
              title: 'Tên Quận Demo 11',
              price: '15000 đ',
              time: '3 ngày'
            },
            {
              id: 12,
              title: 'Tên Quận Demo 12',
              price: '25000 đ',
              time: '3 ngày'
            },
            {
              id: 13,
              title: 'Tên Quận Demo 13',
              price: '45000 đ',
              time: '3 ngày'
            },
            {
              id: 14,
              title: 'Tên Quận Demo 14',
              price: '25000 đ',
              time: '3 ngày'
            },
          ]
        },
        {
          id: 9,
          title: 'Bình Định',
          selected: false,
          district: [
            {
              id: 1,
              title: 'Tên Quận Demo 1',
              price: '15000 đ',
              time: '1 ngày'
            },
            {
              id: 2,
              title: 'Tên Quận Demo 2',
              price: '45000 đ',
              time: '3 ngày'
            },
            {
              id: 3,
              title: 'Tên Quận Demo 3',
              price: '95000 đ',
              time: '6 ngày'
            },
            {
              id: 4,
              title: 'Tên Quận Demo 4',
              price: '25000 đ',
              time: '2 ngày'
            },
            {
              id: 5,
              title: 'Tên Quận Demo 5',
              price: '15000 đ',
              time: '6 ngày'
            },
            {
              id: 6,
              title: 'Tên Quận Demo 6',
              price: '25000 đ',
              time: '1 ngày'
            },
            {
              id: 7,
              title: 'Tên Quận Demo 7',
              price: '25000 đ',
              time: '3 ngày'
            },
            {
              id: 8,
              title: 'Tên Quận Demo 8',
              price: '45000 đ',
              time: '3 ngày'
            },
            {
              id: 9,
              title: 'Tên Quận Demo 9',
              price: '45000 đ',
              time: '3 ngày'
            },
            {
              id: 10,
              title: 'Tên Quận Demo 10',
              price: '45000 đ',
              time: '3 ngày'
            },
            {
              id: 11,
              title: 'Tên Quận Demo 11',
              price: '15000 đ',
              time: '3 ngày'
            },
            {
              id: 12,
              title: 'Tên Quận Demo 12',
              price: '25000 đ',
              time: '3 ngày'
            },
            {
              id: 13,
              title: 'Tên Quận Demo 13',
              price: '45000 đ',
              time: '3 ngày'
            },
            {
              id: 14,
              title: 'Tên Quận Demo 14',
              price: '25000 đ',
              time: '3 ngày'
            },
          ]
        },
        {
          id: 10,
          title: 'Cà Mau',
          selected: false,
          district: [
            {
              id: 1,
              title: 'Tên Quận Demo 1',
              price: '15000 đ',
              time: '1 ngày'
            },
            {
              id: 2,
              title: 'Tên Quận Demo 2',
              price: '45000 đ',
              time: '3 ngày'
            },
            {
              id: 3,
              title: 'Tên Quận Demo 3',
              price: '95000 đ',
              time: '6 ngày'
            },
            {
              id: 4,
              title: 'Tên Quận Demo 4',
              price: '25000 đ',
              time: '2 ngày'
            },
            {
              id: 5,
              title: 'Tên Quận Demo 5',
              price: '15000 đ',
              time: '6 ngày'
            },
            {
              id: 6,
              title: 'Tên Quận Demo 6',
              price: '25000 đ',
              time: '1 ngày'
            },
            {
              id: 7,
              title: 'Tên Quận Demo 7',
              price: '25000 đ',
              time: '3 ngày'
            },
            {
              id: 8,
              title: 'Tên Quận Demo 8',
              price: '45000 đ',
              time: '3 ngày'
            },
            {
              id: 9,
              title: 'Tên Quận Demo 9',
              price: '45000 đ',
              time: '3 ngày'
            },
            {
              id: 10,
              title: 'Tên Quận Demo 10',
              price: '45000 đ',
              time: '3 ngày'
            },
            {
              id: 11,
              title: 'Tên Quận Demo 11',
              price: '15000 đ',
              time: '3 ngày'
            },
            {
              id: 12,
              title: 'Tên Quận Demo 12',
              price: '25000 đ',
              time: '3 ngày'
            },
            {
              id: 13,
              title: 'Tên Quận Demo 13',
              price: '45000 đ',
              time: '3 ngày'
            },
            {
              id: 14,
              title: 'Tên Quận Demo 14',
              price: '25000 đ',
              time: '3 ngày'
            },
          ]
        },
        {
          id: 11,
          title: 'Hải Phòng',
          selected: false,
          district: [
            {
              id: 1,
              title: 'Tên Quận Demo 1',
              price: '15000 đ',
              time: '1 ngày'
            },
            {
              id: 2,
              title: 'Tên Quận Demo 2',
              price: '45000 đ',
              time: '3 ngày'
            },
            {
              id: 3,
              title: 'Tên Quận Demo 3',
              price: '95000 đ',
              time: '6 ngày'
            },
            {
              id: 4,
              title: 'Tên Quận Demo 4',
              price: '25000 đ',
              time: '2 ngày'
            },
            {
              id: 5,
              title: 'Tên Quận Demo 5',
              price: '15000 đ',
              time: '6 ngày'
            },
            {
              id: 6,
              title: 'Tên Quận Demo 6',
              price: '25000 đ',
              time: '1 ngày'
            },
            {
              id: 7,
              title: 'Tên Quận Demo 7',
              price: '25000 đ',
              time: '3 ngày'
            },
            {
              id: 8,
              title: 'Tên Quận Demo 8',
              price: '45000 đ',
              time: '3 ngày'
            },
            {
              id: 9,
              title: 'Tên Quận Demo 9',
              price: '45000 đ',
              time: '3 ngày'
            },
            {
              id: 10,
              title: 'Tên Quận Demo 10',
              price: '45000 đ',
              time: '3 ngày'
            },
            {
              id: 11,
              title: 'Tên Quận Demo 11',
              price: '15000 đ',
              time: '3 ngày'
            },
            {
              id: 12,
              title: 'Tên Quận Demo 12',
              price: '25000 đ',
              time: '3 ngày'
            },
            {
              id: 13,
              title: 'Tên Quận Demo 13',
              price: '45000 đ',
              time: '3 ngày'
            },
            {
              id: 14,
              title: 'Tên Quận Demo 14',
              price: '25000 đ',
              time: '3 ngày'
            },
          ]
        },
        {
          id: 12,
          title: 'Lạng Sơn',
          selected: false,
          district: [
            {
              id: 1,
              title: 'Tên Quận Demo 1',
              price: '15000 đ',
              time: '1 ngày'
            },
            {
              id: 2,
              title: 'Tên Quận Demo 2',
              price: '45000 đ',
              time: '3 ngày'
            },
            {
              id: 3,
              title: 'Tên Quận Demo 3',
              price: '95000 đ',
              time: '6 ngày'
            },
            {
              id: 4,
              title: 'Tên Quận Demo 4',
              price: '25000 đ',
              time: '2 ngày'
            },
            {
              id: 5,
              title: 'Tên Quận Demo 5',
              price: '15000 đ',
              time: '6 ngày'
            },
            {
              id: 6,
              title: 'Tên Quận Demo 6',
              price: '25000 đ',
              time: '1 ngày'
            },
            {
              id: 7,
              title: 'Tên Quận Demo 7',
              price: '25000 đ',
              time: '3 ngày'
            },
            {
              id: 8,
              title: 'Tên Quận Demo 8',
              price: '45000 đ',
              time: '3 ngày'
            },
            {
              id: 9,
              title: 'Tên Quận Demo 9',
              price: '45000 đ',
              time: '3 ngày'
            },
            {
              id: 10,
              title: 'Tên Quận Demo 10',
              price: '45000 đ',
              time: '3 ngày'
            },
            {
              id: 11,
              title: 'Tên Quận Demo 11',
              price: '15000 đ',
              time: '3 ngày'
            },
            {
              id: 12,
              title: 'Tên Quận Demo 12',
              price: '25000 đ',
              time: '3 ngày'
            },
            {
              id: 13,
              title: 'Tên Quận Demo 13',
              price: '45000 đ',
              time: '3 ngày'
            },
            {
              id: 14,
              title: 'Tên Quận Demo 14',
              price: '25000 đ',
              time: '3 ngày'
            },
          ]
        },
        {
          id: 13,
          title: 'Yên Bái',
          selected: false,
          district: [
            {
              id: 1,
              title: 'Tên Quận Demo 1',
              price: '15000 đ',
              time: '1 ngày'
            },
            {
              id: 2,
              title: 'Tên Quận Demo 2',
              price: '45000 đ',
              time: '3 ngày'
            },
            {
              id: 3,
              title: 'Tên Quận Demo 3',
              price: '95000 đ',
              time: '6 ngày'
            },
            {
              id: 4,
              title: 'Tên Quận Demo 4',
              price: '25000 đ',
              time: '2 ngày'
            },
            {
              id: 5,
              title: 'Tên Quận Demo 5',
              price: '15000 đ',
              time: '6 ngày'
            },
            {
              id: 6,
              title: 'Tên Quận Demo 6',
              price: '25000 đ',
              time: '1 ngày'
            },
            {
              id: 7,
              title: 'Tên Quận Demo 7',
              price: '25000 đ',
              time: '3 ngày'
            },
            {
              id: 8,
              title: 'Tên Quận Demo 8',
              price: '45000 đ',
              time: '3 ngày'
            },
            {
              id: 9,
              title: 'Tên Quận Demo 9',
              price: '45000 đ',
              time: '3 ngày'
            },
            {
              id: 10,
              title: 'Tên Quận Demo 10',
              price: '45000 đ',
              time: '3 ngày'
            },
            {
              id: 11,
              title: 'Tên Quận Demo 11',
              price: '15000 đ',
              time: '3 ngày'
            },
            {
              id: 12,
              title: 'Tên Quận Demo 12',
              price: '25000 đ',
              time: '3 ngày'
            },
            {
              id: 13,
              title: 'Tên Quận Demo 13',
              price: '45000 đ',
              time: '3 ngày'
            },
            {
              id: 14,
              title: 'Tên Quận Demo 14',
              price: '25000 đ',
              time: '3 ngày'
            },
          ]
        }
      ],
      districtList: [],
      valueCalculated: {
        price: 'không xác định',
        time: 'không xác định'
      }
    }
  }

  /**
   * Handle Event select city
   * @param {*} _city data received from select box
   * 1. Assign list district by city selected
   * 2. Reset value calculated to 'không xác định'
   */
  handleOnChangeCity(_city) {
    this.setState((prevState, props) => ({
      districtList: prevState.cityList.filter(
        city => city.id === _city.id
      )[0].district.map(item => {
        item.selected = false;
        item.hover = false;
        return item;
      }),
      valueCalculated: {
        price: 'không xác định',
        time: 'không xác định'
      }
    }));
  }

  /**
   * Hangle Event select district
   * @param {*} _district data receivec from select box
   * Calculate price for delivery
   */
  handleOnChangeDistrict(_district) {
    this.setState((prevState, props) => ({
      districtList: prevState.districtList.map(district => {
        district.selected = district.id === _district.id;
        return district;
      }),
      valueCalculated: {
        price: _district.price,
        time: _district.time
      }
    }));
  }

  render() {
    const { cityList, districtList, valueCalculated } = this.state;

    return (
      <div style={STYLE}>

        {/** 1. Select city */}
        <SelectBox
          style={STYLE.selectBox}
          list={cityList}
          title={'Chọn Tỉnh / Thành phố'}
          search={'Tìm kiếm Tỉnh / Thành phố'}
          onChange={this.handleOnChangeCity.bind(this)}
        />

        {/** 2. select district  */}
        {
          districtList.length > 0 &&
          <SelectBox
            style={STYLE.selectBox}
            list={districtList}
            title={'Chọn Quận / Huyện'}
            search={'Tìm kiếm Quận Huyện'}
            onChange={this.handleOnChangeDistrict.bind(this)}
          />
        }

        {/** 3. value calculated */}
        {/** 3.1. Calculate for delivery */}
        <div style={[LAYOUT.flexContainer.left, STYLE.result]}>
          <i style={STYLE.result.icon} className="lx lx-52-calendar"></i>
          <div style={[TYPOGRAPHY.bodyText.bold, STYLE.result.heading]}>
            Giao hàng tiêu chuẩn:
          </div>
          <div style={[TYPOGRAPHY.bodyText.textPink, STYLE.result.value]}>
            {valueCalculated.price}
          </div>
        </div>

        {/** 3.2. Calculate for time delivery */}
        <div style={[LAYOUT.flexContainer.left, STYLE.result]}>
          <i style={STYLE.result.icon} className="lx lx-40-delivery"></i>
          <div style={[TYPOGRAPHY.bodyText.bold, STYLE.result.heading]}>
            Thời gian giao dự kiến:
          </div>
          <div style={[TYPOGRAPHY.bodyText.textPink, STYLE.result.value]}>
            {valueCalculated.time}
          </div>
        </div>

      </div>
    )
  }
};

export default DeliverCalculationContainer;
