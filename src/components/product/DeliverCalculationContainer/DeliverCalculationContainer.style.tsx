/**
 * Created by Thành Võ on 03.04.2017
 */

import * as VARIABLE from '../../../styles/variable.style';
import MEDIA_QUERIES from '../../../styles/mediaQueries.style';

export default {
  display: 'block',
  width: '100%',
  marginBottom: 20,

  selectBox: {
    marginBottom: 15
  },

  result: {
    marginBottom: 10,

    icon: {
      width: 20,
      minWidth: 20,
      height: 20,
      textAlign: 'center',
      lineHeight: '20px',
      marginRight: 10,
    },

    heading: {
      marginRight: 10,
      minWidth: 150
    },

    value: {
      textAlign: 'left'
    }
  }
};
