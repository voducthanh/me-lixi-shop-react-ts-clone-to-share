/**
 * Created by Thành Võ on 03.04.2017
 */

import * as React from 'react';
import * as Radium from 'radium';

import ProductPrice from '../../product/Price';
import AddToCart from '../../product/button/AddToCart';
import AddWishList from '../../product/button/AddWishList';

import * as LAYOUT from '../../../styles/layout.style';

import STYLE from './PriceContainerMobile.style';

/**  PROPS & STATE INTERFACE */
interface IPriceContainerMobileProps extends React.Props<any> {
  price: any
};

interface IPriceContainerMobileState { };

@Radium
class PriceContainerMobile extends React.Component<IPriceContainerMobileProps, any> {
  constructor(props) {
    super(props);

    this.state = {}
  }

  render() {
    const { price } = this.props;

    return (
      <div
        style={[
          LAYOUT.flexContainer.justify,
          LAYOUT.flexContainer.verticalCenter,
          STYLE
        ]}>

        {/** 1. product price */}
        <ProductPrice price={price} />

        {/** 2. AddToCart */}
        <AddToCart />

        {/** 3. AddWishList*/}
        <AddWishList />
      </div>
    )
  }
};

export default PriceContainerMobile;
