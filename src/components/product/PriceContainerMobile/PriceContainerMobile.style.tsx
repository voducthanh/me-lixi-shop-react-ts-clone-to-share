/**
 * Created by Thành Võ on 03.04.2017
 */

import * as VARIABLE from '../../../styles/variable.style';
import MEDIA_QUERIES from '../../../styles/mediaQueries.style';

export default {
  width: '100%',
  height: 64,
  position: 'fixed',
  left: 0,
  bottom: 0,
  background: VARIABLE.colorWhite09,
  boxShadow: VARIABLE.shadowReverse,
  padding: '0 10px 0 15px',
  zIndex: VARIABLE.zIndex9,
};
