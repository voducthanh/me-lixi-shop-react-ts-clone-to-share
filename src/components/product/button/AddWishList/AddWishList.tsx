/**
 * Created by Thành Võ on 03.04.2017
 */

import * as React from 'react';
import * as Radium from 'radium';

import * as COMPONENT from '../../../../styles/component.style';

import STYLE from './AddWishList.style';

/**  PROPS & STATE INTERFACE */
interface IAddWishListProps extends React.Props<any> {
  routes: any
};

interface IAddWishListState {
  showNaviTop: boolean
};

@Radium
class AddWishList extends React.Component<IAddWishListProps, any> {
  constructor(props) {
    super(props);

    this.state = {}
  }

  render() {
    return (
      <div
        style={[
          COMPONENT.buttonBorder,
          STYLE
        ]}>
        <i
          style={[
            COMPONENT.buttonIcon,
            STYLE.icon
          ]}
          className="lx lx-42-heart-line"></i>
        <div style={STYLE.text}>
          Loves
        </div>
      </div>
    )
  }
};

export default AddWishList;
