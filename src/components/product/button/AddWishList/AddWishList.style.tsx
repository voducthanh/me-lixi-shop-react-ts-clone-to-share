/**
 * Created by Thành Võ on 03.04.2017
 */

import * as VARIABLE from '../../../../styles/variable.style';
import MEDIA_QUERIES from '../../../../styles/mediaQueries.style';

export default {
  height: 44,
  width: 44,
  padding: 0,

  icon: {
    fontSize: 18,
    marginLeft: 0,
    height: 42,
    width: 43,
    lineHeight: '42px'
  },

  text: {
    display: 'none'
  }
};
