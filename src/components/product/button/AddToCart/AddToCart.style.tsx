/**
 * Created by Thành Võ on 03.04.2017
 */

import * as VARIABLE from '../../../../styles/variable.style';
import MEDIA_QUERIES from '../../../../styles/mediaQueries.style';

export default {
  height: 44,
  width: 44,
  padding: 0,
  marginRight: 10,

  icon: {
    fontSize: 18,
    marginLeft: 0,
    height: 44,
    width: 44,
    lineHeight: '44px'
  },

  text: {
    display: 'none'
  }
};
