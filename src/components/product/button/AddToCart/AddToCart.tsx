/**
 * Created by Thành Võ on 03.04.2017
 */

import * as React from 'react';
import * as Radium from 'radium';

import STYLE from './AddToCart.style';
import * as COMPONENT from '../../../../styles/component.style';

/**  PROPS & STATE INTERFACE */
interface IAddToCartProps extends React.Props<any> { };

interface IAddToCartState { };

@Radium
class AddToCart extends React.Component<IAddToCartProps, any> {
  constructor(props) {
    super(props);

    this.state = {}
  }

  render() {
    return (
      <div
        style={[
          COMPONENT.button,
          COMPONENT.button.pink,
          STYLE
        ]}>
        <i
          style={[
            COMPONENT.buttonIcon,
            STYLE.icon
          ]}
          className="lx lx-41-shopping-cart"></i>
        <div style={STYLE.text}>
          Thêm vào giỏ
        </div>
      </div>
    )
  }
};

export default AddToCart;
