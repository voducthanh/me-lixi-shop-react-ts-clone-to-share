/**
 * Created by Thành Võ on 22.03.2017
 */

import * as React from 'react';
import * as Radium from 'radium';

import STYLE from './OverlayReload.style';
import { breakPoint960 } from '../../../styles/variable.style';

/**  PROPS & STATE INTERFACE */
interface IOverlayReloadProps extends React.Props<any> { };

interface IOverlayReloadState { };

@Radium
class OverlayReload extends React.Component<IOverlayReloadProps, any> {
  constructor(props) {
    super(props);
  }

  render() {
    return (
      <div style={STYLE}>
        <div style={
          [
            window.innerWidth < breakPoint960
              ? STYLE.mobile
              : STYLE.desktop,
            STYLE.panel
          ]
        }>
          <div
            key="overlay-reload-content"
            style={STYLE.panel.content}>
            <i
              style={STYLE.panel.content.icon}
              className="lx lx-24-reload"></i>
            <div
              style={STYLE.panel.content.text}>
              Please reload the page for the best experience
            </div>
          </div>
        </div>
      </div>
    );
  }
};

export default OverlayReload;
