/**
 * Created by Thành Võ on 30.03.2017
 */

import * as variableStyle from '../../../styles/variable.style';

export default {
  display: 'block',
  width: 0,
  height: 0,

  mobile: {
    display: 'none',

    '@media screen and (min-width: 960px)': {
      display: 'flex'
    }
  },
  desktop: {
    display: 'none',

    '@media screen and (max-width: 959px)': {
      display: 'flex'
    }
  },

  panel: {
    position: 'fixed',
    width: '100%',
    height: '100%',
    zIndex: variableStyle.zIndexMax,
    backgroundColor: variableStyle.colorF7,
    top: 0,
    left: 0,
    justifyContent: 'center',
    alignItems: 'center',

    content: {
      textAlign: 'center',
      opacity: 0.8,

      icon: {
        width: 50,
        height: 50,
        fontSize: 50,
        color: variableStyle.color2E,
        display: 'inline-block',
      },

      text: {
        fontSize: 20,
        color: variableStyle.color2E,
        lineHeight: '30px',
        padding: 20,
        textAlign: 'center',
      }
    }
  }
};
