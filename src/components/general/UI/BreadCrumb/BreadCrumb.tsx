/**
 * Created by Thành Võ on 22.03.2017
 */

import * as React from 'react';
import * as Radium from 'radium';

import STYLE from './BreadCrumb.style';

import * as LAYOUT from '../../../../styles/layout.style';

/**  PROPS & STATE INTERFACE */
interface IBreadCrumbProps extends React.Props<any> { };

interface IBreadCrumbState {
  list: Array<any>;
};

@Radium
class BreadCrumb extends React.Component<IBreadCrumbProps, any> {
  constructor(props) {
    super(props);

    this.state = {
      list: [
        {
          id: 1,
          title: 'Beauty (Mua lẻ)',
          href: '#',
          hover: false,
          list: [
            {
              id: 2,
              title: 'Beauty Box',
              href: '#',
            }
          ]
        },
        {
          id: 3,
          title: 'Skin care (Chăm sóc da)',
          href: '#',
          hover: false,
          list: [
            {
              id: 4,
              title: 'Make up (Trang điểm)',
              href: '#',
            },
            {
              id: 5,
              title: 'Bath & Body (Cơ thể)',
              href: '#',
            },
            {
              id: 6,
              title: 'Tools & Accesssories (Cọ và phụ kiện)',
              href: '#',
            },
            {
              id: 7,
              title: 'New arrivals (Hàng mới nhất)',
              href: '#',
            },
            {
              id: 8,
              title: 'Best sellers (Hàng hot nhất)',
              href: '#',
            }
          ]
        },
        {
          id: 9,
          title: 'Cleanse (Rửa mặt)',
          href: '#',
          hover: false,
          list: [
            {
              id: 10,
              title: 'Mask (Mặt nạ)',
              href: '#',
            },
            {
              id: 11,
              title: 'Moisturize (Duỡng ẩm)',
              href: '#',
            },
            {
              id: 12,
              title: 'Treat (Trị liệu)',
              href: '#',
            },
            {
              id: 13,
              title: 'Sun protection (Kem chống nắng)',
              href: '#',
            },
            {
              id: 14,
              title: 'Lip Treatment (Duỡng môi)',
              href: '#',
            }
          ]
        },
      ]
    };
  }

  /** Show sublist when hover */
  hoverItem(_item, _hover) {
    this.setState((prevState, props) => ({
      list: prevState.list.map((item) => {
        item.hover = item.id === _item.id && _hover;
        return item;
      }),
    }));
  }

  render() {
    const { list } = this.state;

    return (
      <div style={[LAYOUT.flexContainer.left, STYLE]}>
        {
          list.map((item, $index) =>
            <div
              key={`bread-${item.id}`}
              style={STYLE.item}
              onTouchStart={() => this.hoverItem(item, true)}
              onTouchEnd={() => this.hoverItem(item, false)}
              onMouseEnter={() => this.hoverItem(item, true)}
              onMouseLeave={() => this.hoverItem(item, false)}>
              <a
                key={`bread-title-${item.id}`}
                style={[LAYOUT.flexContainer.left, STYLE.title]}
                href={item.href}>
                {item.title}
                {
                  $index < this.state.list.length - 1
                  && <div style={STYLE.title.icon}>/</div>
                }
              </a>
              <div
                style={[
                  LAYOUT.flexContainer.wrap,
                  STYLE.sub,
                  item.hover && STYLE.sub.show
                ]}>
                {
                  item.list.map(sub =>
                    <a
                      style={[
                        STYLE.title,
                        STYLE.title.titleSub
                      ]}
                      key={`bread-sub-${sub.id}`}
                      href={sub.href}>
                      {sub.title}
                    </a>
                  )
                }
              </div>
            </div>
          )
        }
      </div>
    );
  }
};

export default BreadCrumb;
