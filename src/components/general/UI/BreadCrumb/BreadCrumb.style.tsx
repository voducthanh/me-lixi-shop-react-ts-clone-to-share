/**
 * Created by Thành Võ on 30.03.2017
 */

import * as VARIABLE from '../../../../styles/variable.style';

export default {
  width: '100%',
  padding: '10px 0 10px',
  position: 'relative',
  zIndex: VARIABLE.zIndex9,

  item: {
    display: 'block',
    position: 'relative',
  },

  title: {
    whiteSpace: 'nowrap',
    color: VARIABLE.color97,
    fontSize: 12,
    lineHeight: '30px',
    transition: VARIABLE.transitionColor,

    ':hover': {
      color: VARIABLE.colorPink,
    },

    icon: {
      fontSize: 12,
      lineHeight: '30px',
      width: 20,
      textAlign: 'center'
    },

    titleSub: {
      width: '100%',
      padding: '0 30px 0 20px',
      lineHeight: '34px',
      fontSize: 13,

      ':hover': {
        backgroundColor: VARIABLE.colorF7,
      }
    }
  },

  sub: {
    borderRadius: 3,
    position: 'absolute',
    visibility: 'hidden',
    boxShadow: VARIABLE.shadowBlur,
    top: 40,
    left: -20,
    padding: '12px 0',
    background: VARIABLE.colorWhite,
    transition: VARIABLE.transitionTop,
    opacity: 0,

    show: {
      top: 30,
      opacity: 1,
      visibility: 'visible',
    }
  }
};
