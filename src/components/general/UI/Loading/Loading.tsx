/**
 * Created by Thành Võ on 03.04.2017
 */

import * as React from 'react';
import * as Radium from 'radium';

import STYLE from './Loading.style';

import * as LAYOUT from '../../../../styles/layout.style';
import * as VARIABLE from '../../../../styles/variable.style';

/**  PROPS & STATE INTERFACE */
interface ILoadingProps extends React.Props<any> {
  customStyle: any;
};

interface ILoadingState { };

@Radium
class Loading extends React.Component<ILoadingProps, any> {
  static defaultProps: ILoadingProps;
  constructor(props) {
    super(props);

    const listImage = [];
    [0, 1, 2, 3, 4, 5, 6, 7].map(item => {
      listImage.push(require(`../../../../assets/images/general/loading/${item}.png`));
    });

    this.state = {
      list: listImage,
      active: 0,
      intervalAnimation: null
    };
  }


  componentWillMount() {
    this.loadingAnimation();
  }

  componentWillUnmount() {
    clearInterval(this.state.intervalAnimation);
  }

  loadingAnimation() {
    this.setState({
      intervalAnimation: setInterval(() => {
        this.setState((prevState, props) => ({
          active: prevState.active === 7 ? 0 : prevState.active + 1,
        }));
      }, 200)
    });
  }

  render() {
    const { list, active, styleBorder } = this.state;
    const { customStyle } = this.props;

    return (
      <div
        style={[
          LAYOUT.flexContainer.center,
          LAYOUT.flexContainer.verticalCenter,
          STYLE,
          customStyle
        ]}>
        <div
          style={[
            LAYOUT.flexContainer.center,
            LAYOUT.flexContainer.verticalCenter,
            STYLE.container
          ]}>
          <div style={STYLE.container.list}>
            {
              list.map((item, $index) =>
                <div
                  key={`item-loading-${$index}`}
                  style={[
                    {
                      backgroundImage: `url('${item}')`,
                    },
                    STYLE.container.list.item,
                    active === $index && STYLE.container.list.item.active
                  ]}></div>
              )
            }
          </div>
        </div>
      </div>
    );
  }
};

Loading.defaultProps = {
  customStyle: {}
};

export default Loading;
