/**
 * Created by Thành Võ on 30.03.2017
 */

import * as VARIABLE from '../../../../styles/variable.style';

export default {
  width: '100%',
  height: 200,

  container: {
    width: 50,
    height: 50,
    opacity: 1,
    border: `2px solid ${VARIABLE.colorE5}`,
    borderRadius: '50%',

    list: {
      display: 'block',
      width: 34,
      height: 34,
      position: 'relative',

      item: {
        position: 'absolute',
        top: 0,
        left: 0,
        width: 34,
        height: 34,
        opacity: 0,
        backgroundSize: 'cover',
        backgroundPosition: 'center',

        active: {
          opacity: 1
        }
      }
    }
  }
};
