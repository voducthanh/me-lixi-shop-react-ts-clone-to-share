/**
 * Created by Thành Võ on 30.03.2017
 */

import * as VARIABLE from '../../../../styles/variable.style';

const HEIGHT_DEFAULT = 40;

export default {
  display: 'block',
  width: '100%',
  position: 'relative',
  zIndex: VARIABLE.zIndex5,

  open: {
    zIndex: VARIABLE.zIndex9,
  },

  icon: {
    minWidth: 40,
    width: 40,
    height: 38,
    fontSize: 12,
    lineHeight: '38px',
    textAlign: 'center',
    color: VARIABLE.color75,
    cursor: 'pointer',
  },

  header: {
    height: HEIGHT_DEFAULT,
    backgroundColor: VARIABLE.colorWhite,
    border: `1px solid ${VARIABLE.colorD2}`,
    borderRadius: 3,
    padding: '0 0 0 15px',
    cursor: 'pointer',

    text: {
      lineHeight: `${HEIGHT_DEFAULT}px`,
      fontSize: 14,
      color: VARIABLE.color75,
      whiteSpace: 'nowrap',
      textOverflow: 'ellipsis',
      overflow: 'hidden',
    },

    icon: {
      marginLeft: 20,
    }
  },

  content: {
    position: 'absolute',
    width: '100%',
    top: 0,
    left: 0,
    backgroundColor: VARIABLE.colorWhite,
    borderRadius: 3,
    boxShadow: VARIABLE.shadow3,

    search: {
      height: HEIGHT_DEFAULT,
      backgroundColor: VARIABLE.colorWhite,
      borderBottom: `1px solid ${VARIABLE.colorD2}`,
      padding: '0 0 0 15px',

      input: {
        backgroundColor: 'transparent',
        flex: 10,
        height: HEIGHT_DEFAULT - 2,
        lineHeight: `${HEIGHT_DEFAULT - 2}px`,
        border: 'none',
        outline: 'none',
        fontSize: 14,
        whiteSpace: 'nowrap',
        textOverflow: 'ellipsis',
        overflow: 'hidden',
        margin: 0
      },

      close: {
        marginLeft: 20,
      }
    },

    list: {
      maxHeight: HEIGHT_DEFAULT * 6,
      overflow: 'auto',

      container: {},

      item: {
        cursor: 'pointer',
        height: HEIGHT_DEFAULT,
        lineHeight: `${HEIGHT_DEFAULT}px`,

        hover: {
          backgroundColor: VARIABLE.colorF7,
        },

        selected: {
          pointerEvents: 'none',
        },

        icon: {
          opacity: 0,
          color: VARIABLE.colorPink,

          selected: {
            opacity: 1,
          }
        },

        text: {
          flex: 10,
          fontSize: 14,
          lineHeight: `${HEIGHT_DEFAULT}px`,
          color: VARIABLE.color75,
          whiteSpace: 'nowrap',
          textOverflow: 'ellipsis',
          overflow: 'hidden',
          padding: '0 15px',

          selected: {
            color: VARIABLE.colorPink
          }
        }
      }
    }
  }
}
