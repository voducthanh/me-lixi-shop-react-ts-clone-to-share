/**
 * Created by Thành Võ on 03.04.2017
 */

import * as React from 'react';
import * as Radium from 'radium';

import STYLE from './SelectBox.style';

import * as LAYOUT from '../../../../styles/layout.style'

/**  PROPS & STATE INTERFACE */
interface ISelectBoxProps extends React.Props<any> {
  list: Array<any>;
  onChange: any;
  title: string;
  search: string;
  style: object;
};

SelectBox.defaultProps = {
  list: [],
  onChange: () => { },
  title: 'Chọn giá trị...',
  search: 'Tìm kiếm...',
  style: {}
};

interface ISelectBoxState {
  open: boolean,
  list: Array<any>,
  filteredList: Array<any>
};

@Radium
class SelectBox extends React.Component<ISelectBoxProps, any> {
  static defaultProps: ISelectBoxProps;

  constructor(props) {
    super(props);

    /** Assign from: props -> state and init set hover is false */
    const propsList = this.props.list.map(item => {
      item.hover = false;
      return item;
    });

    /**
     * @param {open}          : state open / close list select
     * @param {list}          : orginal list
     * @param {filteredList}  : list after filter by keyword
     */
    this.state = {
      open: false,
      list: propsList,
      filteredList: propsList
    }
  }

  /** Update propList / filteredList when parent change */
  componentWillReceiveProps(nextProps) {
    const propsList = nextProps.list.map(item => {
      item.hover = false;
      return item;
    })

    this.setState({
      list: propsList,
      filteredList: propsList
    });
  }

  /** Toggle to show / hide selectlist */
  toggleSelect(_state) {
    this.setState((prevState, props) => ({
      open: _state || !prevState.open
    }));
  }

  /**
   * Close select list
   * 1. Reset filteredList
   * 2. Close List select
   */

  closeSelectList() {
    this.setState({
      filteredList: this.state.list.map(item => {
        item.hover = false;
        return item;
      })
    });

    this.toggleSelect(false);
  }

  /** Set hover state for background hightlight */
  hoverValue(_item) {
    this.setState((prevState, props) => ({
      filteredList: prevState.filteredList.map(item => {
        item.hover = item.id === _item.id;
        return item;
      }),
    }));
  }

  /**
   *
   * @param {*} _item : item in selected in list
   * 1. Update state for filteredList
   * 2. close select list
   */
  selectValue(_item) {
    this.setState((prevState, props) => ({
      filteredList: prevState.list.map(item => {
        item.selected =
          item.id === _item.id
            ? (this.props.onChange(_item), true)
            : false
        return item;
      }),
      open: false
    }));
  }

  /**
   * Search filter in list
   * @param {*} event : evetn from search input
   * get value from seacrh input to fitler list select
   */
  searchFilter(event) {
    const valueSearch = event.target.value.toLowerCase();
    this.setState((prevState, props) => ({
      filteredList: prevState.list.filter(
        item => item.title.toLowerCase().indexOf(valueSearch) >= 0
      )
    }));
  }

  render() {
    const { open, filteredList, list } = this.state;
    const { title, style, search } = this.props;
    return (
      <div
        style={[
          STYLE,
          open && STYLE.open,
          style
        ]}>

        {/** 1. Header */}
        <div
          onClick={() => this.toggleSelect(true)}
          style={[
            LAYOUT.flexContainer.justify,
            LAYOUT.flexContainer.verticalCenter,
            STYLE.header
          ]}>

          {/** 1.1. Header: text */}
          <div style={STYLE.header.text}>
            {
              filteredList.filter(item => true === item.selected).length === 0
                ? title
                : list.filter(item => item.selected)[0].title
            }
          </div>

          {/** 1.2. Header: icon */}
          <i
            style={[
              STYLE.icon,
              STYLE.header.icon
            ]}
            className={
              true === open
                ? 'lx lx-13-angle-up'
                : 'lx lx-12-angle-down'
            }></i>
        </div>

        {/** 2. Content */}
        {
          true === open &&

          /** item content */
          < div style={STYLE.content}>

            {/*2.1. Search */}
            <div
              style={[
                LAYOUT.flexContainer.justify,
                LAYOUT.flexContainer.verticalCenter,
                STYLE.content.search
              ]}>

              {/** 2.1.1. input search */}
              <input
                ref={input => input && input.focus()}
                onChange={this.searchFilter.bind(this)}
                placeholder={search}
                style={STYLE.content.search.input}
                type="text" />

              {/** 2.1.2. icon close */}
              <i
                onClick={() => this.closeSelectList()}
                style={[
                  STYLE.icon,
                  STYLE.content.search.close
                ]}
                className={'lx lx-31-close'}></i>
            </div>

            {/** 2.2. List value */}
            <div style={STYLE.content.list}>
              <div style={STYLE.content.list.container}>
                {
                  filteredList.map(item =>
                    <div
                      onMouseEnter={() => this.hoverValue(item)}
                      onClick={() => this.selectValue(item)}
                      key={`select-box-${item.id}`}
                      style={[
                        LAYOUT.flexContainer.left,
                        STYLE.content.list.item,
                        item.hover && STYLE.content.list.item.hover,
                        true === item.selected && STYLE.content.list.item.selected,
                      ]}>

                      {/** 2.2.1. text value */}
                      <div
                        style={[
                          STYLE.content.list.item.text,
                          true === item.selected && STYLE.content.list.item.text.selected,
                        ]}>
                        {item.title}
                      </div>

                      {/** 2.2.2. icon value */}
                      <i
                        style={[
                          STYLE.icon,
                          STYLE.content.list.item.icon,
                          true === item.selected && STYLE.content.list.item.icon.selected,
                        ]}
                        className={'lx lx-53-check'}></i>
                    </div>
                  )
                }
              </div>
            </div>
          </div>
        }
      </div >
    );
  }
};

export default SelectBox;
