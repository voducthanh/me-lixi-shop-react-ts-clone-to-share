/**
 * Created by Thành Võ on 03.04.2017
 */

import * as React from 'react';
import * as Radium from 'radium';

import STYLE from './RatingStar.style';

import * as LAYOUT from '../../../../styles/layout.style'

/**  PROPS & STATE INTERFACE */
interface IRatingStarProps extends React.Props<any> {
  value: number
};

RatingStar.defaultProps = {
  value: 0
}

interface IRatingStarState {
  showNaviTop: boolean
};

@Radium
class RatingStar extends React.Component<IRatingStarProps, any> {
  static defaultProps: IRatingStarProps;

  constructor(props) {
    super(props);
  }

  createClassName(_item) {
    switch (this.props.value) {
      case _item:
        return 'lx lx-37-start';

      case (_item - 0.5):
        return 'lx lx-39-start-half';

      default:
        return this.props.value > _item
          ? 'lx lx-37-start'
          : 'lx lx-38-start-line';
    }
  }

  render() {
    return (
      <div
        style={[
          LAYOUT.flexContainer.left,
          STYLE
        ]}>
        {
          [1, 2, 3, 4, 5].map((item) =>
            <i
              key={`rating-star-${item}`}
              className={this.createClassName(item)}
              style={STYLE.item}></i>
          )
        }
      </div>
    );
  }
};

export default RatingStar;
