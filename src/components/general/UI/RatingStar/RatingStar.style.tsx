/**
 * Created by Thành Võ on 30.03.2017
 */

import * as VARIABLE from '../../../../styles/variable.style';

export default {
  item: {
    wwidth: 16,
    height: 16,
    fontSize: 16,
    lineHeight: '16px',
    textAlign: 'center',
    color: VARIABLE.colorPink,
    margin: '0 2px'
  }
}
