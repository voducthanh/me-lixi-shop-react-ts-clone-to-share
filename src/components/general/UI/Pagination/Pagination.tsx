/**
 * Created by Thành Võ on 22.03.2017
 */

import * as React from 'react';
import * as Radium from 'radium';

import STYLE from './Pagination.style';

import * as LAYOUT from '../../../../styles/layout.style';

/**  PROPS & STATE INTERFACE */
interface IPaginationProps extends React.Props<any> { };

interface IPaginationState {
  list: Array<any>;
};

@Radium
class Pagination extends React.Component<IPaginationProps, any> {
  constructor(props) {
    super(props);

    this.state = {
      list: [
        {
          id: 1,
          value: 1,
          active: false
        },
        {
          id: 2,
          value: 2,
          active: false
        },
        {
          id: 3,
          value: 3,
          active: true
        },
        {
          id: 4,
          value: 4,
          active: false
        },
        {
          id: 5,
          value: 5,
          active: false
        }
      ]
    };
  }

  render() {
    const { list } = this.state;

    return (
      <div
        style={[
          LAYOUT.flexContainer.center,
          STYLE
        ]}>
        <div
          key={'pagi-prev'}
          style={STYLE.item}
          className="lx lx-15-angle-left"></div>
        {
          list.map(item =>
            <div
              key={`pagi-item-${item.id}`}
              style={[
                STYLE.item,
                item.active && STYLE.item.active
              ]}>
              {item.value}
            </div>
          )
        }
        <div
          key={'pagi-next'}
          style={[
            STYLE.item,
            STYLE.item.last
          ]}
          className="lx lx-14-angle-right"></div>
      </div>
    );
  }
};

export default Pagination;
