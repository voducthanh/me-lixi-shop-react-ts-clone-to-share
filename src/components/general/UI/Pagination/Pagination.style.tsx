/**
 * Created by Thành Võ on 30.03.2017
 */

import * as VARIABLE from '../../../../styles/variable.style';

export default {
  padding: '10px 20px 40px',

  item: {
    height: 36,
    minWidth: 36,
    padding: '0 10px',
    lineHeight: '36px',
    textAlign: 'center',
    borderTop: `1px solid ${VARIABLE.colorE5}`,
    borderBottom: `1px solid ${VARIABLE.colorE5}`,
    borderLeft: `1px solid ${VARIABLE.colorE5}`,
    fontSize: 13,
    color: VARIABLE.color97,
    cursor: 'pointer',

    last: {
      borderRight: `1px solid ${VARIABLE.colorE5}`,
    },

    active: {
      backgroundColor: VARIABLE.colorF7,
      pointerEvents: 'none'
    },

    ':hover': {
      backgroundColor: VARIABLE.colorF7
    }
  }
};
