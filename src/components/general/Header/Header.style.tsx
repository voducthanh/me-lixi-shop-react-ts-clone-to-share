/**
 * Created by Thành Võ on 23.03.2017
 */

import { zIndex9 } from '../../../styles/variable.style';

export default {
  display: 'block',
  position: 'relative',
  zIndex: zIndex9,

  offsetFixHeader: {
    height: 110,
    width: '100%',
  }
};
