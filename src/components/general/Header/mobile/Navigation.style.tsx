
/**
 * Created by Thành Võ on 13.03.2017
 */

import * as VARIABLE from '../../../../styles/variable.style';
import * as COMPONENT from '../../../../styles/component.style';
import * as LAYOUT from '../../../../styles/layout.style';

export default {
  display: 'block',
  height: 50,

  fixTop: {
    position: 'fixed',
    width: '100%',
    height: 50,
    backgroundColor: VARIABLE.colorBlack,
    boxShadow: VARIABLE.shadowBlur,
  },

  /** Menu icon in mobile */
  menuIcon: {
    display: 'block',
    width: '50px',
    height: '50px',
    lineHeight: '50px',
    fontSize: '16px',
    color: VARIABLE.colorWhite09,
    fontStyle: 'normal',
    verticalAlign: 'top',
    cursor: 'pointer',

    hide: {
      display: 'none'
    }
  },

  logo: {
    icon: {
      fontSize: 24,
      lineHeight: '24px',
      width: 24,
      height: 24,
      marginRight: 15,
      textShadow: `0 0 0 ${VARIABLE.colorWhite}`
    },

    img: {
      width: 'atuo',
      height: 16
    },

    hide: {
      display: 'none'
    }
  },

  inputSearch: {
    outline: 'none',
    border: 'none',
    height: 34,
    borderRadius: 15,
    margin: 0,
    flex: 10,
    background: VARIABLE.colorWhite,
    padding: '0 15px',
    textTransform: 'captialize',
    fontSize: 13,

    hide: {
      display: 'none'
    }
  },

  /** Left menu in mobile */
  menuMobile: {
    display: 'block',
    transition: VARIABLE.transitionNormal,
    position: 'fixed',
    width: '100%',
    height: '100%',
    top: 0,
    opacity: 1,
    overflowX: 'hidden',
    overflowY: 'auto',

    left: {
      left: '-100%',

      active: {
        left: 0,
      },
    },

    right: {
      right: '-100%',

      active: {
        right: 0,
      },
    },
  },
};
