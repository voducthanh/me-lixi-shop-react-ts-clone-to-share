
/**
 * Created by Thành Võ on 23.03.2017
 */

import * as VARIABLE from '../../../../styles/variable.style';
import * as COMPONENT from '../../../../styles/component.style';
import * as LAYOUT from '../../../../styles/layout.style';

const BG_IMAGE = require('../../../../assets/images/header/bg-mobile.jpg');

export default {
  background: VARIABLE.colorWhite,
  minHeight: '100%',

  closePanel: {
    position: 'absolute',
    top: 0,
    right: 0,
    cursor: 'pointer',
    color: VARIABLE.colorWhite07,
    width: 44,
    height: 44,
    lineHeight: '44px',
    fontSize: 14,
    zIndex: VARIABLE.zIndex9,
  },

  loginPanel: {
    width: '100%',
    height: 150,
    backgroundImage: `url('${BG_IMAGE}')`,
    backgroundPosition: 'center',
    backgroundSize: 'cover',
    boxShadow: VARIABLE.shadowInsetBottom,
    position: 'relative',

    button: {
      backgroundColor: VARIABLE.colorWhite09,
      border: `1px solid ${VARIABLE.colorWhite}`,
      color: VARIABLE.color4D,
      width: 'auto',
      display: 'inline-block',
      height: 40,
      lineHeight: '40px',
      padding: '0 30px',
      fontSize: 15,
      borderRadius: '22px',
      boxShadow: VARIABLE.shadowBlur,
      cursor: 'pointer',

      ':hover': {
        color: VARIABLE.colorPink,
        backgroundColor: VARIABLE.colorWhite,
      }
    },
  },

  profilePanel: {
    position: 'relative',
    overflow: 'hidden',

    backgroundBlur: {
      zIndex: VARIABLE.zIndex1,
      position: 'absolute',
      top: 0,
      left: 0,
      height: '100%',
      backgroundPosition: 'center',
      backgroundSize: 'cover',
      width: '100%',
      filter: 'blur(10px)',
      transform: 'scale(1.2)',
    },

    container: {
      position: 'relative',
      zIndex: VARIABLE.zIndex5,
      backgroundColor: VARIABLE.colorBlack04,
      paddingTop: 40,
      boxShadow: VARIABLE.shadowInsetBottom,
    },

    avatar: {
      width: 120,
      height: 120,
      backgroundPosition: 'center',
      backgroundSize: 'cover',
      borderRadius: '50%',
      marginBottom: 20,
      boxShadow: VARIABLE.shadowBlur,
    },

    userName: {
      fontSize: 18,
      lineHeight: '24px',
      color: VARIABLE.colorWhite08,
      whiteSpace: 'nowrap',
      overflow: 'hidden',
      textOverflow: 'ellipsis',
      marginBottom: 20,
      padding: '0 30px',
      maxWidth: '100%',
      fontFamily: VARIABLE.fontAvenirRegular,
    },

    listNavigation: {
      transition: VARIABLE.transitionNormal,
      overflow: 'hidden',

      item: {
        fontSize: 13,
        lineHeight: '24px',
        textAlign: 'center',
        color: VARIABLE.colorWhite07,
      },
    },

    toggleNavigation: {
      width: 40,
      hieght: 40,
      color: VARIABLE.colorWhite05,
      fontSize: 16,
      lineHeight: '40px',
      textAlign: 'center',
      cursor: 'pointer'
    }
  },

  menu: {
    padding: '20px 0 50px',

    item: {
      height: 50,
      cursor: 'pointer',

      icon: {
        width: 60,
        height: 50,
        lineHeight: '50px',
        textAlign: 'center',
        fontSize: 20,
        marginLeft: 20,
        color: VARIABLE.color75
      },

      title: {
        lineHeight: '50px',
        fontSize: 15,
        color: VARIABLE.color4D,
        fontFamily: VARIABLE.fontAvenirLight,
        flex: 10,
      },

      sub: {
        flex: 10,

        iconSub: {
          fontSize: 9,
          width: 60,
          height: 60,
          lineHeight: '60px',
          color: VARIABLE.color97,
        },
      },

      subContainer: {
        padding: '0 20px 0 80px',
        boxShadow: VARIABLE.shadowInsetMiddle,
        backgroundColor: VARIABLE.colorE5,
        overflow: 'hidden',
        transition: VARIABLE.transitionHeight,

        item: {
          height: 32,
          lineHeight: '32px',
          overflow: 'hidden',
          textOverflow: 'ellipsis',
          whiteSpace: 'nowrap',
          fontSize: 13,
          fontFamily: VARIABLE.fontAvenirLight,
          color: VARIABLE.color75
        }
      }
    },
  },

  footerInfo: {
    logo: {
      height: 50,

      icon: {
        width: 24,
        height: 24,
        fontSize: 24,
        lineHeight: '24px',
        textAlign: 'center',
        marginRight: 15,
      },

      image: {
        height: 16,
        width: 'auto'
      }
    },

    link: {
      background: VARIABLE.colorBlack,

      item: {
        color: VARIABLE.colorWhite07,
        fontSize: 12,
        lineHeight: '30px',
        padding: '0 15px',
        fontFamily: VARIABLE.fontAvenirRegular
      }
    }
  }
};
