
/**
 * Created by Thành Võ on 13.03.2017
 */

import * as React from 'react';
import * as Radium from 'radium';

import LeftNavigationMobile from './Left';

import * as LAYOUT from '../../../../styles/layout.style';
import * as VARIABLE from '../../../../styles/variable.style';
import STYLE from './Navigation.style';

const LOGO_TEXT_WHITE = require('../../../../assets/images/header/logo-text-white.png');

/**  PROPS & STATE INTERFACE */
interface INavigationMobileProps extends React.Props<any> { };

interface INavigationMobileState {
  openLeftMenu: boolean;
  openSearch: boolean;
};

@Radium
class NavigationMobile extends React.Component<INavigationMobileProps, any> {
  constructor(props) {
    super(props);

    this.state = {
      openLeftMenu: false,
      openSearch: false,
    };
  }

  /** Open / Close left menu : PROFILE & NAVIGATION */
  toggleLeftMenu(_state = true) {
    this.setState({ openLeftMenu: _state });
  }

  /** Open / Close right menu : SHOPPING CART */
  toggleSearch(_state = true) {
    this.setState({ openSearch: _state });
  }

  /** RENDER */
  render() {
    return (
      <div style={STYLE}>
        {/** 1. Fix top bar */}
        <div
          style={[
            LAYOUT.flexContainer.justify,
            LAYOUT.flexContainer.verticalCenter,
            STYLE.fixTop,
          ]}>
          {/** 1.1. MENU ICON*/}
          <a
            className="lx lx-16-menu"
            style={[
              STYLE.menuIcon,
              this.state.openSearch && STYLE.menuIcon.hide,
            ]}
            onClick={() => this.toggleLeftMenu(true)}></a>

          {/** 1.2. Close search icon */}
          <a
            className="lx lx-31-close"
            style={[
              STYLE.menuIcon,
              !this.state.openSearch && STYLE.menuIcon.hide,
            ]}
            onClick={() => this.toggleSearch(false)}></a>

          {/** 1.3. LOGO */}
          <a
            style={[
              LAYOUT.flexContainer.justify,
              LAYOUT.flexContainer.verticalCenter,
              STYLE.logo,
              this.state.openSearch && STYLE.logo.hide,
            ]}>
            <i
              style={[
                STYLE.menuIcon,
                STYLE.logo.icon
              ]}
              className="lx lx-02-logo-line"></i>
            <img
              style={STYLE.logo.img}
              src={LOGO_TEXT_WHITE}
              alt="#Logo" />
          </a>

          {/** 1.4. Input search */}
          <input
            ref={input => input && input.focus()}
            placeholder="Nhập từ khoá tìm kiếm..."
            style={[
              STYLE.inputSearch,
              !this.state.openSearch && STYLE.inputSearch.hide,
            ]}></input>

          {/** 1.5. Toggle Search */}
          <a
            className="lx lx-04-search"
            style={STYLE.menuIcon}
            onClick={() => this.toggleSearch(true)}></a>
        </div>

        {/** 2. Menu Left Mobile */}
        <div
          style={[
            STYLE.menuMobile,
            STYLE.menuMobile.left,
            this.state.openLeftMenu && STYLE.menuMobile.left.active
          ]}>
          <LeftNavigationMobile closeLeftPanel={() => this.toggleLeftMenu(false)} />
        </div>
      </div>
    );
  }
};

export default NavigationMobile;
