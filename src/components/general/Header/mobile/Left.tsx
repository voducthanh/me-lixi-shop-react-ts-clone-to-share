
/**
 * Created by Thành Võ on 13.03.2017
 */

import * as React from 'react';
import * as Radium from 'radium';

import * as LAYOUT from '../../../../styles/layout.style';
import * as VARIABLE from '../../../../styles/variable.style';
import STYLE from './Left.style';

const LOGO_TEXT = require('../../../../assets/images/header/logo-text.png');
const LOGO_TEXT_BLACK = require('../../../../assets/images/header/logo-text-black.png');
const AVATAR = require('../../../../assets/images/sample-data/avatar-2.jpg');

/**  PROPS & STATE INTERFACE */
interface ILeftNavigationMobileProps extends React.Props<any> {
  closeLeftPanel: any;
};

interface ILeftNavigationMobileState {
  menu: Array<any>;
  login: boolean;
  openMenuProfile: boolean;
};

@Radium
class LeftNavigationMobile extends React.Component<ILeftNavigationMobileProps, any> {
  constructor(props) {
    super(props);

    this.state = {
      menu: [
        {
          id: 1,
          title: 'Trang chủ',
          link: '#',
          class: 'lx lx-25-home',
          sub: [],
          open: false,
        },
        {
          id: 2,
          title: 'Beauty Box',
          link: '#',
          class: 'lx lx-26-box',
          sub: [
            {
              id: 9,
              title: 'Box mới nhất',
              link: '#'
            },
            {
              id: 10,
              title: 'Box bán chạy nhất',
              link: '#'
            },
            {
              id: 11,
              title: 'Box trang điểm',
              link: '#'
            },
            {
              id: 12,
              title: 'Box duỡng da',
              link: '#'
            },
            {
              id: 13,
              title: 'Box trị mụn',
              link: '#'
            }
          ],
          open: false,
        },
        {
          id: 3,
          title: 'Trang điểm',
          link: '#',
          class: 'lx lx-26-box',
          sub: [
            {
              id: 14,
              title: 'Mắt',
              link: '#'
            },
            {
              id: 15,
              title: 'Mặt',
              link: '#'
            },
            {
              id: 16,
              title: 'Môi',
              link: '#'
            },
            {
              id: 17,
              title: 'Set trang điểm',
              link: '#'
            },
            {
              id: 18,
              title: 'Trang điểm mới nhất',
              link: '#'
            },
            {
              id: 19,
              title: 'Trang điểm hot nhất',
              link: '#'
            },
          ],
          open: false,
        },
        {
          id: 4,
          title: 'Chăm sóc da',
          link: '#',
          class: 'lx lx-26-box',
          sub: [
            {
              id: 20,
              title: 'Rửa mặt',
              link: '#'
            },
            {
              id: 21,
              title: 'Mặt nạ',
              link: '#'
            },
            {
              id: 22,
              title: 'Duỡng ẩm',
              link: '#'
            },
            {
              id: 23,
              title: 'Trị liệu',
              link: '#'
            },
            {
              id: 24,
              title: 'Duỡng môi',
              link: '#'
            },
            {
              id: 25,
              title: 'Kem chống nắng',
              link: '#'
            },
            {
              id: 26,
              title: 'Set duỡng da',
              link: '#'
            },
            {
              id: 27,
              title: 'Duỡng da mới nhất',
              link: '#'
            },
            {
              id: 28,
              title: 'Duỡng da hot nhất',
              link: '#'
            },
          ],
          open: false,
        },
        {
          id: 5,
          title: 'Cơ thể',
          link: '#',
          class: 'lx lx-26-box',
          sub: [
            {
              id: 29,
              title: 'Nuớc hoa',
              link: '#'
            },
            {
              id: 30,
              title: 'Duỡng thể',
              link: '#'
            },
            {
              id: 31,
              title: 'Sữa tắm',
              link: '#'
            },
            {
              id: 32,
              title: 'Chống nắng',
              link: '#'
            },
            {
              id: 33,
              title: 'Hair Remover',
              link: '#'
            },
            {
              id: 34,
              title: 'Tóc',
              link: '#'
            },
          ],
          open: false,
        },
        {
          id: 6,
          title: 'Cọ & Phụ kiện',
          link: '#',
          class: 'lx lx-26-box',
          sub: [
            {
              id: 35,
              title: 'Cọ',
              link: '#'
            },
            {
              id: 36,
              title: 'Khác',
              link: '#'
            },
          ],
          open: false,
        },
        {
          id: 7,
          title: 'Đặc biệt',
          link: '#',
          class: 'lx lx-27-special',
          sub: [
            {
              id: 37,
              title: 'Deal of the day',
              link: '#'
            },
            {
              id: 38,
              title: 'Sản phẩm minisize',
              link: '#'
            },
            {
              id: 39,
              title: 'BOX DESIGNED FOR MEN',
              link: '#'
            },
          ],
          open: false,
        },
        {
          id: 8,
          title: 'Theo Thuơng hiệu',
          link: '#',
          class: 'lx lx-28-tag',
          sub: [],
          open: false,
        }
      ],
      login: true,
      openMenuProfile: false
    };
  }

  toggleMenuItem = (_menuItem) => {
    this.setState((prevState, props) => ({
      menu: prevState.menu.map(menuItem => {
        menuItem.open = menuItem.id !== _menuItem.id
          ? false
          : !_menuItem.open;
        return menuItem;
      })
    }));
  }

  toggleMenuProfile = () => {
    this.setState((prevState, props) => ({
      openMenuProfile: !prevState.openMenuProfile
    }));
  }

  /** RENDER */
  render() {
    return (
      <div
        style={[
          LAYOUT.flexContainer.justify,
          LAYOUT.flexContainer.verticalFlex,
          STYLE
        ]}>
        {
          true === this.state.login
            ? /** Profile user */
            <div style={STYLE.profilePanel}>
              <i
                onClick={this.props.closeLeftPanel}
                style={STYLE.closePanel}
                className="lx lx-31-close"></i>
              <div
                style={[
                  { backgroundImage: `url('${AVATAR}')` },
                  STYLE.profilePanel.backgroundBlur
                ]}></div>
              <div
                style={[
                  LAYOUT.flexContainer.verticalFlex,
                  LAYOUT.flexContainer.verticalCenter,
                  STYLE.profilePanel.container
                ]}>
                <div
                  style={[
                    { backgroundImage: `url('${AVATAR}')` },
                    STYLE.profilePanel.avatar
                  ]}></div>
                <div
                  style={STYLE.profilePanel.userName}>
                  Huỳnh Yến Nhi
                </div>
                <div
                  style={[
                    STYLE.profilePanel.listNavigation,
                    {
                      height: this.state.openMenuProfile ? 4 * 24 : 0,
                      opacity: this.state.openMenuProfile ? 1 : 0
                    }
                  ]}>
                  <div style={STYLE.profilePanel.listNavigation.item}>Quản lý Tài khoản</div>
                  <div style={STYLE.profilePanel.listNavigation.item}>Trang Quản trị</div>
                  <div style={STYLE.profilePanel.listNavigation.item}>Danh sách Yêu thích</div>
                  <div style={STYLE.profilePanel.listNavigation.item}>Đăng xuất</div>
                </div>
                <i
                  style={STYLE.profilePanel.toggleNavigation}
                  onClick={this.toggleMenuProfile}
                  className={false === this.state.openMenuProfile
                    ? 'lx lx-12-angle-down'
                    : 'lx lx-13-angle-up'}>
                </i>
              </div>
            </div>
            : /** Login button panel */
            <div
              style={[
                LAYOUT.flexContainer.center,
                LAYOUT.flexContainer.verticalCenter,
                STYLE.loginPanel
              ]}>
              <i
                onClick={this.props.closeLeftPanel}
                style={STYLE.closePanel}
                className="lx lx-31-close"></i>
              <a
                key="buttonLogin"
                style={STYLE.loginPanel.button}>
                Sign In / Sign Up
              </a>
            </div>
        }
        {/** List menu */}
        <div style={STYLE.menu}>
          {
            this.state.menu.map(menuItem => {
              return (
                <div key={menuItem.id}>
                  <div
                    style={[
                      LAYOUT.flexContainer.left,
                      STYLE.menu.item
                    ]}>
                    {/** Menu Item : Icon */}
                    <i
                      style={STYLE.menu.item.icon}
                      className={menuItem.class}></i>
                    <div
                      style={[
                        LAYOUT.flexContainer.justify,
                        LAYOUT.flexContainer.verticalCenter,
                        STYLE.menu.item.sub
                      ]}>

                      {/** Menu Item : Title */}
                      <div style={STYLE.menu.item.title}>
                        {menuItem.title}
                      </div>
                      {
                        menuItem.sub
                        && menuItem.sub.length > 0
                        && <i
                          onClick={() => this.toggleMenuItem(menuItem)}
                          key={'sub-icon-' + menuItem.id}
                          style={STYLE.menu.item.sub.iconSub}
                          className={menuItem.open
                            ? 'lx lx-30-minus'
                            : 'lx lx-29-plus'}></i>
                      }
                    </div>
                  </div>

                  {/** Menu Item : Sub container */}
                  {
                    menuItem.sub
                    && menuItem.sub.length > 0
                    && <div
                      style={[
                        STYLE.menu.item.subContainer,
                        {
                          height: menuItem.open
                            ? menuItem.sub.length * 32
                            : 0
                        }
                      ]}
                      key={'sub-container-' + menuItem.id}>

                      {/** Sub menu item */}
                      {
                        menuItem.sub.map(subItem => {
                          return (
                            <div
                              style={STYLE.menu.item.subContainer.item}
                              key={subItem.id}>
                              {subItem.title}
                            </div>
                          );
                        })
                      }
                    </div>
                  }
                </div>
              );
            })
          }
        </div>

        {/** Footer */}
        <div style={STYLE.footerInfo}>
          {/** Footer logo */}
          <div
            style={[
              LAYOUT.flexContainer.center,
              LAYOUT.flexContainer.verticalCenter,
              STYLE.footerInfo.logo
            ]}>
            <i
              style={STYLE.footerInfo.logo.icon}
              className="lx lx-02-logo-line"></i>
            <img
              style={STYLE.footerInfo.logo.image}
              src={LOGO_TEXT_BLACK}
              alt="#Logo" />
          </div>

          {/** List linked footer */}
          <div
            style={[
              LAYOUT.flexContainer.center,
              STYLE.footerInfo.link
            ]}>
            <a
              style={STYLE.footerInfo.link.item}
              href="#">
              Magazine
            </a>
            <a
              style={STYLE.footerInfo.link.item}
              href="#">
              Đổi Lixicoin
            </a>
          </div>
        </div>
      </div >
    );
  }
};

export default LeftNavigationMobile;
