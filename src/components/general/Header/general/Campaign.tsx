/**
 *
 * Created by Thành Võ on 09.03.2017
 *
 */

import React from 'react';
import * as variableStyle from '../../../styles/variable.style';
import * as componentStyle from '../../../styles/component.style';

const CAMPAIGN_BACKGROUND = require('../../../assets/images/sample-data/banner-campaign.png');
const COMPONENT_STYLE = {
  display: 'block',
  padding: '12px',
  backgroundSize: 'cover',
  backgroundPosition: 'center',
  textAlign: 'center',

  text: {
    display: 'inline-block',
    fontFamily: variableStyle.fontGoThamMedium,
    fontSize: '14px',
    lineHeight: '20px',
    color: variableStyle.colorBlack,
    textAlign: 'center',
    marginBottom: '10px'
  },

  button: {
    margin: '0 20px'
  }
};

class CamPaignBanner extends React.Component {
  constructor(props) {
    super(props);
  }

  render() {
    return (
      <campaign-banner style={ Object.assign({}, COMPONENT_STYLE, {backgroundImage: `url(${CAMPAIGN_BACKGROUND})`}) }>
        <div style={ COMPONENT_STYLE.text }>GIỚI THIỆU BẠN BÈ, NHẬN NGAY 50,000đ, 200 LIXICOIN VÀ FREE SON ELF</div>
        <div style={ Object.assign({}, componentStyle.button, componentStyle.button.pink, COMPONENT_STYLE.button) }>Tìm hiểu ngay</div>
      </campaign-banner>
    )
  }
};

export default CamPaignBanner;
