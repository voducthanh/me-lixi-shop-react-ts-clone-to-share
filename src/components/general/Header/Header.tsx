/**
 * Created by Thành Võ on 09.03.2017
 */

import * as React from 'react';
import * as Radium from 'radium';

import BannerHomeSplash from '../../banner/HomeSplash';
import NavigationMobile from './mobile/Navigation';
import TopLink from './desktop/TopLink';
import NavigationDesktop from './desktop/Navigation';

import { breakPoint960 } from '../../../styles/variable.style';
import * as LAYOUT from '../../../styles/layout.style';

import STYLE from './Header.style';

/**  PROPS & STATE INTERFACE */
interface IHeaderProps extends React.Props<any> { };

interface IHeaderState {
  fixHeader: boolean;
};

@Radium
class Header extends React.Component<IHeaderProps, IHeaderState>  {
  constructor(props) {
    super(props);

    this.state = {
      fixHeader: false
    };
  }

  componentDidMount() {
    if (window.innerWidth >= breakPoint960) {
      this.handleScroll();
      window.addEventListener('scroll', () => this.handleScroll());
    }
  }

  componentWillUnmount() {
    if (window.innerWidth >= breakPoint960) {
      window.removeEventListener('scroll');
    }
  }

  handleScroll() {
    let scrollTop = document.body.scrollTop;
    if (scrollTop >= 92) {
      if (false === this.state.fixHeader) {
        this.setState({
          fixHeader: true
        });
      }
    } else {
      if (true === this.state.fixHeader) {
        this.setState({
          fixHeader: false
        });
      }
    }
  }

  render() {
    const { fixHeader } = this.state;
    return (
      <div style={STYLE} >
        {
          window.innerWidth >= breakPoint960 && <TopLink />
        }
        {
          window.innerWidth < breakPoint960
            ? <NavigationMobile />
            : <NavigationDesktop fixHeader={fixHeader} />
        }

        {
          this.state.fixHeader &&
          <div style={STYLE.offsetFixHeader}></div>
        }

        {/** Splash banner */}
        {
          window.innerWidth >= breakPoint960 &&
          <div
            key="header-wrap-splash"
            style={LAYOUT.wrapLayout} >
            <BannerHomeSplash />
          </div>
        }
      </div>
    );
  }
};

export default Header;
