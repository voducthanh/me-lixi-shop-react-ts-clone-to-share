/**
 * Created by Thành Võ on 23.03.2017
 */

import { wrapLayout } from '../../../../styles/layout.style';
import * as VARIABLE from '../../../../styles/variable.style';

export default {
  display: 'block',
  backgroundColor: VARIABLE.colorF0,

  wrap: {
    height: '26px',
  },

  item: {
    height: 12,
    lineHeight: '12px',
    fontSize: 10,
    fontFamily: VARIABLE.fontAvenirDemiBold,
    color: VARIABLE.color75,
    textTransform: 'uppercase',
    cursor: 'pointer',

    ':hover': {
      color: VARIABLE.colorPink
    }
  },

  itemLine: {
    paddingRight: 10,
    marginRight: 10,
    borderRight: `1px solid ${VARIABLE.colorB0}`,
  }
};
