import { zIndex1 } from '../../../../styles/variable.style';
/**
 * Created by Thành Võ on 24.03.2017
 */

import * as VARIABLE from '../../../../styles/variable.style';
import MEDIA_QUERIES from '../../../../styles/mediaQueries.style';

export default {
  zIndex: VARIABLE.zIndex9,
  position: 'relative',
  display: 'block',
  width: '100%',

  wrapContent: {
    boxShadow: VARIABLE.shadowBlur,
    background: VARIABLE.colorWhite095,
    position: 'relative',
    zIndex: VARIABLE.zIndex9,
  },

  fixHeader: {
    position: 'fixed',
    width: '100%',
    top: -66,
    left: 0,
  },

  /** Top nav for desktop */
  topNav: {
    height: 66,
    position: 'relative',
    zIndex: VARIABLE.zIndex9,

    /** Logo desktop */
    logo: {
      padding: '0 40px 0 0',
      whiteSpace: 'nowrap',
      cursor: 'pointer',

      icon: {
        height: 30,
        widht: 30,
        fontSize: 30,
        display: 'block',
        color: VARIABLE.colorPink,
        marginRight: '25px',
      },

      text: {
        display: 'block',
        width: 130,
        height: 'auto',
      },
    },

    /** Search group: dropdown - input - button */
    search: {
      flex: 10,
      height: 40,
      border: `1px solid ${VARIABLE.colorE5}`,

      /** Dropdown to select category */
      dropdown: {
        backgroundColor: VARIABLE.colorFA,
        borderRight: `1px solid ${VARIABLE.colorE5}`,
        cursor: 'pointer',
        position: 'relative',

        text: {
          lineHeight: '38px',
          height: 38,
          color: VARIABLE.color97,
          fontFamily: VARIABLE.fontAvenirMedium,
          padding: '0 32px 0 20px',
          position: 'relative',
          display: 'block',
          whiteSpace: 'nowrap',

          icon: {
            position: 'absolute',
            right: '0px',
            top: '0px',
            fontSize: '10px',
            width: '32px',
          },
        },

        list: {
          backgroundColor: VARIABLE.colorFA,
          borderTop: `1px solid ${VARIABLE.colorE5}`,
          borderRight: `1px solid ${VARIABLE.colorE5}`,
          borderBottom: `1px solid ${VARIABLE.colorE5}`,
          borderLeft: `1px solid ${VARIABLE.colorE5}`,
          position: 'absolute',
          left: -1,
          padding: '10px 0',
          overflow: 'hidden',
          transition: VARIABLE.transitionNormal,

          item: {
            height: 34,
            lineHeight: '34px',
            fontSize: 14,
            padding: '0 20px',
            color: VARIABLE.color97,
            whiteSpace: 'nowrap',

            ':hover': {
              color: VARIABLE.color2E
            },
          },
        },
      },

      /** Input search */
      input: {
        flex: 10,
        border: 'none',
        outline: 'none',
        fontSize: '14px',
        color: VARIABLE.color4D,
        padding: '0 15px',
      },

      /** Button search */
      button: {
        width: 50,
        height: 40,
        backgroundColor: 'transparent',
        color: VARIABLE.color75,
        lineHeight: '40px',
        fontSize: 18,
        marginRight: -1,
        marginTop: -1,
        cursor: 'pointer',
        transition: VARIABLE.transitionColor,

        ':hover': {
          color: VARIABLE.colorBlack,
        }
      },
    },

    /** Sign in / Sign up */
    signInSignUp: {
      color: VARIABLE.colorPink,
      border: `1px solid ${VARIABLE.colorPink}`,
      borderRadius: '33px',
      height: 34,
      lineHeight: '32px',
      padding: '0 20px',
      fontSize: 15,
      marginLeft: 30,
      whiteSpace: 'nowrap',
      cursor: 'pointer',
      transition: VARIABLE.transitionNormal,

      ':hover': {
        background: VARIABLE.colorPink,
        color: VARIABLE.colorWhite
      },
    },
  },

  /** List navigation */
  listNav: {
    position: 'relative',
    height: 44,
    zIndex: VARIABLE.zIndex5,

    /** Main menu */
    nav: {
      position: 'relative',
      zIndex: VARIABLE.zIndex9,

      /** Item menu */
      navItem: {
        cursor: 'pointer',
        height: 44,
        lineHeight: '44px',
        padding: 0,
        marginRight: 10,
        fontFamily: VARIABLE.fontAvenirDemiBold,
        color: VARIABLE.colorBlack,

        [MEDIA_QUERIES.desktop1280]: {
          marginRight: 30,
        },

        active: {
          color: VARIABLE.colorPink,
          borderBottom: `3px solid ${VARIABLE.colorPink}`,
        },
      },
    },

    navText: {
      /** Defined general for text in nav: main - sub - bold */
      main: {
        fontSize: 15,
        lineHeight: '20px',
        fontFamily: VARIABLE.fontAvenirRegular,
        color: VARIABLE.color2E,
        opacity: 0.9,
        whiteSpace: 'nowrap',
      },

      active: {
        color: VARIABLE.colorPink,
      },

      inner: {
        background: VARIABLE.colorRed,
        color: VARIABLE.colorWhite,
        height: 18,
        lineHeight: '18px',
        margin: '0 2px 0 5px',
        padding: '0 5px',
        borderRadius: 3,
        fontSize: 10,
      },

      sub: {
        fontSize: 14,
        lineHeight: '20px',
        fontFamily: VARIABLE.fontAvenirRegular,
        color: VARIABLE.color4D,
        opacity: 0.5,
        whiteSpace: 'nowrap',
      },

      bold: {
        fontSize: 15,
        lineHeight: '20px',
        fontFamily: VARIABLE.fontAvenirDemiBold,
        color: VARIABLE.colorBlack,
        opacity: 0.9,
        whiteSpace: 'nowrap',
      },

      icon: {
        color: VARIABLE.colorBlack,
        opacity: 0.9,
        fontSize: 10,
        width: 20,
        textAlign: 'center',

        active: {
          color: VARIABLE.colorPink
        },

        inBlock: {
          marginTop: 11,
        }
      },
    },

    rightNav: {
      block: {
        cursor: 'pointer',
        marginLeft: 20,

        info: {
          textAlign: 'left',
        },
      },

      shoppingCart: {
        marginLeft: '0',

        largeSpace: {
          marginLeft: 20,
        },

        icon: {
          width: 44,
          height: 44,
          lineHeight: '44px',
          fontSize: 22,
          color: VARIABLE.color75,
          position: 'relative',

          value: {
            backgroundColor: VARIABLE.colorPink,
            color: VARIABLE.colorWhite,
            height: 20,
            lineHeight: '16px',
            fontSize: 10,
            position: 'absolute',
            fontStyle: 'normal',
            borderRadius: 10,
            padding: '0 6px',
            top: 2,
            right: -2,
            border: `2px solid ${VARIABLE.colorWhite}`,
          }
        },
      },

      wishList: {
        marginLeft: '0',
        cursor: 'pointer',

        [MEDIA_QUERIES.desktop1280]: {
          marginLeft: 20,
        },

        icon: {
          width: 60,
          height: 44,
          lineHeight: '44px',
          fontSize: 22,
          color: VARIABLE.colorPink,
          position: 'relative',
        },
      },
    },
  },

  subNavPanel: {
    position: 'absolute',
    width: '100vw',
    left: 0,
    top: 110,
    height: '100vh',
    zIndex: VARIABLE.zIndex5,
  },

  subNavOverlay: {
    background: VARIABLE.colorBlack03,
    position: 'absolute',
    width: '100%',
    height: '100%',
    left: 0,
    top: 0,
  },

  subNavContainer: {
    width: '100%',
    height: 350,
    backgroundColor: VARIABLE.colorWhite,
    boxShadow: VARIABLE.shadow3,
    position: 'relative',
    zIndex: VARIABLE.zIndex9,
  },

  subBox: {
    padding: 10,
    height: 350,

    item: {
      flex: 1,
      margin: 10,
      position: 'relative',
      backgroundSize: 'cover',
      backgorundPosition: 'center',
      border: `1px solid ${VARIABLE.colorF0}`,
      cursor: 'pointer',

      ':hover': {
        border: `1px solid ${VARIABLE.colorD2}`,
      },

      info: {
        padding: '12px 15px',
        height: '100%',
        width: '100%',
      },

      title: {
        fontSize: 18,
        lineHeight: '30px',
        color: VARIABLE.colorBlack,
        textTransform: 'uppercase',
        fontFamily: VARIABLE.fontAvenirLight,
        whiteSpace: 'nowrap',
      },

      description: {
        fontSize: 13,
        lineHeight: '18px',
        color: VARIABLE.color97,
        fontFamily: VARIABLE.fontAvenirRegular,
      },
    }
  },

  subCategory: {
    padding: 10,
    height: 'auto',
    minHeight: 300,
    backgroundRepeat: 'no-repeat',
    backgroundPosition: 'right bottom',

    column: {
      padding: 20,
      flex: 1,
      maxWidth: 310,

      group: {
      },

      groupSub: {}
    },

    title: {
      display: 'block',
      cursor: 'pointer',
      width: '100%',
      fontSize: 14,
      lineheight: '22px',
      color: VARIABLE.color2E,
      marginBottom: 5,

      ':hover': {
        color: VARIABLE.colorPink
      }
    },

    description: {
      display: 'block',
      width: '100%',
      fontSize: 12,
      lineheight: '18px',
      color: VARIABLE.color75,
      marginBottom: 15,

      withBorder: {
        borderBottom: `1px solid ${VARIABLE.colorE5}`,
        paddingBottom: 7,
        marginBottom: 15,
      }
    },

    heading: {
      display: 'block',
      cursor: 'pointer',
      width: '100%',
      textTransform: 'uppercase',
      fontSize: 15,
      lineheight: '24px',
      color: VARIABLE.color4D,
      fontFamily: VARIABLE.fontAvenirDemiBold,
      padding: '5px 0 3px',

      ':hover': {
        color: VARIABLE.colorPink
      }
    }
  }
};
