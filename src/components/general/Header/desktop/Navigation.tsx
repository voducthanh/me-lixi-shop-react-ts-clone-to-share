/**
 * Created by Thành Võ on 13.03.2017
 */

import * as React from 'react';
import * as Radium from 'radium';
const connect = require('react-redux').connect;
import { Link } from 'react-router';

import { decodeEntities } from '../../../../utils/htmlEntities';

import * as VARIABLE from '../../../../styles/variable.style';
import * as LAYOUT from '../../../../styles/layout.style';

import STYLE from './Navigation.style';

import { NAVIGATION_BOX } from '../../../../constants/navigation';
const LOGO_TEXT = require('../../../../assets/images/header/logo-text.png');
const BG_MENU = require('../../../../assets/images/navigation/box/0.jpg');

/**  PROPS & STATE INTERFACE */
interface INavigationDesktopProps extends React.Props<any> {
  fixHeader: any;
  listMenu: any;
};

function mapStateToProps(state) {
  return {
    listMenu: state.menu.get('listMenu').toJS(),
  };
}

function mapDispatchToProps(dispatch) {
  return {};
}

interface INavigationDesktopState {
  showCategorySearch: boolean;
  categorySearchList: Array<any>;
  categorySearchSelected: any;
  menu: Array<any>;
};

@Radium
class NavigationDesktop extends React.Component<INavigationDesktopProps, any> {
  static defaultProps: INavigationDesktopProps;

  constructor(props) {
    super(props);

    this.state = {
      subMenuDesktop: {
        show: false,
        id: ''
      },
      showCategorySearch: false,
      categorySearchList: [
        {
          id: 1,
          title: 'Tất cả'
        },
        {
          id: 2,
          title: 'Beauty Box'
        },
        {
          id: 3,
          title: 'Sản phẩm'
        },
        {
          id: 4,
          title: 'Chuyên gia'
        }
      ],
      categorySearchSelected: {
        id: 1,
        title: 'Tất cả'
      },
      box: NAVIGATION_BOX,
      listCategoryReject: [
        841, 992, 844
      ]
    };
  }

  initData(_newListMenu: any = this.state.listMenu) {
  }

  /**
   * When component will receive new props -> init data for slide
   * @param nextProps prop from parent
   */
  componentWillReceiveProps(nextProps) {
    this.initData(nextProps.listMenu);
  }

  /**
   * Show Menu Desktop popup
   * @param id number : id to show sub menu
   */
  showMenuDesktop(_id) {
    this.setState({
      subMenuDesktop: {
        show: true,
        id: _id
      }
    });
  }

  /**
   * Hide menu desktop popup
   */
  hideMenuDesktop() {
    this.setState({
      subMenuDesktop: {
        show: false,
        id: ''
      }
    });
  }

  /**
   * Render top navigation
   * 1. Logo
   * 2. Search box
   * 2.1. Category search
   * 2.2 Input search
   * 2.3 Button search
   * 3. Button signin / signup
   */
  renderTopNavigation() {
    const {
      categorySearchSelected,
      showCategorySearch,
      categorySearchList,
      menu
    } = this.state;

    return (
      <div
        onMouseEnter={() => this.hideMenuDesktop()}
        style={[
          LAYOUT.flexContainer.justify,
          LAYOUT.flexContainer.verticalCenter,
          STYLE.topNav
        ]}>

        {/** 1. Logo */}
        <Link to={'/'}>
          <div
            style={[
              LAYOUT.flexContainer.justify,
              LAYOUT.flexContainer.verticalCenter,
              STYLE.topNav.logo
            ]}>
            <i className="lx lx-01-logo" style={STYLE.topNav.logo.icon}></i>
            <img src={LOGO_TEXT} style={STYLE.topNav.logo.text} />
          </div>
        </Link>

        {/** 2. Search box */}
        <div style={[LAYOUT.flexContainer.justify, STYLE.topNav.search]}>

          {/** 2.1. Seach box - dropdown list category search */}
          <div
            key="list-category-search"
            style={STYLE.topNav.search.dropdown}
            onMouseEnter={() => this.setState({ showCategorySearch: true })}
            onMouseLeave={() => this.setState({ showCategorySearch: false })}>
            <span style={STYLE.topNav.search.dropdown.text}>
              {categorySearchSelected.title}
              <i
                className="lx lx-08-down"
                style={STYLE.topNav.search.dropdown.text.icon}></i>
            </span>
            <div
              style={[
                STYLE.topNav.search.dropdown.list,
                true === showCategorySearch
                  ? {
                    height: 156,
                    padding: '10px 0',
                    visibility: 'visible',
                    opacity: 1
                  }
                  : {
                    height: 0,
                    padding: 0,
                    visibility: 'hidden',
                    opacity: 0
                  }
              ]}>
              {/** List category for search */}
              {
                categorySearchList.map(category =>
                  <div
                    key={`category-${category.id}`}
                    style={STYLE.topNav.search.dropdown.list.item}
                    onClick={() => this.setState({
                      categorySearchSelected: category,
                      showCategorySearch: false
                    })}>
                    {category.title}
                  </div>
                )
              }
            </div>
          </div>

          {/** 2.2. Seach box - input */}
          <input
            type="text"
            placeholder="Nhập từ khoá tìm kiếm..."
            style={STYLE.topNav.search.input} />

          {/** 2.3. Seach box - icon search */}
          <i
            className="lx lx-04-search"
            style={STYLE.topNav.search.button}></i>
        </div>
        {
          /** 3. Sign in / Sign up */
          <div
            key="sign-in-sign-up"
            style={STYLE.topNav.signInSignUp}>
            Sign in / Sign up
            </div>
        }
      </div>
    );
  }

  /**
   * Render List navigation
   * 1. Left: Main menu
   * 2. User info
   * 2.1 Favorite list
   * 2.2. Shopping cart
   */
  renderListNavigation() {
    const {
      categorySearchSelected,
      showCategorySearch,
      categorySearchList,
      menu,
      box,
      listCategoryReject,
      subMenuDesktop
    } = this.state;

    const { listMenu } = this.props;

    return (
      <div
        style={[
          LAYOUT.flexContainer.justify,
          LAYOUT.flexContainer.verticalCenter,
          STYLE.listNav,
        ]}>


        {/** 1. Main Menu : Left */}
        <nav
          style={[
            LAYOUT.flexContainer.left,
            STYLE.listNav.nav
          ]}>
          <Link
            key={`link-box-item-${box.id}`}
            to={`shop/${box.slug}`}
            onClick={() => this.hideMenuDesktop()}>
            <div
              key={`box-item-${box.id}`}
              onMouseEnter={() => this.showMenuDesktop(box.id)}
              style={[
                LAYOUT.flexContainer.left,
                LAYOUT.flexContainer.verticalCenter,
                STYLE.listNav.navText.main,
                STYLE.listNav.nav.navItem,
                box.id === subMenuDesktop.id && STYLE.listNav.navText.active,
              ]}>
              {decodeEntities(box.title)}
              <i
                className="lx lx-08-down"
                style={[
                  STYLE.listNav.navText.icon,
                  box.id === subMenuDesktop.id && STYLE.listNav.navText.icon.active
                ]}></i>
            </div>
          </Link>

          {
            'undefined' !== typeof listMenu.browse_nodes &&
            listMenu.browse_nodes[1].sub_nodes.map(menuItem =>
              listCategoryReject.indexOf(menuItem.id) < 0 &&
              <Link
                key={`link-menu-item-${menuItem.id}`}
                to={`shop/${menuItem.slug}`}
                onClick={() => this.hideMenuDesktop()}>
                <div
                  key={`menu-item-${menuItem.id}`}
                  onMouseEnter={() => this.showMenuDesktop(menuItem.id)}
                  style={[
                    LAYOUT.flexContainer.left,
                    LAYOUT.flexContainer.verticalCenter,
                    STYLE.listNav.navText.main,
                    STYLE.listNav.nav.navItem,
                    menuItem.id === subMenuDesktop.id && STYLE.listNav.navText.active,
                  ]}>
                  {decodeEntities(menuItem.name)}
                  <i
                    className="lx lx-08-down"
                    style={[
                      STYLE.listNav.navText.icon,
                      menuItem.id === subMenuDesktop.id && STYLE.listNav.navText.icon.active
                    ]}></i>
                </div>
              </Link>
            )
          }
        </nav>

        {/** 2. User info, Shopping cart : Right */}
        <div
          style={[
            LAYOUT.flexContainer.right,
            STYLE.listNav.rightNav
          ]}>

          {/** 2.1. Favorite list */}
          <div
            style={[
              LAYOUT.flexContainer.verticalCenter,
              STYLE.listNav.rightNav.block,
              STYLE.listNav.rightNav.wishList,
            ]}>
            <i
              className="lx lx-18-heart"
              style={STYLE.listNav.rightNav.wishList.icon}></i>
          </div>

          {/** 2.2. Shopping cart */}
          {
            window.innerWidth > VARIABLE.breakPoint960 &&
            <div
              style={[
                LAYOUT.flexContainer.verticalCenter,
                STYLE.listNav.rightNav.block,
                STYLE.listNav.rightNav.shoppingCart,
              ]}>
              <i
                className="lx lx-17-shopping-bag"
                style={STYLE.listNav.rightNav.shoppingCart.icon}>
                <span
                  style={STYLE.listNav.rightNav.shoppingCart.icon.value}>
                  1
                </span>
              </i>
              <div
                style={[
                  LAYOUT.flexContainer.center,
                  LAYOUT.flexContainer.verticalCenter,
                  STYLE.listNav.rightNav.block.info
                ]}>

                <div style={STYLE.listNav.navText.bold}>Giỏ hàng</div>
              </div>
            </div>
          }
        </div>
      </div >
    );
  }

  /**
   * Render sub navigation: show when hover
   */
  renderSubNavigation() {
    const {
      box,
      listCategoryReject,
      subMenuDesktop,
    } = this.state;
    const {
      listMenu
    } = this.props;

    return (
      true === subMenuDesktop.show &&
      <div style={STYLE.subNavPanel}>
        {/** Main wrap */}
        < div style={LAYOUT.wrapLayout} >
          {/** 1. Sub Navigation for BOXES */}
          {
            box.id === subMenuDesktop.id &&
            <div
              onMouseLeave={() => this.hideMenuDesktop()}
              style={
                [
                  LAYOUT.flexContainer.justify,
                  STYLE.subNavContainer,
                  STYLE.subBox,
                ]}>
              {
                /** Render each box */
                box.sub.map(sub =>
                  <div
                    key={`box-sub-${sub.id}`}
                    style={[
                      { backgroundImage: `url('${sub.background}')` },
                      STYLE.subBox.item
                    ]}>
                    <div style={[LAYOUT.flexContainer.verticalFlex, STYLE.subBox.item.info]}>

                      {/** Box title */}
                      <div style={STYLE.subBox.item.title}>
                        {decodeEntities(sub.title)}
                      </div>

                      {/** Box description */}
                      <div style={STYLE.subBox.item.description}>
                        {decodeEntities(sub.description)}
                      </div>
                    </div>
                  </div>
                )
              }
            </div>
          }

          {/** 2. Sub Navigation for PRODUCT CATEGORIES */}
          {
            'undefined' !== typeof listMenu.browse_nodes &&
            listMenu.browse_nodes[1].sub_nodes.map(menuItem =>
              listCategoryReject.indexOf(menuItem.id) < 0 &&
              menuItem.id === subMenuDesktop.id &&
              <div
                key={`category-sub-${menuItem.id}`}
                onMouseLeave={() => this.hideMenuDesktop()}
                style={[
                  { backgroundImage: `url('${BG_MENU}')` },
                  LAYOUT.flexContainer.left,
                  STYLE.subNavContainer,
                  STYLE.subCategory,
                ]}>
                {
                  this.renderSubCategory(menuItem)
                }
              </div>
            )
          }
        </div>

        {/** Sub Wrap overlay */}
        <div style={STYLE.subNavOverlay}
          onMouseEnter={() => this.hideMenuDesktop()}></div>
      </div >
    );
  }

  /**
   * Render component sub category
   * @param category Object menu
   */
  renderSubCategory(category: any) {
    let data = [[], [], [], [], []];

    /**
     * Split data into every group
     */
    category.sub_nodes.map(sub => {
      data[sub.menu_column].push(sub);
    });

    data = data.filter(item => item.length > 0);

    return (
      data.map((sub, $index) =>
        <div
          style={STYLE.subCategory.column}
          key={`col-sub-category-${$index}`}>
          {
            sub.map(item =>
              <div
                style={STYLE.subCategory.column.group}
                key={`item-col-category-${item.id}`}>
                <a
                  style={STYLE.subCategory.heading}
                  key={`item-col-category-heading-${item.id}`}>
                  {decodeEntities(item.name)}
                </a>
                <span
                  style={[
                    STYLE.subCategory.description,
                    STYLE.subCategory.description.withBorder
                  ]}>
                  {decodeEntities(item.vn_name)}
                </span>
                {
                  item.sub_nodes.length > 0 &&
                  <div style={STYLE.subCategory.column.groupSub}>
                    {
                      item.sub_nodes.map(itemSub =>
                        <div key={`item-col-sub-category-${itemSub.id}`}>
                          <a
                            key={`item-col-sub-category-title-${itemSub.id}`}
                            style={STYLE.subCategory.title}>
                            {decodeEntities(itemSub.name)}
                          </a>
                          <span style={STYLE.subCategory.description}>
                            {decodeEntities(itemSub.vn_name)}
                          </span>
                        </div>
                      )
                    }
                  </div>
                }
              </div>
            )
          }
        </div>
      )
    );
  }

  render() {
    const {
      categorySearchSelected,
      showCategorySearch,
      categorySearchList,
      menu
    } = this.state;

    return (
      <div style={[STYLE , this.props.fixHeader && STYLE.fixHeader]}>
        <div style={[STYLE.wrapContent]}>
          <div key="header-navigation" style={LAYOUT.wrapLayout}>

            {/** A. Top nav: logo + search + sign in / sign up */}
            {
              this.renderTopNavigation()
            }

            {/** B. List nav: menu + wish list + shopping cart */}
            {
              this.renderListNavigation()
            }
          </div>
        </div>
        {/** C. Navigation sub list overlay background */ }
        {
          this.renderSubNavigation()
        }
      </div>
    );
  }
};

NavigationDesktop.defaultProps = {
  fixHeader: false,
  listMenu: []
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(NavigationDesktop);
