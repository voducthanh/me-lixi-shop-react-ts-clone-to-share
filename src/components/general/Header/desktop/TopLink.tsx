/**
 * Created by Thành Võ on 09.03.2017
 */

import * as React from 'react';
import * as Radium from 'radium';

import * as LAYOUT from '../../../../styles/layout.style';
import STYLE from './TopLink.style';

const CAMPAIGN_BACKGROUND = require('../../../../assets/images/sample-data/banner-campaign.png');

/**  PROPS & STATE INTERFACE */
interface ITopLinkProps extends React.Props<any> { };

interface ITopLinkState { };

@Radium
class TopLink extends React.Component<ITopLinkProps, any> {
  constructor(props) {
    super(props);
  }

  render() {
    return (
      <div style={STYLE}>
        <div
          key="header-nav-top"
          style={[
            LAYOUT.wrapLayout,
            LAYOUT.flexContainer.right,
            LAYOUT.flexContainer.verticalCenter,
            STYLE.wrap,
          ]}>
          <div key="magazine" style={[STYLE.item, STYLE.itemLine]}> Magazine</div>
          <div key="lixicoin" style={STYLE.item}> Đổi Lixicoin </div>
        </div>
      </div>
    );
  }
};

export default TopLink;
