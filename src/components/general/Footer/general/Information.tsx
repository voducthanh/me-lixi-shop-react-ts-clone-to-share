/**
 * Created by Thành Võ on 17.03.2017
 */

import * as React from 'react';
import * as Radium from 'radium';

import * as VARIABLE from '../../../../styles/variable.style';
import * as LAYOUT from '../../../../styles/layout.style';

const BG_PAYMENT_VISA = require('../../../../assets/images/footer/payment-visa.png');
const BG_PAYMENT_MASTERCARD = require('../../../../assets/images/footer/payment-master-card.png');
const BG_PAYMENT_JCB = require('../../../../assets/images/footer/payment-jcb.png');
const BG_PAYMENT_AMERICAN_EXPRESS = require('../../../../assets/images/footer/payment-american-express.png');
const BG_SUPPORT_ONLINE = require('../../../../assets/images/footer/support-online.png');

import STYLE from './Information.style';

/**  PROPS & STATE INTERFACE */
interface IFooterInformationProps extends React.Props<any> { };

interface IFooterInformationState {
  paymentList: Array<any>;
};

@Radium
class FooterInformation extends React.Component<IFooterInformationProps, any> {
  constructor(props: IFooterInformationProps) {
    super(props);

    this.state = {
      paymentList: [
        {
          id: 1,
          link: '#',
          title: 'Visa',
          image: BG_PAYMENT_VISA
        },
        {
          id: 2,
          link: '#',
          title: 'MasterCard',
          image: BG_PAYMENT_MASTERCARD
        },
        {
          id: 3,
          link: '#',
          title: 'JCB',
          image: BG_PAYMENT_JCB
        },
        {
          id: 4,
          link: '#',
          title: 'American Express',
          image: BG_PAYMENT_AMERICAN_EXPRESS
        },
      ]
    };
  }

  render() {
    const { paymentList } = this.state;

    return (
      <div style={STYLE}>
        <div style={[LAYOUT.wrapLayout, LAYOUT.flexContainer.wrap]}>

          {/** 1. Social */}
          <div style={[STYLE.social, STYLE.col]}>
            <div style={STYLE.title}>Follow Lixibox on</div>
            <div style={[LAYOUT.flexContainer.center, STYLE.social.iconList]}>
              <div
                key={'social-footer-facebook'}
                className={'lx lx-20-facebook'}
                style={[STYLE.social.iconList.icon, STYLE.social.iconList.icon.facebook]}></div>
              <div
                key={'social-footer-instagram'}
                className={'lx lx-21-instagram'}
                style={[STYLE.social.iconList.icon, STYLE.social.iconList.icon.instagram]}></div>
              <div
                key={'social-footer-youtube'}
                className={'lx lx-22-youtube'}
                style={[STYLE.social.iconList.icon, STYLE.social.iconList.icon.youtube]}></div>
              <div
                key={'social-footer-pinterest'}
                className={'lx lx-23-pinterest'}
                style={[STYLE.social.iconList.icon, STYLE.social.iconList.icon.pinterest]}></div>
            </div>
          </div>

          {/** 2. Payment */}
          <div style={[STYLE.payment, STYLE.col]}>
            <div
              style={STYLE.title}>
              Phuơng thức thanh toán
            </div>
            <div
              style={STYLE.payment.description}>
              Chuyển khoản hoặc Thẻ tín dụng đuợc bảo mật bởi Comodo
            </div>
            <div style={STYLE.payment.list}>
              {
                paymentList.map((payment) =>
                  <img
                    key={`payment-footer-${payment.id}`}
                    src={payment.image}
                    style={STYLE.payment.list.item} />
                )
              }
            </div>
            <a href={'//mixpanel.com/f/partner'} style={STYLE.payment.mixpanel}>
              <img
                src={'//cdn.mxpnl.com/site_media/images/partner/badge_light.png'}
                style={STYLE.payment.mixpanel.logo} />
            </a>
          </div>

          {/** 3. Hotline */}
          <div
            style={[
              STYLE.hotLine,
              STYLE.col
            ]}>
            <div style={STYLE.title}>Hotline hỗ trợ</div>
            <div style={STYLE.hotLine.phone}>
              <div
                style={[
                  STYLE.hotLine.phone.icon,
                  { backgroundImage: `url('${BG_SUPPORT_ONLINE}')` }
                ]}></div>
              <div style={STYLE.hotLine.phone.number}>0871 062 999</div>
            </div>
          </div>
        </div>
      </div>
    );
  }
};

export default FooterInformation;
