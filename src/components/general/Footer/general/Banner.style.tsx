/**
 * Created by Thành Võ on 29.03.2017
 */

import MEDIA_QUERIES from '../../../../styles/mediaQueries.style';
import * as VARIABLE from '../../../../styles/variable.style';

export default {
  display: 'block',
  marginTop: 20,
  marginBottom: 30,

  banner: {
    height: 'auto',
    cursotr: 'pointer',
    width: '50%',
    transition: VARIABLE.transitionOpacity,

    ':hover': {
      opacity: 0.8
    },

    [MEDIA_QUERIES.tablet960]: {
      width: '25%',
    },

    image: {
      width: '100%',
      height: 'auto',
      display: 'block'
    }
  },
};
