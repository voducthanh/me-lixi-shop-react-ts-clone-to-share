/**
 * Created by Thành Võ on 29.03.2017
 */

import * as VARIABLE from '../../../../styles/variable.style';

export default {
  width: '100%',
  padding: '10px 0',

  nav: {
    display: 'block',
    color: VARIABLE.colorBlack,
    margin: '0 10px',
    fontSize: 12,
    lineHeight: '20px',
    fontFamily: VARIABLE.fontAvenirMedium,
    whiteSpace: 'nowrap',

    copyright: {
      color: VARIABLE.color4D,
    },
  }
};
