/**
 * Created by Thành Võ on 17.03.2017
 */

import * as React from 'react';
import * as Radium from 'radium';

import * as LAYOUT from '../../../../styles/layout.style';
import STYLE from './Navigation.style';

/**  PROPS & STATE INTERFACE */
interface IFooterNavigationProps extends React.Props<any> { };

interface IFooterNavigationState {
  navList: Array<any>;
};

@Radium
class FooterNavigation extends React.Component<IFooterNavigationProps, IFooterNavigationState> {
  constructor(props) {
    super(props);

    this.state = {
      navList: [
        {
          id: 1,
          title: 'About us',
          link: '#'
        },
        {
          id: 2,
          title: 'Terms',
          link: '#'
        },
        {
          id: 3,
          title: 'Privacy',
          link: '#'
        },
        {
          id: 4,
          title: 'Careers',
          link: '#'
        }
      ]
    };
  }

  render() {
    const { navList } = this.state;

    return (
      <div
        style={[
          LAYOUT.flexContainer.center,
          LAYOUT.flexContainer.wrap,
          STYLE
        ]}>
        <div
          style={[
            STYLE.nav,
            STYLE.nav.copyright
          ]}>
          © Lixibox Inc.
        </div>
        {
          navList.map(nav =>
            <a
              key={`footer-nav-${nav.id}`}
              href={nav.link}
              style={STYLE.nav}>
              {nav.title}
            </a>
          )
        }
      </div>
    );
  }
};

export default FooterNavigation;
