/**
 * Created by Thành Võ on 17.03.2017
 */

import * as React from 'react';
import * as Radium from 'radium';

import * as VARIABLE from '../../../../styles/variable.style';
import * as LAYOUT from '../../../../styles/layout.style';

const FOOTER_BANNER_1 = require('../../../../assets/images/footer/banner-footer-1.jpg');
const FOOTER_BANNER_2 = require('../../../../assets/images/footer/banner-footer-2.jpg');
const FOOTER_BANNER_3 = require('../../../../assets/images/footer/banner-footer-3.jpg');
const FOOTER_BANNER_4 = require('../../../../assets/images/footer/banner-footer-4.jpg');

import STYLE from './Banner.style';

/**  PROPS & STATE INTERFACE */
interface IFooterBannerProps extends React.Props<any> { };

interface IFooterBannerState {
  listBanner: Array<any>;
};

@Radium
class FooterBanner extends React.Component<IFooterBannerProps, IFooterBannerState> {
  constructor(props) {
    super(props);

    this.state = {
      listBanner: [
        FOOTER_BANNER_1,
        FOOTER_BANNER_2,
        FOOTER_BANNER_3,
        FOOTER_BANNER_4
      ]
    };
  }

  render() {
    const { listBanner } = this.state;

    return (
      <div style={[STYLE]}>
        <div
          style={[
            LAYOUT.flexContainer.wrap,
            LAYOUT.wrapLayout
          ]}>
          {
            listBanner.map((banner, $index) =>
              <div key={`banner-footer-${$index}`} style={STYLE.banner} >
                <img style={STYLE.banner.image} src={banner} />
              </div>
            )
          }
        </div>
      </div>
    );
  }
};

export default FooterBanner;
