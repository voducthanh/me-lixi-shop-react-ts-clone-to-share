/**
 * Created by Thành Võ on 29.03.2017
 */

import * as VARIABLE from '../../../../styles/variable.style';
import MEDIA_QUERIES from '../../../../styles/mediaQueries.style';

export default {
  display: 'block',
  backgroundColor: VARIABLE.colorE5,
  padding: '20px 0',

  col: {
    width: '100%',

    [MEDIA_QUERIES.tablet768]: {
      width: '33.333%',
    },
  },

  title: {
    width: '100%',
    fontSize: 12,
    color: VARIABLE.color4D,
    textTransform: 'uppercase',
    textAlign: 'center',
    marginBottom: 30,
  },

  social: {
    padding: '20px 0',

    iconList: {
      width: '100%',

      icon: {
        width: 50,
        height: 50,
        lineHeight: '50px',
        borderRadius: '50%',
        fontSize: 20,
        backgroundColor: VARIABLE.color97,
        color: VARIABLE.colorWhite,
        margin: '0 5px 20px',
        cursor: 'pointer',
        transition: VARIABLE.transitionBackground,

        facebook: {
          ':hover': {
            fontSize: 25,
            backgroundColor: VARIABLE.colorSocial.facebook
          }
        },

        instagram: {
          ':hover': {
            fontSize: 25,
            backgroundColor: VARIABLE.colorSocial.instagram
          }
        },

        youtube: {
          ':hover': {
            fontSize: 25,
            backgroundColor: VARIABLE.colorSocial.youtube
          }
        },

        pinterest: {
          ':hover': {
            fontSize: 25,
            backgroundColor: VARIABLE.colorSocial.pinterest
          }
        }
      },
    }
  },

  payment: {
    padding: '20px 0',

    description: {
      width: '100%',
      fontSize: 12,
      lineHeight: '20px',
      color: VARIABLE.color4D,
      textAlign: 'center',
      marginBottom: 30,
    },

    list: {
      width: '100%',
      textAlign: 'center',
      marginBottom: 15,

      item: {
        display: 'inline-block',
        height: 35,
        width: 'auto',
        cursor: 'pointer',
        margin: '0 10px',
      },
    },

    mixpanel: {
      display: 'block',
      margin: '0 auto',
      width: 114,
      height: 'auto',
      logo: {}
    },
  },

  hotLine: {
    padding: '10px 0',

    phone: {
      width: '100%',
      textAlign: 'center',

      icon: {
        width: 40,
        height: 40,
        display: 'inline-block',
        verticalAlign: 'top',
        marginRight: 10,
        backgroundSize: 'cover',
      },

      number: {
        display: 'inline-block',
        verticalAlign: 'top',
        lineHeight: '40px',
        height: 40,
        fontSize: 20,
        color: VARIABLE.colorPink
      }
    },
  }
};
