/**
 * Created by Thành Võ on 29.03.2017
 */

export default {
  display: 'block',
  position: 'relative',

  mobileArticle: {
    paddingBottom: 64,
  }
};
