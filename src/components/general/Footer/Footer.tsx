/**
 * Created by Thành Võ on 17.03.2017
 */

import * as React from 'react';
import * as Radium from 'radium';

import FooterBanner from './general/Banner';
import FooterInformation from './general/Information';
import FooterNavigation from './general/Navigation';

import * as VARIABLE from '../../../styles/variable.style';

import STYLE from './Footer.style';

/**  PROPS & STATE INTERFACE */
interface IFooterProps extends React.Props<any> {
  routes: any;
};

interface IFooterState {
  showNaviTop: boolean;
};

@Radium
class Footer extends React.Component<IFooterProps, any> {
  constructor(props: IFooterProps) {
    super(props);
    this.state = {
      showNaviTop: true
    };
  }

  render() {
    const { routes } = this.props;

    return (
      <div
        style={[
          STYLE,
          'article' === routes[1].path
          && window.innerWidth < VARIABLE.breakPoint960
          && STYLE.mobileArticle
        ]}>
        <FooterBanner />
        <FooterInformation />
        <FooterNavigation />
      </div>
    );
  }
};

export default Footer;
