

// /* Transition */
// @mixin transition($timeTransition: 500ms) {
//   -webkit-transition   : all $timeTransition cubic-bezier(0.230, 1.000, 0.320, 1.000);
//   -moz-transition      : all $timeTransition cubic-bezier(0.230, 1.000, 0.320, 1.000);
//   -o-transition        : all $timeTransition cubic-bezier(0.230, 1.000, 0.320, 1.000);
//   transition           : all $timeTransition cubic-bezier(0.230, 1.000, 0.320, 1.000);
// }

// /* Scale */
// @mixin scaleTransform($scale: 1) {
//   -webkit-transform   : scale($scale);
//   -moz-transform      : scale($scale);
//   -o-transform        : scale($scale);
//   transform           : scale($scale);
// }

// /* Transform translate */
// @mixin transformTranslate($transform-x: 0px, $transform-y: 0px) {
//   -webkit-transform : translate($transform-x, $transform-y);
//   -ms-transform     : translate($transform-x, $transform-y);
//   -o-transform      : translate($transform-x, $transform-y);
//   transform         : translate($transform-x, $transform-y);
// }

// @mixin transformRotate($deg: 0px) {
//   -webkit-transform : rotate($deg);
//   -ms-transform     : rotate($deg);
//   -o-transform      : rotate($deg);
//   transform         : rotate($deg);
// }

// /* Border radius */
// @mixin radius($radius: 3px) {
//   -webkit-border-radius : $radius;
//   -moz-border-radius    : $radius;
//   border-radius         : $radius;
// }

// /* Clear Fix */
// @mixin clearFix() {
//   &:after {
//     display: block;
//     content: "";
//     width: 0;
//     height: 0;
//     clear: both;
//     visibility: hidden;
//     opacity: 0;
//   }
// }

// /* Flex box */
// @mixin displayFlex() {
//   display: -webkit-flex;
//   display: flex;
// }

// /* Calc */
// @mixin calc($property, $expression) {
//   #{$property}: -moz-calc(#{$expression});
//   #{$property}: -webkit-calc(#{$expression});
//   #{$property}: calc(#{$expression});
// }

// /* text ellipsis */
// @mixin textEllipsis() {
//   white-space: nowrap;
//   overflow: hidden;
//   text-overflow: ellipsis;
//   max-width: 100%;
// }

// /* Box Shadow */
// @mixin boxShadow($val){
//   -webkit-box-shadow: $val;
//   -moz-box-shadow: $val;
//   box-shadow: $val;
// }

// @mixin arrow-up(){
//   @include transformRotate(45deg);
//   display: block;
//   content: "";
//   width: 12px;
//   height: 12px;
//   background: $colorContentBackground;
//   border-top: 1px solid $colorContentBorder;
//   border-left: 1px solid $colorContentBorder;
//   position: absolute;
//   top: -7px;
//   right: 10px;
//   z-index: 1;
// }

// @mixin triangle($color: $colorWhite, $height: 5, $side: 3){
//     width: 0;
//     height: 0;
//     border-left: #{$side}px solid transparent;
//     border-right: #{$side}px solid transparent;
//     border-top: #{$height}px solid $color;
// }

// /* Mixin  vertical-align */
// @mixin vertical-align($position: relative) {
//   position: $position;
//   top: 50%;
//   -webkit-transform: translateY(-50%);
//   -ms-transform: translateY(-50%);
//   transform: translateY(-50%);
// }

// @mixin align2side($position: relative) {
//   @include transformTranslate(-50%, -50%);
//   position: $position;
//   top: 50%;
//   left: 50%;
// }

// /*
// ==== using ======

// .element p {
//   @include vertical-align();
// }
// */
// @mixin font-size($size: 14px) {
//   font-size: $size !important;
// }

// @mixin loadAnimation($data) {
//   animation: $data;
//   -ms-animation: $data;
//   -o-animation: $data;
//   -webkit-animation: $data;
//   -moz-animation: $data;
// }