/* STYLE FOR LAYOUT - GRID */

import * as variableStyle from './variable.style';
import MEDIA_QUERIRES from './mediaQueries.style';

/*----------  GENERAL WRAP LAYOUT  ----------*/

export const wrapLayout = {
  width: '100%',
  maxWidth: variableStyle.breakPoint1280,
  minWidth: variableStyle.breakPoint320,
  margin: '0 auto',
  padding: '0',

  [MEDIA_QUERIRES.tablet960]: {
    paddingLeft: 15,
    paddingRight: 15,
  }
};

/*----------  SPLIT LAYOUT: MAIN / RIGHT COLUMN  ----------*/
export const splitContainer = {
  display: 'flex',
  flexWrap: 'wrap',

  main: {
    width: '100%',

    [MEDIA_QUERIRES.tablet960]: {
      width: 'calc(75% - 20px)',
    },
  },

  left: {
    width: '100%',

    [MEDIA_QUERIRES.tablet960]: {
      width: '25%',
    },
  },

  right: {
    width: '100%',

    [MEDIA_QUERIRES.tablet960]: {
      width: '25%',
    },
  }
};

/*----------  CONTAINER LAYOUT: FLEX / BLOCK  ----------*/

export const flexContainer = {
  wrap: {
    display: 'flex',
    flexWrap: 'wrap',
  },

  justify: {
    display: 'flex',
    justifyContent: 'space-between'
  },

  left: {
    display: 'flex',
    justifyContent: 'flex-start',
  },

  right: {
    display: 'flex',
    justifyContent: 'flex-end',
  },

  center: {
    display: 'flex',
    justifyContent: 'center',
  },

  verticalFlex: {
    display: 'flex',
    flexDirection: 'column',
  },

  verticalFlexBottom: {
    display: 'flex',
    flexDirection: 'column',
    justifyContent: 'flex-end',
  },

  verticalCenter: {
    display: 'flex',
    alignItems: 'center'
  }
};

/*----------  COMLUMN LAYOUT  ----------*/

export const layoutColumn = {
  col1: { width: '8.333%' },
  col2: { width: '16.666%' },
  col3: { width: '25%' },
  col4: { width: '33.333%' },
  col5: { width: '41.666%' },
  col6: { width: '50%' },
  col7: { width: '58.333%' },
  col8: { width: '66.666%' },
  col9: { width: '75%' },
  col10: { width: '83.333%' },
  col11: { width: '91.666%' },
  col12: { width: '100%' }
};
