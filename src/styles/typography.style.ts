import * as VARIABLE from './variable.style';
import MEDIA_QUERIES from './mediaQueries.style';

/*----------  BODY TEXT  ----------*/
const textBase = {
  fontSize: 14,
  lineHeight: '20px',
  color: VARIABLE.color4D,
  textAlign: 'justify',
}

export const bodyText = {
  normal: Object.assign({},
    textBase,
    {
      fontFamily: VARIABLE.fontAvenirRegular
    }
  ),

  textPink: Object.assign({},
    textBase,
    {
      fontFamily: VARIABLE.fontAvenirRegular,
      color: VARIABLE.colorPink
    }
  ),

  sub: Object.assign({},
    textBase,
    {
      fontSize: 12,
      color: VARIABLE.color97,
    }
  ),

  bold: Object.assign({},
    textBase,
    {
      fontFamily: VARIABLE.fontAvenirDemiBold,
    }
  ),

  italic: Object.assign({},
    textBase,
    {
      fontStyle: 'italic'
    }
  ),

  titlePink: Object.assign({},
    textBase,
    {
      fontSize: 16,
      fontFamily: VARIABLE.fontAvenirDemiBold,
      color: VARIABLE.colorPink,
    }
  ),

  border: Object.assign({},
    textBase,
    {
      background: VARIABLE.colorE5,
      borderRadius: 2,
      padding: '0 5px',
      marginRight: 10,
      lineHeight: '22px',
    }
  ),
}

/*----------  TITLE  ----------*/

const titleBase = {
  fontSize: 18,
  lineHeight: '26px',
  textTransform: 'uppercase',
  textAlign: 'justify',
  color: VARIABLE.color2E,
}

export const title = {
  normal: Object.assign({},
    titleBase, {
      marginBottom: 10
    }
  )
}
