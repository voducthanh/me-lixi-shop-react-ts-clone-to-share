import * as VARIABLE from './variable.style';
import MEDIA_QUERIES from './mediaQueries.style';

/*----------  BUTTON  ----------*/

export const buttonIcon = {
  width: '12px',
  height: 'inherit',
  lineHeight: 'inherit',
  textAlign: 'center',
  color: 'inherit',
  display: 'inline-block',
  verticalAlign: 'middle',
  whiteSpace: 'nowrap',
  marginLeft: '5px',
  fontSize: '11px',
};

const buttonBase = {
  display: 'inline-block',
  textTransform: 'capitalize',
  height: 36,
  lineHeight: '36px',
  padding: '0 15px',
  fontSize: 15,
  fontFamily: VARIABLE.fontAvenirMedium,
  transition: VARIABLE.transitionNormal,
  whiteSpace: 'nowrap',
  borderRadius: 3,
  cursor: 'pointer',
};

export const button = Object.assign({},
  buttonBase,
  {
    backgroundColor: VARIABLE.colorBlack,
    color: VARIABLE.colorWhite,

    ':hover': {
      backgroundColor: VARIABLE.color2E,
    },

    pink: {
      backgroundColor: VARIABLE.colorPink,
      color: VARIABLE.colorWhite,

      ':hover': {
        backgroundColor: VARIABLE.colorPinkDarker
      }
    }
  }
);

export const buttonBorder = Object.assign({},
  buttonBase,
  {
    lineHeight: '34px',
    border: `1px solid ${VARIABLE.colorBlack}`,
    backgroundColor: VARIABLE.colorWhite,
    color: VARIABLE.colorBlack,

    ':hover': {
      border: `1px solid ${VARIABLE.colorPink}`,
      backgroundColor: VARIABLE.colorPink,
      color: VARIABLE.colorWhite
    },

    pink: {
      border: `1px solid ${VARIABLE.colorPink}`,
      color: VARIABLE.colorPink,

      ':hover': {
        backgroundColor: VARIABLE.colorPink,
        color: VARIABLE.colorWhite
      }
    }
  }
);

/*----------  BLOCK  ----------*/
export const block = {
  display: 'block',

  heading: {
    height: 56,
    width: '100%',
    textAlign: 'center',
    position: 'relative',
    marginBottom: 10,

    headingWithoutViewMore: {
      height: 50,
    },

    line: {
      height: 10,
      width: '100%',
      backgroundColor: VARIABLE.colorF0,
      position: 'absolute',
      top: 20,
    },

    title: {
      height: 50,
      lineHeight: '50px',
      display: 'inline-block',
      fontSize: 18,
      position: 'relative',
      iIndex: VARIABLE.zIndex2,
      backgroundColor: VARIABLE.colorWhite,
      color: VARIABLE.color4D,
      fontFamily: VARIABLE.fontAvenirMedium,
      padding: '0 15px',
      textTransform: 'uppercase',
    },


    viewMore: {
      whiteSpace: 'nowrap',
      fontSize: 13,
      height: 16,
      lineHeight: '16px',
      fontFamily: VARIABLE.fontAvenirRegular,
      color: VARIABLE.color97,
      display: 'block',
      cursor: 'pointer',
      padding: '0 10px',
      position: 'absolute',
      right: 0,
      bottom: 0,

      ':hover': {
        color: VARIABLE.color4D,
      },

      icon: {
        width: 16,
        height: 16,
        lineHeight: '16px',
        display: 'inline-block',
        fontSize: 9,
      },
    },
  },

  content: {
    padding: '20px 0 10px',
  }
};

/*----------  DOT DOT DOT SLIDE PAGINATION  ----------*/
export const slidePagination = {
  position: 'absolute',
  zIndex: VARIABLE.zIndex9,
  left: 0,
  bottom: 20,
  width: '100%',

  item: {
    display: 'block',
    width: 12,
    height: 12,
    borderRadius: '50%',
    margin: '0 5px',
    background: VARIABLE.colorBlack03,
    cursor: 'pointer',
  },

  itemActive: {
    background: VARIABLE.colorWhite,
    boxShadow: VARIABLE.shadowBlur,
  }
};

/*----------  SLIDE NAVIGATION BUTTON  ----------*/

export const slideNavigation = {
  position: 'absolute',
  width: 60,
  height: '100%',
  lineHeight: '100%',
  background: VARIABLE.colorWhite04,
  top: 0,
  zIndex: VARIABLE.zIndex9,
  cursor: 'pointer',
  transition: VARIABLE.transitionNormal,

  black: {
    background: VARIABLE.colorBlack005,
  },

  icon: {
    color: VARIABLE.color4D,
    fontSize: 20,
    width: 20,
    height: 20
  },

  left: {
    left: -60,

    active: {
      left: 0
    }
  },

  right: {
    right: -60,

    active: {
      right: 0
    }
  },
};

/*----------  ASIDE BLOCK (FILTER CATEOGRY)  ----------*/
export const asideBlock = {
  display: block,

  heading: {
    position: 'relative',
    height: 50,
    borderBottom: `1px solid ${VARIABLE.colorF0}`,
    display: 'flex',
    justifyContent: 'space-between',
    alignItems: 'center',
    paddingRight: 5,

    [MEDIA_QUERIES.tablet768]: {
      height: 60,
    },

    text: {
      width: 'auto',
      display: 'inline-block',
      fontSize: 16,
      lineHeight: '46px',
      fontFamily: VARIABLE.fontAvenirLight,
      textTransform: 'uppercase',
      padding: '0 20px',
      borderBottom: `4px solid ${VARIABLE.colorPink}`,
      whiteSpace: 'nowrap',

      [MEDIA_QUERIES.tablet768]: {
        fontSize: 18,
        lineHeight: '56px'
      },

      [MEDIA_QUERIES.tablet960]: {
        padding: 0
      },

      [MEDIA_QUERIES.tablet1024]: {
        fontSize: 20,
      }
    },

    close: {
      height: 40,
      width: 40,
      minWidth: 40,
      textAlign: 'center',
      lineHeight: '40px',
      marginBottom: 4,
      borderRadius: 3,
      cursor: 'pointer',

      ':hover': {
        backgroundColor: VARIABLE.colorF7
      }
    }
  },

  content: {
    padding: '20px 0 10px',
  }
};
