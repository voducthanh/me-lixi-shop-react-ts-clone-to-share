/*----------  COLOR LIST  ----------*/

export const colorBlack = '#000';
export const colorBlack005 = 'rgba(0, 0, 0, .05)';
export const colorBlack01 = 'rgba(0, 0, 0, .1)';
export const colorBlack02 = 'rgba(0, 0, 0, .2)';
export const colorBlack03 = 'rgba(0, 0, 0, .3)';
export const colorBlack04 = 'rgba(0, 0, 0, .4)';
export const colorBlack05 = 'rgba(0, 0, 0, .5)';
export const colorBlack06 = 'rgba(0, 0, 0, .6)';
export const colorBlack07 = 'rgba(0, 0, 0, .7)';
export const colorBlack08 = 'rgba(0, 0, 0, .8)';
export const colorBlack09 = 'rgba(0, 0, 0, .9)';

export const colorWhite = '#FFF';
export const colorWhite005 = 'rgba(255, 255, 255, .05)';
export const colorWhite01 = 'rgba(255, 255, 255, .1)';
export const colorWhite02 = 'rgba(255, 255, 255, .2)';
export const colorWhite03 = 'rgba(255, 255, 255, .3)';
export const colorWhite04 = 'rgba(255, 255, 255, .4)';
export const colorWhite05 = 'rgba(255, 255, 255, .5)';
export const colorWhite06 = 'rgba(255, 255, 255, .6)';
export const colorWhite07 = 'rgba(255, 255, 255, .7)';
export const colorWhite08 = 'rgba(255, 255, 255, .8)';
export const colorWhite09 = 'rgba(255, 255, 255, .9)';
export const colorWhite095 = 'rgba(255, 255, 255, .95)';

export const colorPink = '#FF2B70';
export const colorPink01 = 'rgba(255, 43, 112, .1)';
export const colorPink02 = 'rgba(255, 43, 112, .2)';
export const colorPink03 = 'rgba(255, 43, 112, .3)';
export const colorPink04 = 'rgba(255, 43, 112, .4)';
export const colorPink05 = 'rgba(255, 43, 112, .5)';
export const colorPink06 = 'rgba(255, 43, 112, .6)';
export const colorPink07 = 'rgba(255, 43, 112, .7)';
export const colorPink08 = 'rgba(255, 43, 112, .8)';
export const colorPink09 = 'rgba(255, 43, 112, .9)';

export const colorRed = '#FF4B4B';
export const colorPinkHightLight = '#FFE0E9';
export const colorPinkLighter = '#C71762';
export const colorPinkDarker = '#C71762';

export const color2E = '#2E3E4E';
export const color4D = '#4D4E4F';
export const color75 = '#757779';
export const color97 = '#97999A';
export const colorA2 = '#A2A3A5';
export const colorB0 = '#B0B2B4';
export const colorCC = '#CCCCCC';
export const colorD2 = '#D2D3D5';
export const colorE5 = '#E5E7EA';
export const colorF0 = '#F0F2F5';
export const colorF7 = '#F7F9FA';
export const colorFA = '#FAFCFE';

export const colorSocial = {
  facebook: '#3b5998',
  instagram: '#8a3ab9',
  youtube: '#FF0202',
  pinterest: '#BD081C',

};

/*----------  BOX SHADOW  ----------*/

export const shadowInsetBottom = '0 -2px 2px rgba(0,0,0,0.25) inset';
export const shadowInsetTop = '0 2px 2px rgba(0,0,0,0.25) inset';
export const shadowInsetMiddle = '0 0 2px rgba(0,0,0,0.25) inset';
export const shadowText = '0px 2px 2px rgba(0, 0, 0, 0.9)';
export const shadowReverse = '0 -5px 7px rgba(0,0,0,0.1)';
export const shadow1 = '0 5px 7px rgba(0,0,0,0.1)';
export const shadow2 = '0 1px 3px rgba(0,0,0,0.12), 0 1px 2px rgba(0,0,0,0.24)';
export const shadow3 = '0 2px 3px rgba(0,0,0,0.1), 0 3px 6px rgba(0,0,0,0.15)';
export const shadow4 = '0 10px 20px rgba(0,0,0,0.19), 0 6px 6px rgba(0,0,0,0.23)';
export const shadow5 = '0 14px 28px rgba(0,0,0,0.25), 0 10px 10px rgba(0,0,0,0.22)';
export const shadow6 = '0 19px 38px rgba(0,0,0,0.30), 0 15px 12px rgba(0,0,0,0.22)';
export const shadowBlur = '0 1px 3px rgba(0,0,0,0.05), 0 1px 4px rgba(0,0,0,0.2)';
export const shadowBorderGray = '0 0 0 2px #FFF, 0 0 0 5px #DDD';
export const shadowBorderBlack = '0 0 0 2px #FFF, 0 0 0 5px #000';

/*----------  BREAK POINT  ----------*/

export const breakPoint1920 = 1920;
export const breakPoint1600 = 1600;
export const breakPoint1440 = 1440;
export const breakPoint1280 = 1280;
export const breakPoint1170 = 1170;
export const breakPoint1024 = 1024;
export const breakPoint960 = 960;
export const breakPoint768 = 768;
export const breakPoint610 = 610;
export const breakPoint480 = 480;
export const breakPoint320 = 320;

/*----------  Z INDEX LAYER  ----------*/

export const zIndex1 = 10;
export const zIndex2 = 20;
export const zIndex3 = 30;
export const zIndex4 = 40;
export const zIndex5 = 50;
export const zIndex6 = 60;
export const zIndex7 = 70;
export const zIndex8 = 80;
export const zIndex9 = 90;

export const zIndexMin = 0;
export const zIndexMax = 100;

/*----------  TRANSITION  ----------*/

export const transitionNormal = '0.4s all ease 0s';
export const transitionHeight = '0.4s height ease 0s';
export const transitionOpacity = '0.4s opacity ease 0s';
export const transitionBackground = '0.4s background ease 0s';
export const transitionColor = '0.4s color ease 0s';
export const transitionTop = '0.4s top ease 0s';
export const transitionLeft = '0.4s left ease 0s';

/*----------  FONT  ----------*/

export const fontAvenirBold = 'avenir-next-bold, arial';
export const fontAvenirDemiBold = 'avenir-next-demi-bold, arial';
export const fontAvenirMedium = 'avenir-next-medium, arial';
export const fontAvenirRegular = 'avenir-next-regular, arial';
export const fontAvenirLight = 'avenir-next-light, arial';

/*----------  DEFINED BLUR  ----------*/

export const blur5 = 'blur(5px)';
export const blur10 = 'blur(10px)';
export const blur15 = 'blur(15px)';
export const blur20 = 'blur(20px)';
