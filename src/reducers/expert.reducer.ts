import { fromJS } from 'immutable';

import {
  /** BOX THIẾT KẾ BỞI CÁC CHUYÊN GIA */
  FECTH_LIST_EXPERT_SUCCESS,
  FECTH_LIST_EXPERT_ERROR,

  /** VIDEO TƯ VẤN - TRANG CHỦ */
  FECTH_LIST_EXPERT_VIDEO_SUCCESS,
  FECTH_LIST_EXPERT_VIDEO_ERROR,

} from '../constants/expert.action';

const INITIAL_STATE = fromJS({
  listExpert: {}, /** BOX THIẾT KẾ BỞI CÁC CHUYÊN GIA */
  listExpertVideo: [] /** VIDEO TƯ VẤN - TRANG CHỦ */
});

function expertReducer(state = INITIAL_STATE, action = { type: '', payload: null }) {
  switch (action.type) {

    /** BOX THIẾT KẾ BỞI CÁC CHUYÊN GIA */
    case FECTH_LIST_EXPERT_SUCCESS:
      return state.merge(
        fromJS({ 'listExpert': action.payload })
      );

    /** VIDEO TƯ VẤN - TRANG CHỦ */
    case FECTH_LIST_EXPERT_VIDEO_SUCCESS:
      return state.merge(
        fromJS({ 'listExpertVideo': action.payload.items })
      );

    default:
      return state;
  }
}


export default expertReducer;
