import { fromJS } from 'immutable';

import {
  /** BROWSE NODE - MENU CHÍNH */
  FETCH_LIST_MENU_SUCCESS,
  FETCH_LIST_MENU_ERROR,

  /** SHOW / HIDE SUB MENU DESKTOP */
  MENU_DESKTOP_ENTER,
  MENU_DESKTOP_LEAVE,

} from '../constants/menu.action';

const INITIAL_STATE = fromJS({
  listMenu: {}, /** BROWSE NODE - MENU CHÍNH */
  subMenuDesktop: { /** SHOW / HIDE SUB MENU DESKTOP */
    show: false,
    id: ''
  },
});

function menuReducer(state = INITIAL_STATE, action = { type: '', payload: null }) {
  switch (action.type) {

    /** BROWSE NODE - MENU CHÍNH */
    case FETCH_LIST_MENU_SUCCESS:
      return state.merge(
        fromJS({ 'listMenu': action.payload })
      );

    case MENU_DESKTOP_ENTER:
      return state.merge(
        fromJS({
          'subMenuDesktop': {
            show: true,
            id: action.payload
          }
        })
      );

    case MENU_DESKTOP_LEAVE:
      return state.merge(
        fromJS({
          'subMenuDesktop': {
            show: false,
            id: 0
          }
        })
      );

    default:
      return state;
  }
}


export default menuReducer;
