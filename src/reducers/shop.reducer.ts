import { fromJS } from 'immutable';

import {
  /** BOX MỚI NHẤT */
  FETCH_LASTEST_BOXES_SUCCESS,
  FETCH_LASTEST_BOXES_ERROR,

  /** BOX BÁN CHẠY NHẤT */
  FETCH_HOTTEST_BOXES_SUCCESS,
  FETCH_HOTTEST_BOXES_ERROR,

  /** MUA LẺ MỚI NHẤT */
  FETCH_LASTTEST_INDIVIDUAL_BOXES_SUCCESS,
  FETCH_LASTTEST_INDIVIDUAL_BOXES_ERROR,

  /** XEM BOX CÓ SẢN PHẨM */
  FETCH_HOTTEST_PRODUCT_SUCCESS,
  FETCH_HOTTEST_PRODUCT_ERROR,

  /** LẤY DANH SÁCH SẢN PHẨM CỦA CATEGORY */
  FETCH_PRODUCT_BY_CATEGORY_SUCCESS,
  FETCH_PRODUCT_BY_CATEGORY_ERROR
} from '../constants/shop.action';

const INITIAL_STATE = fromJS({
  lastestBoxes: [], /** BOX MỚI NHẤT */
  hottestBoxes: [], /** BOX BÁN CHẠY NHẤT */
  latestIndividualBoxes: [], /** MUA LẺ MỚI NHẤT */
  hottestProduct: [], /** XEM BOX CÓ SẢN PHẨM */
});

function shopReducer(state = INITIAL_STATE, action = { type: '', payload: null }) {
  switch (action.type) {

    /** BOX MỚI NHẤT */
    case FETCH_LASTEST_BOXES_SUCCESS:
      return state.merge(
        fromJS({ 'lastestBoxes': action.payload })
      );

    /** BOX BÁN CHẠY NHẤT */
    case FETCH_HOTTEST_BOXES_SUCCESS:
      return state.merge(
        fromJS({ 'hottestBoxes': action.payload })
      );

    /** MUA LẺ MỚI NHẤT */
    case FETCH_LASTTEST_INDIVIDUAL_BOXES_SUCCESS:
      return state.merge(
        fromJS({ 'latestIndividualBoxes': action.payload })
      );

    /** XEM BOX CÓ SẢN PHẨM */
    case FETCH_HOTTEST_PRODUCT_SUCCESS:
      return state.merge(
        fromJS({ 'hottestProduct': action.payload })
      );

    /** LẤY DANH SÁCH SẢN PHẨM CỦA CATEGORY */
    case FETCH_PRODUCT_BY_CATEGORY_SUCCESS:
      console.log(action);
      return;

    default:
      return state;
  }
}


export default shopReducer;
