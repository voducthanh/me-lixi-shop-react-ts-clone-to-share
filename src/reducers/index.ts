import { combineReducers } from 'redux';
const { routerReducer } = require('react-router-redux');
const formReducer = require('redux-form').reducer;

import shop from './shop.reducer';
import expert from './expert.reducer';
import menu from './menu.reducer';

const rootReducer = combineReducers({
  shop,
  expert,
  menu,
  routing: routerReducer,
  form: formReducer,
});

export default rootReducer;
