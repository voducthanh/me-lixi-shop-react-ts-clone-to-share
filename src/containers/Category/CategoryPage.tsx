import { Key } from 'readline';
/**
 * Created by Thành Võ on 09.03.2017
 */

import * as React from 'react';
import * as Radium from 'radium';
const connect = require('react-redux').connect;

import BreadCrumb from '../../components/general/UI/BreadCrumb';
import Pagination from '../../components/general/UI/Pagination';
import ContainerFilterCategory from '../../components/container/FilterCategory';
import ContainerFilterBrand from '../../components/container/FilterBrand';
import ContainerProductList from '../../components/container/ProductList';

import { CATEGORY_FILTER } from '../../constants/category.config';

import * as LAYOUT from '../../styles/layout.style';
import * as VARIABLE from '../../styles/variable.style';
import STYLE from './CategoryPage.style';

interface ICategoryPageProps extends React.Props<any> {
  params: {
    categoryFilter: string
  };
};
interface ICategoryPageState {};

function mapStateToProps(state) {
  return {};
}

function mapDispatchToProps(dispatch) {
  return {};
}

@Radium
class CategoryPage extends React.Component<ICategoryPageProps, any> {
  constructor(props: ICategoryPageProps) {
    super(props);

    this.state = {
      showCategory: false,
      showFilter: false,
      categoryFilter: {
        idCategory: {},
        params: []
      },
    };

    this.parseParamsUrl();
  }

  /**
   * Parse url to category filter
   *
   * @sample: /makeup_chanel_lv_luis-vuition_200k-300k_newest_page-3
   *
   * 1. Split by _
   * 2. First item -> id category
   * 3. Check item splitted by regex
   */
  parseParamsUrl() {
    const parserCategoryFilter = this.props.params.categoryFilter.split('_');

    /** Foreach list parse category */
    parserCategoryFilter.map((item, $index) => {
      if (0 === $index) {
        /** Assign first item -> id category */
        this.state.categoryFilter.idCategory = {
          key: CATEGORY_FILTER.idCategory.key,
          value: parserCategoryFilter[0],
        };
      } else if (CATEGORY_FILTER.price.pattern.test(item)) {
        /** Pattern min-max price */
        let price = item.split('-');

        /** Min price */
        this.state.categoryFilter.params.push({
          key: CATEGORY_FILTER.price.minPrice.key,
          value: price[0],
        });

        /** Max price */
        this.state.categoryFilter.params.push({
          key: CATEGORY_FILTER.price.maxPrice.key,
          value: price[1]
        });
      } else if (
        item === CATEGORY_FILTER.sort.value.newest
        || item === CATEGORY_FILTER.sort.value.priceDesc
        || item === CATEGORY_FILTER.sort.value.priceAsc) {
        /**
         * Sort:
         * - newest
         * - price-desc
         * - price-asc
         */
        this.state.categoryFilter.params.push({
          key: CATEGORY_FILTER.sort.key,
          value: item
        });
      } else if (CATEGORY_FILTER.page.pattern.test(item)) {
        /** Pagination */
        this.state.categoryFilter.params.push({
          key: CATEGORY_FILTER.page.key,
          value: item.replace(CATEGORY_FILTER.page.textReplace, '')
        });
      } else {
        /** Brand */
        this.state.categoryFilter.params.push({
          key: CATEGORY_FILTER.brand.key,
          value: item
        });
      }
    });

  }

  handleToggleShowCategory(_state: Boolean = false) {
    this.setState({
      showCategory: _state
    });
  }

  handleToogleShowFilter(_state) {
    this.setState({
      showFilter: _state
    });
  }

  render() {
    const { categoryFilter} = this.state;

    return (
      <div style={STYLE}>
        {/* wrap layout */}
        <div key="category-page-wrap" style={LAYOUT.wrapLayout}>
          <div style={[LAYOUT.splitContainer, LAYOUT.flexContainer.justify]}>

            {/* 1. Sidebar category */}
            <div style={[LAYOUT.splitContainer.left, STYLE.layout.left]}>

              {/* 1.1. Filter by category */}
              {
                (true === this.state.showCategory || window.innerWidth >= VARIABLE.breakPoint960)
                && <ContainerFilterCategory closeFilterCategory={() => this.handleToggleShowCategory(false)} />
              }

              {/* 1.2. Filter by brand - price range */}
              {
                (true === this.state.showFilter || window.innerWidth >= VARIABLE.breakPoint960)
                && <ContainerFilterBrand closeFilterBrand={() => this.handleToogleShowFilter(false)} />
              }
            </div>

            {/* 2. Sidebar category */}
            <div style={LAYOUT.splitContainer.main}>
              {/* 2.1. BreadCrumb List */}
              {
                window.innerWidth >= VARIABLE.breakPoint960
                && <BreadCrumb />
              }

              {
                (
                  (false === this.state.showCategory && false === this.state.showFilter)
                  || window.innerWidth >= VARIABLE.breakPoint960
                ) &&
                <div>
                  {/* 2.2. List product */}
                  <ContainerProductList
                    categoryFilter={categoryFilter}
                    showCategory={() => this.handleToggleShowCategory(true)}
                    showFilter={() => this.handleToogleShowFilter(true)}
                    title={'Cleanse (Rửa mặt)'}
                    column={4} />

                  {/* 2.3. Pagination */}
                  <Pagination />
                </div>
              }
            </div>
          </div>
        </div>
      </div>
    );
  }
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(CategoryPage);
