import * as React from 'react';
import * as Radium from 'radium';
import { StyleRoot } from 'radium';

import { fetchListMenuAction } from '../../actions/menu.action';

const connect = require('react-redux').connect;
const Link = require('react-router').Link;

import Header from '../../components/general/Header';
import Footer from '../../components/general/Footer';
import OverlayReload from '../../components/general/OverlayReload';

import * as VARIABLE from '../../styles/variable.style';

interface IAppProps extends React.Props<any> {
  routes: any;
  fetchListMenu: () => void;
  subMenuDesktop: any;
};

function mapStateToProps(state) {
  return {
    router: state.router,
    subMenuDesktop: state.menu.get('subMenuDesktop').toJS()
  };
}

function mapDispatchToProps(dispatch) {
  return {
    fetchListMenu: (): void => dispatch(fetchListMenuAction()),
  };
}

@Radium
class App extends React.Component<IAppProps, void> {
  constructor(props: IAppProps) {
    super(props);
  }


  componentWillMount() {
    this.props.fetchListMenu();
  }


  render() {
    const { children, subMenuDesktop } = this.props;

    return (
      <StyleRoot>
        {/* Header component */}
        <Header />

        {/* Render view */}
        {children}

        {/* Footer component */}
        {<Footer routes={this.props.routes} />}

        {/* Overlay Reload compoent: for responsive cross device */}
        <OverlayReload />
      </StyleRoot>
    );
  };
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(App);
