/**
 * Created by Thành Võ on 09.03.2017
 */

import * as VARIABLE from '../../styles/variable.style';

export default {
  display: 'block',
  position: 'relative',
  zIndex: VARIABLE.zIndex5,

  groupProductVideomagazine: {
    paddingTop: '10px',
    marginBottom: '20px',
  }
};
