/**
 * Created by Thành Võ on 09.03.2017
 */

import * as React from 'react';
import * as Radium from 'radium';
const connect = require('react-redux').connect;

import {
  fetchLastestBoxesAction,
  fetchHottestBoxesAction,
  fetchLastestIndividualBoxesAction,
  fetchHottestProductAction
} from '../../actions/shop.action';

import ContainerProductSlide from '../../components/container/ProductSlide';
import ContainerAsideVideo from '../../components/container/AsideVideo';
import ContainerAsideMagazine from '../../components/container/AsideMagazine';
import ContainerExpert from '../../components/container/Expert';
import ContainerFeedback from '../../components/container/Feedback';
import BannerHomeMain from '../../components/banner/HomeMain';

import * as VARIABLE from '../../styles/variable.style';
import * as LAYOUT from '../../styles/layout.style';

import STYLE from './HomePage.style';

/**  PROPS & STATE INTERFACE */
interface IHomePageProps extends React.Props<any> {
  routes: any;

  lastestBoxes: any;
  hottestBoxes: any;
  latestIndividualBoxes: any;
  hottestProduct: any;

  fetchLastestBoxes: () => void;
  fetchHottestBoxes: () => void;
  fetchLastestIndividualBoxes: () => void;
  fetchHottestProduct: () => void;
};

interface IHomePageState {
  showNaviTop: boolean;
};

function mapStateToProps(state) {
  return {
    lastestBoxes: {
      title: 'BEAUTY BOX MỚI NHẤT',
      data: state.shop.get('lastestBoxes').toJS()
    },
    hottestBoxes: {
      title: 'BEAUTY BOX BÁN CHẠY',
      data: state.shop.get('hottestBoxes').toJS()
    },
    latestIndividualBoxes: {
      title: 'MUA LẺ MỚI NHẤT',
      column: 5,
      data: state.shop.get('latestIndividualBoxes').toJS()
    },
    hottestProduct: {
      title: 'XEM BOX CÓ SẢN PHẨM',
      column: 5,
      type: 'minimal',
      data: state.shop.get('hottestProduct').toJS()
    },
  };
}

function mapDispatchToProps(dispatch) {
  return {
    fetchLastestBoxes: (): void => dispatch(fetchLastestBoxesAction()),
    fetchHottestBoxes: (): void => dispatch(fetchHottestBoxesAction()),
    fetchLastestIndividualBoxes: (): void => dispatch(fetchLastestIndividualBoxesAction()),
    fetchHottestProduct: (): void => dispatch(fetchHottestProductAction())
  };
}

@Radium
class HomePage extends React.Component<IHomePageProps, any> {
  constructor(props) {
    super(props);

    this.state = {};
  }

  componentDidMount() {
    this.initData();
  }


  /**
   * Init data for product / box component
   * - if without data: dispatch action to get
   * - if exsit data from state: nothing action
   */

  initData() {
    const {
      lastestBoxes,
      hottestBoxes,
      latestIndividualBoxes,
      hottestProduct,

      fetchLastestBoxes,
      fetchHottestBoxes,
      fetchLastestIndividualBoxes,
      fetchHottestProduct,
    } = this.props;

    /** FETCH DATA IF WITHOUT */
    (0 === lastestBoxes.data.length) && fetchLastestBoxes();
    0 === hottestBoxes.data.length && fetchHottestBoxes();
    0 === latestIndividualBoxes.data.length && fetchLastestIndividualBoxes();
    0 === hottestProduct.data.length && fetchHottestProduct();
  }

  render() {
    const {
      lastestBoxes, hottestBoxes, latestIndividualBoxes, hottestProduct
    } = this.props;

    return (
      <div style={STYLE}>
        {/** wrap layout */}
        <div key="home-page-wrap" style={LAYOUT.wrapLayout}>
          {/** 1. Splash banner -> move to header */}

          {/** 2. Banner main */}
          <BannerHomeMain />

          {/** 3. Group product list & video */}
          <div
            style={[
              LAYOUT.splitContainer,
              LAYOUT.flexContainer.justify,
              STYLE.groupProductVideomagazine
            ]}>
            {/** 3.1. List product */}
            <div style={LAYOUT.splitContainer.main}>

              {/** 3.3.1. BEAUTY BOX MỚI NHẤT */}
              <ContainerProductSlide {...lastestBoxes} />

              {/** 3.3.2. BEAUTY BOX BÁN CHẠY */}
              <ContainerProductSlide {...hottestBoxes} />
            </div>

            {/** 3.2. Video & Magazine */}
            <div style={LAYOUT.splitContainer.right}>
              <ContainerAsideVideo />
              <ContainerAsideMagazine />
            </div>
          </div>

          {/** 4. Expert List */}
          <ContainerExpert />

          {/** 5. MUA LẺ */}
          <ContainerProductSlide {...latestIndividualBoxes} />

          {/** 5. BOX CÓ SẢN PHẨM */}
          <ContainerProductSlide {...hottestProduct} />

          {/** 6. FEEDBACK */}
          <ContainerFeedback />
        </div>
      </div>
    );
  }
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(HomePage);
