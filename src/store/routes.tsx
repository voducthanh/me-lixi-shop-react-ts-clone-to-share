import * as React from 'react';
const { IndexRoute, Route } = require('react-router');

import App from '../containers/App/app';
import HomePage from '../containers/Home';
import CategoryPage from '../containers/Category';

import { ROUTING_HOME } from '../constants/routing';


export default (
  <Route path={ROUTING_HOME} component={App}>
    <IndexRoute component={HomePage} />
    <Route path="shop/:categoryFilter" component={CategoryPage} />
  </Route>
);
