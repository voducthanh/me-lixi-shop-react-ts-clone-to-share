/** CONFIG NAVIGATION DESKTOP & MOBILE */
export const NAVIGATION_BOX = {
  id: 847,
  title: 'Beauty box',
  slug: 'beauty-box',
  description: '',
  background: '',
  sub: [
    {
      id: 848,
      title: 'Box mới nhất',
      slug: 'new-beauty-box',
      description: 'Những box vừa được cập nhật mới trên lixibox',
      background: require('../assets/images/navigation/box/1.jpg'),
    },
    {
      id: 849,
      title: 'Box bán chạy nhất',
      slug: 'best-selling-beauty-box',
      description: 'Những box hot nhất đang được cộng đồng '
      + 'làm đẹp chú ý. Xem ngay.',
      background: require('../assets/images/navigation/box/2.jpg'),
    },
    {
      id: 850,
      title: 'Box trang điểm',
      slug: 'makeup-beauty-box',
      description: 'Sản phẩm makeup tốt nhất với mức giá '
      + ' cực ưu đãi đang được bày bán tại Lixibox.',
      background: require('../assets/images/navigation/box/3.jpg'),
    },
    {
      id: 851,
      title: 'Box dưỡng da',
      slug: 'skin-care-beauty-box',
      description: 'Chăm sóc làn da của bạn tốt nhất '
      + 'với các box mỹ phẩm hấp dẫn tại Lixibox',
      background: require('../assets/images/navigation/box/4.jpg'),
    },
    {
      id: 852,
      title: 'Box trị mụn',
      slug: 'acne-beauty-box',
      description: 'Bảo vệ làn da khỏi các tác nhân ngăn ngừa mụn '
      + 'cùng các sản phẩm ngừa mụn tại Lixibox.',
      background: require('../assets/images/navigation/box/5.jpg'),
    }
  ]
};
