const API_TYPE = {
  STAGING: 'staging',
  PRODUCTION: 'www'
};

const MOCK_DATA: Boolean = false;

export const SERVER_API = true === MOCK_DATA
  ? '/mock-data/'
  : 'http://localhost:8000/api/';

// : 'https://${API_TYPE.PRODUCTION}.lixibox.com:443/api/';

export const VERSION_API = {
  VERSION_1: 'v1',
  VERSION_2: 'v2',
};

export const VERSION_API_1 = `${SERVER_API}${VERSION_API.VERSION_1}`;
export const VERSION_API_2 = `${SERVER_API}${VERSION_API.VERSION_2}`;

export const STORAGE_DATA_LOCAL: Boolean = true;
