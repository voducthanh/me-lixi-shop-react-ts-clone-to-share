/**
 * Default pagination value
*/
export const DEFAULT_PAGINATION = {
  PAGE: 1,
  SUM: 12,
  SORT: 'newest'
};

/**
 * Category filter
 *
 * url from browser: /makeup_chanel_lv_200k-300k_newest_page-3
 * api query: /makeup.json?page=3&bids=lv%2Cchanel&pl=100&ph=200&sort=wprice-desc
 */
export const CATEGORY_FILTER = {
  idCategory: {
    key: 'idCategory'
  },
  price: {
    pattern: /\d*k-\d*k/,

    minPrice: {
      key: 'minPrice',
      apiQuery: 'pl'
    },
    maxPrice: {
      key: 'maxPrice',
      apiQuery: 'ph'
    },
  },
  sort: {
    key: 'sort',
    value: {
      newest: 'newest',
      priceDesc: 'price-desc',
      priceAsc: 'price-asc'
    },
    apiQuery: 'sort'
  },
  page: {
    key: 'page',
    pattern: /page-\d*/,
    textReplace: 'page-',
    apiQuery: 'page'
  },
  brand: {
    key: 'brand',
    apiQuery: 'bids'
  }
}
