export interface IDetailApi {
  path: String;
  description: String;
  errorMesssage?: String;
}

export const SHOP_API = {
  /** BOX MỚI NHẤT */
  LASTEST_BOXES: {
    path: '/shop/latest_boxes.json',
    description: 'Get latest boxs (in new home page) - BOX MỚI NHẤT',
    errorMesssage: `Can't get latest boxs data. Please try again`,
  },

  /** BOX BÁN CHẠY NHẤT */
  HOTTEST_BOXES: {
    path: '/shop/hottest_boxes.json',
    description: 'Get hottest boxs (in new home page) - BOX BÁN CHẠY NHẤT',
    errorMesssage: `Can't get hottest boxs data. Please try again`,
  },

  /** MUA LẺ MỚI NHẤT */
  LASTTEST_INDIVIDUAL_BOXES: {
    path: '/shop/latest_individual_boxes.json',
    description: 'Get lastest individual boxs (in new home page) - MUA LẺ MỚI NHẤT',
    errorMesssage: `Can't get lastest individual boxs data. Please try again`,
  },

  /** XEM BOX CÓ SẢN PHẨM */
  HOTTEST_PRODUCT: {
    path: '/shop/hottest_products.json',
    description: 'Get hottest product (in new home page) - XEM BOX CÓ SẢN PHẨM',
    errorMesssage: `Can't get hottest product data. Please try again`,
  },

  /** LẤY DANH SÁCH SẢN PHẨM CỦA CATEGORY */
  PRODUCT_BY_CATEGORY: (idCategory: string, query = '') => {
    return {
      path: `/browse_nodes/${idCategory}.json${query}`,
      description: 'Get list product in category - DANH SÁCH SẢN PHẪM TRONG CATEGORY',
      errorMesssage: `Can't get list product by category. Please try again`,
    };
  }
};

export const EXPERT_API = {
  /** BOX THIẾT KẾ BỞI CÁC CHUYÊN GIA */
  LIST_EXPERT: {
    path: '/experts.json',
    description: 'Get small list expert: recent_experts & most_revenue_experts - TRANG CHỦ - BOX THIẾT KẾ BỞI CÁC CHUYÊN GIA',
    errorMesssage: `Can't get list expert. Please try again`,
  },

  /** TRANG CHỦ - VIDEO TƯ VẤN */
  LIST_EXPERT_VIDEO: {
    path: '/experts/videos.json',
    description: 'Get list video make byt expert:  - TRANG CHỦ - VIDEO TƯ VẤN',
    errorMesssage: `Can't get list expert video. Please try again`,
  },
};

export const MENU_API = {
  /** BROWSE NODE - MENU CHÍNH */
  LIST_MENU: {
    path: '/browse_nodes.json',
    description: 'Get list menu - BROWSE NODE - MENU CHÍNH',
    errorMesssage: `Can't get list Menu. Please try again`,
  }
};
